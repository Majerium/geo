package de.majeres.geo.geodata.facade.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DynamicFilterRequest {
    @NotBlank private String type;
    @NotBlank private String value;
}
