package de.majeres.geo.geodata.facade.model;

public interface DataRequest {
    String getDataSet();
    FilterDatasetRequest getFilterDatasetRequest();
    boolean equals(Object o);
    int hashCode();
}
