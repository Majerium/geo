package de.majeres.geo.geodata.provider.deaths.services;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBeanBuilder;
import de.majeres.geo.geodata.mapper.Mapper;
import de.majeres.geo.geodata.provider.corona.model.DataSet;
import de.majeres.geo.geodata.provider.deaths.csv.CsvEntry;
import de.majeres.geo.geodata.provider.deaths.repository.model.DeathData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DeathsCsvParserService {

    @Value("classpath:sterbezahlen.csv")
    private Resource resourceFile;

    public List<DeathData> loadFromCsv() {

        List<CsvEntry> beans = new ArrayList<>();
        try {
            beans = new CsvToBeanBuilder(new CSVReader(new InputStreamReader(resourceFile.getInputStream())))
                    .withType(CsvEntry.class)
                    .build()
                    .parse();


            List<DeathData> collect = beans.stream().map(e -> Mapper.fromCsvEntry(e, DataSet.DEATHS.name())).collect(Collectors.toList());

            for(int i = 0; i < collect.size(); i++) {
                collect.get(i).setId(i);
            }

            return collect;

        } catch (IOException e) {
            log.error("could not parse file", e);
        }
        return new ArrayList<>();
    }
}

