package de.majeres.geo.geodata.provider.deaths.repository;

import de.majeres.geo.geodata.provider.deaths.repository.model.DeathData;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeathsRepository extends ReactiveMongoRepository<DeathData, String>, DeathsDataRepository {
}
