package de.majeres.geo.geodata.facade.model;

import de.majeres.geo.geodata.provider.corona.model.DataSet;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
@NoArgsConstructor
public class DatasetResponse {
    List<String> dataSets = Arrays.asList(DataSet.values()).stream().map(Enum::name).collect(Collectors.toList());
}
