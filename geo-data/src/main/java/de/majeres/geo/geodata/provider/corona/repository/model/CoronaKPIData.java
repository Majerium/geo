package de.majeres.geo.geodata.provider.corona.repository.model;

import de.majeres.geo.geodata.kpi.model.GeoPoint;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@Document(value = "GeoKPIData")
public class CoronaKPIData implements GeoPoint {

    @Id
    private String objectId;
    @Indexed
    private String dataset;
    @Indexed
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate meldedatum;
    @Indexed @Field("AGE_GROUP")
    private String altersgruppe;
    @Field("DISTRICT")
    private String idLandkreis;
    private Long anzahlTodesfall;
    private Long neuerFall;
    private Long neuerTodesfall;
    private Long neuGenesen;
    private Long anzahlGenesen;
    private Long anzahlFall;
    private Long istErkrankungsbeginn;
    @GeoSpatialIndexed
    private double[] position;

}
