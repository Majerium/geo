package de.majeres.geo.geodata.facade.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MultiGraphResponse implements DataResponse {
    private List<GraphResponse> graphResponseList = new ArrayList<>();
}
