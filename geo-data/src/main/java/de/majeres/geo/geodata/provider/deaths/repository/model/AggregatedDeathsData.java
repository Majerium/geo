package de.majeres.geo.geodata.provider.deaths.repository.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;

@Data
public class AggregatedDeathsData {
    @Id
    private LocalDate date;
    private Long count;

}