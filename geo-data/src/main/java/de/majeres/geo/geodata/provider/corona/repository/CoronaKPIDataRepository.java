package de.majeres.geo.geodata.provider.corona.repository;

import de.majeres.geo.geodata.provider.corona.repository.model.AggregatedCoronaKPIData;
import de.majeres.geo.geodata.provider.corona.repository.model.CoronaKPIData;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;
import org.springframework.data.mongodb.core.query.Criteria;
import reactor.core.publisher.Flux;

public interface CoronaKPIDataRepository {
    Flux<CoronaKPIData> findByPolygon(Criteria filterCriteria, GeoJsonPolygon polygon);
    Flux<AggregatedCoronaKPIData> findByAggregation(Aggregation filterCriteria);
    void persistCoronaKPIData(CoronaKPIData entry);

}
