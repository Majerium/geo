package de.majeres.geo.geodata.facade.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GraphResponse implements DataResponse {
    private String name;
    private List<PointResponse> graph;
}
