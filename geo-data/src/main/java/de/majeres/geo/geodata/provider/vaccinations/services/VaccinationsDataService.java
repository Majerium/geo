package de.majeres.geo.geodata.provider.vaccinations.services;

import de.majeres.geo.geodata.provider.vaccinations.repository.VaccinationKpiRepository;
import de.majeres.geo.geodata.provider.vaccinations.repository.model.VaccinationKPIData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.List;

@Slf4j
@Service
public class VaccinationsDataService {

    @Autowired
    private VaccinationKpiRepository geoRepository;

    @Autowired
    private VaccinationsCsvParserService rkiCsvParserService;

    @Value("${rki.csv.initialload}")
    private boolean initialLoad;

    @PostConstruct
    public void initialize() {
        if (initialLoad) getAndSaveRKIData();
    }

    public void getAndSaveRKIData() {
        List<VaccinationKPIData> vaccinationKPIData = rkiCsvParserService.loadFromCsv();
        log.info("Inserting Vaccination Data");
        Mono.just(vaccinationKPIData)
                .flatMapMany(Flux::fromIterable)
                .subscribe(e -> geoRepository.saveVaccinationKPIData(e));
    }
}
