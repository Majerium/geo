package de.majeres.geo.geodata.provider.corona.model;

import de.majeres.geo.geodata.provider.vaccinations.csv.CsvEntry;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

@AllArgsConstructor
@Getter
public enum State {

    GROUP_0(5l,"Nordrhein-Westfalen", new double[] {7.435722, 51.471119}),
    GROUP_1(11l, "Berlin", new double[] {13.404194, 52.502889}),
    GROUP_2(10l, "Saarland", new double[] {6.878333, 49.376944}),
    GROUP_3(9l, "Bayern", new double[] {11.404167, 48.946389}),
    GROUP_4(3l, "Niedersachsen", new double[] {9.076019, 52.839842}),
    GROUP_5(12l,"Brandenburg", new double[] {13.015833, 52.459028}),
    GROUP_6(8l,"Baden-Württemberg", new double[] {9.003889, 48.661944}),
    GROUP_7(1l,"Schleswig-Holstein", new double[] {9.822222, 54.185556}),
    GROUP_8(6l,"Hessen", new double[] {9.028465, 50.608047}),
    GROUP_9(7l,"Rheinland-Pfalz", new double[] {7.310417, 49.955139}),
    GROUP_10(2l,"Hamburg", new double[] {10.028889, 53.568889}),
    GROUP_11(4l,"Bremen", new double[] {8.806442, 53.082477}),
    GROUP_12(14l,"Sachsen", new double[] {13.458333, 50.929472}),
    GROUP_13(13l,"Mecklenburg-Vorpommern", new double[] {12.575558, 53.7735}),
    GROUP_14(16l,"Thüringen", new double[] {11.026389, 50.903333}),
    GROUP_15(15l,"Sachsen-Anhalt", new double[] {11.835861, 51.9245}),
    GROUP_16(17l, "Bundesressort", new double[] {10.447683, 51.163361});

    private Long id;
    private String value;
    private double[] center;

    public static Optional<State> fromCsvEntry(CsvEntry csvEntry) {
        return Arrays.asList(State.values()).stream()
                .filter(state -> state.id.equals(csvEntry.getDistrictId()))
                .findFirst();
    }
}
