package de.majeres.geo.geodata.facade.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Getter
@NoArgsConstructor
public class KpiConfig {
    private String kpi;
    private Long max;
}
