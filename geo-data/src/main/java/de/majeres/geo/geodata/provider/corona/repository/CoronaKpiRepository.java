package de.majeres.geo.geodata.provider.corona.repository;

import de.majeres.geo.geodata.provider.corona.repository.model.CoronaKPIData;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoronaKpiRepository extends ReactiveMongoRepository<CoronaKPIData, String>, CoronaKPIDataRepository {

}
