package de.majeres.geo.geodata.facade;

import de.majeres.geo.geodata.facade.model.*;
import de.majeres.geo.geodata.kpi.service.ResponseCacheService;
import de.majeres.geo.geodata.mapper.Mapper;
import de.majeres.geo.geodata.provider.vaccinations.services.KPIService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.Optional;
import java.util.Set;

@AllArgsConstructor
@RestController
public class GeoDataAPIController {
    private Set<KPIService> kpiServiceList;
    private ResponseCacheService cacheService;

    @ResponseBody @CrossOrigin
    @RequestMapping(value = "/api/data/byShapeRequest", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ShapeDataResponse> getKPIbyFilterShapeRequest(@RequestBody @Valid ShapeDataRequest request) {
        Mono<DataResponse> dataResponseMono = cacheService.useCache(request) ? getFromCache().getResponse(request) : getShapeFromDB(request.getDataSet()).getResponse(request);
        return dataResponseMono.map(e -> (ShapeDataResponse) e);
    }

    @ResponseBody @CrossOrigin
    @RequestMapping(value = "/api/data/byGraphRequest", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<MultiGraphResponse> getKPIsByFilterGraphRequest(@RequestBody @Valid GraphDataRequest request) {
        Mono<DataResponse> dataResponseMono = cacheService.useCache(request) ? getFromCache().getResponse(request) : getGraphFromDB(request).getResponse(request);
        return dataResponseMono.map(e -> (MultiGraphResponse) e);
    }

    private ShapeDataSupplier getFromCache() {
        return (request) ->  Mono.just(cacheService.getCachedResponse(request)).doOnTerminate(() -> cacheService.increaseCachingTime(request));
    }


    private ShapeDataSupplier getGraphFromDB(GraphDataRequest request) {
        KPIService service = this.getServiceForDataset(request.getDataSet()).get();
        return r -> service.getValuesForGraph(request)
                .map(Mapper::fromKpiList)
                .map(e -> (DataResponse) e).doOnSuccess((response) -> cacheService.cacheResponse(request, response));
    }

    private ShapeDataSupplier getShapeFromDB(String dataSet) {
        KPIService service =  this.kpiServiceList.stream().filter( s -> s.getDataset().equals(dataSet)).findFirst().get();
        return request -> service.getValueForPolygon(request, Mapper.toGeoJsonPolygon(((ShapeDataRequest) request).getFeature())).map(ShapeDataResponse::new).map(e -> (DataResponse) e) .doOnSuccess((response) -> cacheService.cacheResponse(request, response));
    }

    private Optional<KPIService> getServiceForDataset(String dataset) {
        return this.kpiServiceList.stream().filter(s -> s.getDataset().equals(dataset)).findFirst();
    }

}
