package de.majeres.geo.geodata.kpi.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Optional;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Kpi {
    private Long anzahlFall;
    private Long anzahlTodesfall;
    private Long neuerFall;
    private Long neuerTodesfall;
    private Long neuGenesen;
    private Long anzahlGenesen;
    private Long isErkrankunsbeginn;
    private Long vaccinationAmount;
    private Long deaths;
    private Optional<LocalDate> meldedatum = Optional.empty();
    private Optional<LocalDate> vaccinationDate = Optional.empty();


    public static Kpi emptyKPI() {
        return new Kpi(0l,0l,0l,0l,0l,0l, 0l, 0l, 0l, Optional.empty(), Optional.empty());
    }
}
