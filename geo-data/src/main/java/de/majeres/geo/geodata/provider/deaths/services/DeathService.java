package de.majeres.geo.geodata.provider.deaths.services;

import de.majeres.geo.geodata.facade.model.DataRequest;
import de.majeres.geo.geodata.facade.model.GraphDataRequest;
import de.majeres.geo.geodata.kpi.model.Kpi;
import de.majeres.geo.geodata.mapper.Mapper;
import de.majeres.geo.geodata.provider.corona.model.DataSet;
import de.majeres.geo.geodata.provider.deaths.repository.DeathsRepository;
import de.majeres.geo.geodata.provider.deaths.repository.model.AggregatedDeathsData;
import de.majeres.geo.geodata.provider.deaths.repository.model.DeathData;
import de.majeres.geo.geodata.provider.vaccinations.services.KPIService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;

@Slf4j
@Service
@AllArgsConstructor
public class DeathService implements KPIService {
    private DeathsRepository deathsRepository;

    public Mono<Kpi> getValueForPolygon(DataRequest request, GeoJsonPolygon geoJsonPolygon) {
        Flux<DeathData> byPolygon = deathsRepository.findByPolygon(getFilterCriteria(request), geoJsonPolygon);
        return byPolygon.map(Mapper::fromDeathData)
            .collect(Collectors.reducing(Mapper::reduceKpi))
            .map(opt -> opt.orElseGet(Kpi::emptyKPI));
    }

    public Mono<List<Kpi>> getValuesForGraph(GraphDataRequest request) {
        Flux<AggregatedDeathsData> findByAggregation = deathsRepository.findByAggregation(getAggregation(request)).sort(Comparator.comparing(AggregatedDeathsData::getDate));
        return findByAggregation.map(Mapper::fromDeathKpiData)
                .collectList();
    }

    @Override
    public String getDataset() {
        return DataSet.DEATHS.name();
    }

    // Query aufbau: https://lishman.io/spring-data-mongotemplate-queries
    private Criteria getFilterCriteria(DataRequest request) {
        Criteria criteria = Criteria.where("dataset").is(request.getDataSet());
        // Add dynamic filter
        request.getFilterDatasetRequest().getDynamicFilterRequests().forEach(df -> criteria.and(df.getType()).is(df.getValue()));

        if (hasFrom(request) && hasTo(request)) {
            criteria.and("date").gte(request.getFilterDatasetRequest().getFrom().get()).lte(request.getFilterDatasetRequest().getTo().get());
            return criteria;
        }

        if (request.getFilterDatasetRequest().getFrom().isPresent()) {
            criteria.and("date").gte(request.getFilterDatasetRequest().getFrom().get());
        }

        if (hasTo(request)) {
            criteria.and("date").lte(request.getFilterDatasetRequest().getTo().get());
        }

        return criteria;
    }


    private Aggregation getAggregation(DataRequest request) {
        Criteria criteria = getFilterCriteria(request);

        ProjectionOperation projectStage = Aggregation.project()
            .and("count").as("count")
            .and("date").as("date");

        GroupOperation groupBy = group("date")
            .sum("count").as("count");

        return Aggregation.newAggregation(Aggregation.match(criteria), projectStage, groupBy);
    }

    private boolean hasFrom(DataRequest request) {
        return request.getFilterDatasetRequest().getFrom().isPresent();
    }

    private boolean hasTo(DataRequest request) {
        return request.getFilterDatasetRequest().getTo().isPresent();
    }
}
