package de.majeres.geo.geodata.provider.deaths.repository.model;

import de.majeres.geo.geodata.kpi.model.GeoPoint;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Data
@Document(value = "DeathData")
public class DeathData implements GeoPoint {
    @Id
    private long id;
    @Indexed
    private String dataset;
    @Indexed
    private LocalDate date;
    private Long count;
    @GeoSpatialIndexed
    private double[] position;

}
