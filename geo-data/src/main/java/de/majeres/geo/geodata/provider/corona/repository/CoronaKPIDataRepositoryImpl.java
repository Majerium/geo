package de.majeres.geo.geodata.provider.corona.repository;

import de.majeres.geo.geodata.provider.corona.repository.model.AggregatedCoronaKPIData;
import de.majeres.geo.geodata.provider.corona.repository.model.CoronaKPIData;
import lombok.AllArgsConstructor;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import reactor.core.publisher.Flux;

@AllArgsConstructor
public class CoronaKPIDataRepositoryImpl implements CoronaKPIDataRepository {
    private final ReactiveMongoTemplate mongoTemplate;

    @Override
    public Flux<CoronaKPIData> findByPolygon(Criteria filterCriteria, GeoJsonPolygon polygon) {
        Query query = new Query(filterCriteria.and("position").within(polygon));
        return mongoTemplate.find(query, CoronaKPIData.class);
    }

    @Override
    public Flux<AggregatedCoronaKPIData> findByAggregation(Aggregation aggregation) {
        return mongoTemplate.aggregate(aggregation,"GeoKPIData", AggregatedCoronaKPIData.class);
    }

    @Override
    public void persistCoronaKPIData(CoronaKPIData data) {
        mongoTemplate.save(data).subscribe();
    }

}
