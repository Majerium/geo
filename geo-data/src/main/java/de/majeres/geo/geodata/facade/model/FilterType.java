package de.majeres.geo.geodata.facade.model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum FilterType {
    GENDER, AGE_GROUP, KPI, DISTRICT, VACCINATION_SERIES, VACCINE, VACCINATION_DISTRICT;
}
