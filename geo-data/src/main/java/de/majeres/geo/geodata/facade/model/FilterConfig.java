package de.majeres.geo.geodata.facade.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class FilterConfig {
    private String type;
    private List<String> options;
    private boolean visibleOnMap;
    private boolean visibleOnGraph;
}
