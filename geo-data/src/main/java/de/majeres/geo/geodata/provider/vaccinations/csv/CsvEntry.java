package de.majeres.geo.geodata.provider.vaccinations.csv;

import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvDate;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

//Impfdatum,BundeslandId_Impfort,Impfstoff,Impfserie,Anzahl
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CsvEntry {

    public static CsvEntry reduce(CsvEntry kpi1, CsvEntry kpi2) {
        return new CsvEntry(
                kpi2.getDate(),
                kpi2.getDistrictId(),
                kpi2.getVaccine(),
                kpi2.getSeries(),
                getValueOrZero(() -> kpi1.getAmount()) + getValueOrZero(() -> kpi2.getAmount())
        );
    }

    public static Long getValueOrZero(Supplier<Long> supplier) {
        return Optional.ofNullable(supplier.get()).orElse(0L);
    }

    //Impfdatum
    @CsvDate(value = "yyyy-MM-dd")
    @CsvBindByPosition(position = 0)
    private LocalDate date;

    //BundeslandId_Impfort
    @CsvBindByPosition(position = 1)
    private Long districtId;

    //Impfstoff
    @CsvBindByPosition(position = 2)
    private String vaccine;

    //Impfserie
    @CsvBindByPosition(position = 3)
    private Long series;

    //Anzahl
    @CsvBindByPosition(position = 4)
    private Long amount;

    public static void main(String... args) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        String fileName = "C:\\Users\\majer\\git\\geo\\geo-data\\src\\main\\resources\\vaxxinations.csv";

        List<CsvEntry> beans = new CsvToBeanBuilder(new FileReader(fileName))
                .withType(CsvEntry.class)
                .build()
                .parse();

        Map<LocalDate, Map<Long, Map<String, Map<Long, Optional<CsvEntry>>>>> collect = beans.stream()
                .collect(Collectors.groupingBy(CsvEntry::getDate,
                        Collectors.groupingBy(CsvEntry::getDistrictId,
                                Collectors.groupingBy(CsvEntry::getVaccine,
                                        Collectors.groupingBy(CsvEntry::getSeries,
                                         Collectors.mapping(Function.identity(), Collectors.reducing(CsvEntry::reduce)))))));


        List<CsvEntry> collect1 = collect.values().stream().flatMap(y -> y.values().stream().flatMap(x -> x.values().stream().flatMap(z -> z.values().stream()))).filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());

        List<String> collect2 = collect1.stream().map(s -> s.getVaccine()).distinct().collect(Collectors.toList());

        //Long aLong = collect.values().stream().flatMap(y -> y.values().stream().flatMap(x -> x.values().stream())).filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList()).stream().map(CsvEntry::getAnzahlTodesfall).reduce((a, b) -> a + b).get();
        System.out.println(collect);
    }
}
