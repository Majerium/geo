package de.majeres.geo.geodata.provider.vaccinations.repository.model;

import lombok.AllArgsConstructor;

import java.util.Arrays;

@AllArgsConstructor
public enum VaccinationSeries {
    SERIES_1(1l), SERIES_2(2l), SERIES_3(3l), SERIES_4(4l), SERIES_5(5l),SERIES_ERR(-1l);
    private Long series;

    public static String fromLong(Long value) {
        return Arrays.stream(VaccinationSeries.values()).filter(s -> s.series.equals(value)).findFirst().orElse(SERIES_ERR).name();
    }
}
