package de.majeres.geo.geodata.provider.corona.repository.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;

@Data
public class AggregatedCoronaKPIData {
    @Id
    private LocalDate meldedatum;
    private Long anzahlFall;
    private Long anzahlTodesfall;
    private Long neuerFall;
    private Long neuerTodesfall;
    private Long neuGenesen;
    private Long anzahlGenesen;
    private Long istErkrankungsbeginn;

}
