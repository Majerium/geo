package de.majeres.geo.geodata.provider.vaccinations.repository;

import de.majeres.geo.geodata.provider.vaccinations.repository.model.AggregatedVaccinationKPIData;
import de.majeres.geo.geodata.provider.vaccinations.repository.model.VaccinationKPIData;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;
import org.springframework.data.mongodb.core.query.Criteria;
import reactor.core.publisher.Flux;

public interface VaccinationKPIDataRepository {
    Flux<VaccinationKPIData> findByPolygon(Criteria filterCriteria, GeoJsonPolygon polygon);
    Flux<AggregatedVaccinationKPIData> findByAggregation(Aggregation filterCriteria);
    void saveVaccinationKPIData(VaccinationKPIData data);
}
