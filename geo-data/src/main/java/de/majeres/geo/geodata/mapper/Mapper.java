package de.majeres.geo.geodata.mapper;

import de.majeres.geo.geodata.facade.model.GraphResponse;
import de.majeres.geo.geodata.facade.model.MultiGraphResponse;
import de.majeres.geo.geodata.facade.model.PointResponse;
import de.majeres.geo.geodata.kpi.model.Kpi;
import de.majeres.geo.geodata.provider.corona.csv.CsvEntry;
import de.majeres.geo.geodata.provider.corona.model.AgeGroup;
import de.majeres.geo.geodata.provider.corona.model.District;
import de.majeres.geo.geodata.provider.corona.model.RKIKpis;
import de.majeres.geo.geodata.provider.corona.model.State;
import de.majeres.geo.geodata.provider.corona.repository.model.AggregatedCoronaKPIData;
import de.majeres.geo.geodata.provider.corona.repository.model.CoronaKPIData;
import de.majeres.geo.geodata.provider.deaths.repository.model.AggregatedDeathsData;
import de.majeres.geo.geodata.provider.deaths.repository.model.Country;
import de.majeres.geo.geodata.provider.deaths.repository.model.DeathData;
import de.majeres.geo.geodata.provider.deaths.repository.model.DeathsKpis;
import de.majeres.geo.geodata.provider.vaccinations.repository.model.*;
import lombok.extern.slf4j.Slf4j;
import org.geojson.Feature;
import org.geojson.LngLatAlt;
import org.geojson.Polygon;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Slf4j
public class Mapper {

    public static GeoJsonPolygon toGeoJsonPolygon(Feature feature) {
        Polygon polygon = (Polygon) feature.getGeometry();
        GeoJsonPolygon mongoPolygon = new GeoJsonPolygon(polygon.getExteriorRing().stream().map(Mapper::toPoint).collect(Collectors.toList()));
        polygon.getInteriorRings().forEach(ring -> mongoPolygon.withInnerRing(ring.stream().map(Mapper::toPoint).collect(Collectors.toList())));
        return mongoPolygon;
    }

    public static Point toPoint(LngLatAlt from) {
        return new Point(from.getLongitude(), from.getLatitude());
    }

    public static CoronaKPIData fromCsvEntry(CsvEntry csvEntry, String dataset) {
        District district = District.fromCsvEntry(csvEntry).orElseGet(() -> District.GROUP_ERR);
        CoronaKPIData entry = new CoronaKPIData();
        entry.setDataset(dataset);
        entry.setObjectId(csvEntry.getObjectId());
        entry.setAltersgruppe(AgeGroup.fromCsvEntry(csvEntry).orElseGet(() -> AgeGroup.GROUP_ERR).name());
        //entry.setGeschlecht(Gender.fromCsvEntry(csvEntry).orElseGet(() -> Gender.GROUP_ERR).name());
        entry.setAnzahlGenesen(csvEntry.getAnzahlGenesen());
        entry.setAnzahlTodesfall(csvEntry.getAnzahlTodesfall());
        entry.setIdLandkreis(district.getLandkreis());
        entry.setIstErkrankungsbeginn(csvEntry.getIstErkrankungsbeginn());
        entry.setMeldedatum(csvEntry.getMeldedatum());
        entry.setAnzahlTodesfall(csvEntry.getAnzahlTodesfall());
        entry.setAnzahlGenesen(csvEntry.getAnzahlGenesen());
        entry.setNeuerTodesfall(csvEntry.getNeuerTodesfall());
        entry.setAnzahlFall(csvEntry.getAnzahlFall());
        entry.setPosition(district.getCenter());
        entry.setIstErkrankungsbeginn(csvEntry.getIstErkrankungsbeginn());
        return entry;
    }

    public static VaccinationKPIData fromCsvEntry(de.majeres.geo.geodata.provider.vaccinations.csv.CsvEntry csvEntry, String dataset) {
        State state = State.fromCsvEntry(csvEntry).orElseGet(() -> State.GROUP_16);
        VaccinationKPIData entry = new VaccinationKPIData();
        entry.setDataset(dataset);
        entry.setVaccinationDate(csvEntry.getDate());
        entry.setAmount(csvEntry.getAmount());
        entry.setDistrictId(csvEntry.getDistrictId());
        entry.setSeries(VaccinationSeries.fromLong(csvEntry.getSeries()));
        entry.setPosition(state.getCenter());
        entry.setVaccine(Vaccine.fromName(csvEntry.getVaccine()));
        return entry;
    }

    public static DeathData fromCsvEntry(de.majeres.geo.geodata.provider.deaths.csv.CsvEntry csvEntry, String dataset) {
        DeathData entry = new DeathData();
        entry.setDataset(dataset);
        entry.setCount(csvEntry.getCount());
        entry.setDate(csvEntry.getDate());
        entry.setPosition(Country.GERMANY.getCenter());
        return entry;
    }

    public static Kpi fromDeathData(DeathData entry) {
        return new Kpi(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                entry.getCount(),
                Optional.empty(),
                Optional.empty()
        );
    }

    public static Kpi fromShapeKPIData(CoronaKPIData entry) {
        return new Kpi(
                entry.getAnzahlFall(),
                entry.getAnzahlTodesfall(),
                entry.getNeuerFall(),
                entry.getNeuerTodesfall(),
                entry.getNeuGenesen(),
                entry.getAnzahlGenesen(),
                entry.getIstErkrankungsbeginn(),
                null,
                null,
                Optional.empty(),
                Optional.empty()
        );
    }

    public static Kpi fromVaccinationKPIData(VaccinationKPIData entry) {
        return new Kpi(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                entry.getAmount(),
                null,
                Optional.empty(),
                Optional.empty()
        );
    }

    public static Kpi fromDeathKpiData(AggregatedDeathsData entry) {
        return new Kpi(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                entry.getCount(),
                Optional.ofNullable(entry.getDate()),
                Optional.empty()
        );
    }

    public static Kpi fromGraphKpiData(AggregatedCoronaKPIData entry) {
        return new Kpi(
                entry.getAnzahlFall(),
                entry.getAnzahlTodesfall(),
                entry.getNeuerFall(),
                entry.getNeuerTodesfall(),
                entry.getNeuGenesen(),
                entry.getAnzahlGenesen(),
                entry.getIstErkrankungsbeginn(),
                null,
                null,
                Optional.ofNullable(entry.getMeldedatum()),
                Optional.empty()
        );
    }

    public static Kpi fromAggregatedVaccinationKPIData(AggregatedVaccinationKPIData entry) {
        return new Kpi(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                entry.getAmount(),
                null,
                Optional.empty(),
                Optional.ofNullable(entry.getVaccinationDate())
        );
    }

    public static MultiGraphResponse fromKpiList(List<Kpi> kpis) {
        MultiGraphResponse response = new MultiGraphResponse();
        response.getGraphResponseList().add(getGraphResponse(DeathsKpis.DEATH_COUNT.name(), kpis, (kpi) -> valueExtractor(() -> kpi.getDeaths()).doubleValue()));
        response.getGraphResponseList().add(getGraphResponse(VaccinationKpis.VACCINATIONS_AMOUNT.name(), kpis, (kpi) -> valueExtractor(() -> kpi.getVaccinationAmount()).doubleValue()));
        response.getGraphResponseList().add(getGraphResponse(RKIKpis.COUNT_IS_ILLNESS_START.name(), kpis, (kpi) -> valueExtractor(() -> kpi.getIsErkrankunsbeginn()).doubleValue()));
        response.getGraphResponseList().add(getGraphResponse(RKIKpis.COUNT_CASE.name(), kpis, (kpi) -> valueExtractor(() -> kpi.getAnzahlFall()).doubleValue()));
        response.getGraphResponseList().add(getGraphResponse(RKIKpis.COUNT_HEALED.name(), kpis, (kpi) -> valueExtractor(() -> kpi.getAnzahlGenesen()).doubleValue()));
        response.getGraphResponseList().add(getGraphResponse(RKIKpis.COUNT_NEW_CASE.name(), kpis, (kpi) -> valueExtractor(() -> kpi.getNeuerFall()).doubleValue()));
        response.getGraphResponseList().add(getGraphResponse(RKIKpis.COUNT_NEW_DEATH_CASE.name(),  kpis, (kpi) -> valueExtractor(() -> kpi.getNeuerTodesfall()).doubleValue()));
        response.getGraphResponseList().add(getGraphResponse(RKIKpis.COUNT_NEW_HEALED.name(), kpis, (kpi) -> valueExtractor(() -> kpi.getNeuGenesen()).doubleValue()));
        response.getGraphResponseList().add(getGraphResponse(RKIKpis.COUNT_DEATH_CASE.name(), kpis, (kpi) -> valueExtractor(() -> kpi.getAnzahlTodesfall()).doubleValue()));

        return response;
    }

    private static Long valueExtractor(Supplier<Long> longSupplier) {
        return Optional.ofNullable(longSupplier.get()).orElse(0l);
    }

    public static GraphResponse getGraphResponse(String kpiName, List<Kpi> kpis, Function<Kpi, Double> fn) {
        GraphResponse graphResponse = new GraphResponse();
        graphResponse.setName(kpiName);
        graphResponse.setGraph(kpis.stream().map(kpi -> fromKpi(kpi, fn)).collect(Collectors.toList()));
        return graphResponse;
    }

    public static PointResponse fromKpi(Kpi kpi, Function<Kpi, Double> fn) {
        return new PointResponse(
              fn.apply(kpi), kpi.getMeldedatum().isPresent() ? kpi.getMeldedatum().get() : kpi.getVaccinationDate().get()
        );
    }

    public static Kpi reduceKpi(Kpi kpi1, Kpi kpi2) {
        return new Kpi(
                getValueOrNull(() -> getValueOrZero(() -> kpi1.getAnzahlFall()) + getValueOrZero(() -> kpi2.getAnzahlFall())),
                getValueOrNull(() -> getValueOrZero(() -> kpi1.getAnzahlTodesfall()) + getValueOrZero(() -> kpi2.getAnzahlTodesfall())),
                getValueOrNull(() -> getValueOrZero(() -> kpi1.getNeuerFall()) + getValueOrZero(() -> kpi2.getNeuerFall())),
                getValueOrNull(() -> getValueOrZero(() -> kpi1.getNeuerTodesfall()) + getValueOrZero(() -> kpi2.getNeuerTodesfall())),
                getValueOrNull(() -> getValueOrZero(() -> kpi1.getNeuGenesen() )+ getValueOrZero(() -> kpi2.getNeuGenesen())),
                getValueOrNull(() -> getValueOrZero(() -> kpi1.getAnzahlGenesen()) + getValueOrZero(() -> kpi2.getAnzahlGenesen())),
                getValueOrNull(() -> getValueOrZero(() -> kpi1.getIsErkrankunsbeginn()) + getValueOrZero(() -> kpi2.getIsErkrankunsbeginn())),
                getValueOrNull(() -> getValueOrZero(() -> kpi1.getVaccinationAmount()) + getValueOrZero(() -> kpi2.getVaccinationAmount())),
                getValueOrNull(() -> getValueOrZero(() -> kpi1.getDeaths()) + getValueOrZero(() -> kpi2.getDeaths())),
                Optional.empty(),
                Optional.empty()

        );
    }

    public static Long getValueOrZero(Supplier<Long> supplier) {
        return Optional.ofNullable(supplier.get()).orElse(0L);
    }

    public static Long getValueOrNull(Supplier<Long> supplier) {
        Long aLong = supplier.get();
        return aLong.equals(0) ? null : aLong;
    }

}
