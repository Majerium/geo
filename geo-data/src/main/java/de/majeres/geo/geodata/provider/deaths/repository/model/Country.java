package de.majeres.geo.geodata.provider.deaths.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Country {
    GERMANY(new double[] {10.18867, 51.31722});
    private double[] center;
}
