package de.majeres.geo.geodata.facade.model;

import lombok.Getter;
import lombok.Setter;
import org.geojson.Feature;

import javax.validation.constraints.NotNull;
import java.util.Objects;

@Setter
@Getter
public class ShapeDataRequest implements DataRequest {
    @NotNull private String geoHash;
    @NotNull private String dataSet;
    @NotNull private Feature feature;
    @NotNull private FilterDatasetRequest filterDatasetRequest;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShapeDataRequest that = (ShapeDataRequest) o;
        return geoHash.equals(that.geoHash) &&
                dataSet.equals(that.dataSet) &&
                filterDatasetRequest.equals(that.filterDatasetRequest);
    }

    @Override
    public int hashCode() {
        return Objects.hash(geoHash, dataSet, filterDatasetRequest);
    }
}
