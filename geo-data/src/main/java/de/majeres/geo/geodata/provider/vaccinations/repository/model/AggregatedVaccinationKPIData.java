package de.majeres.geo.geodata.provider.vaccinations.repository.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;

@Data
public class AggregatedVaccinationKPIData {
    @Id
    private LocalDate vaccinationDate;
    private Long amount;

}
