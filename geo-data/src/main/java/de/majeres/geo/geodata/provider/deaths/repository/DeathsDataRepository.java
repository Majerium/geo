package de.majeres.geo.geodata.provider.deaths.repository;

import de.majeres.geo.geodata.provider.deaths.repository.model.AggregatedDeathsData;
import de.majeres.geo.geodata.provider.deaths.repository.model.DeathData;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;
import org.springframework.data.mongodb.core.query.Criteria;
import reactor.core.publisher.Flux;

public interface DeathsDataRepository {
    Flux<DeathData> findByPolygon(Criteria filterCriteria, GeoJsonPolygon polygon);
    Flux<AggregatedDeathsData> findByAggregation(Aggregation filterCriteria);
    void saveDeathData(DeathData data);
}
