package de.majeres.geo.geodata.kpi.model;

public interface GeoPoint extends GeoShape {

    double[] getPosition();

    @Override
    default GeoType getType() {
        return GeoType.POINT;
    }
}
