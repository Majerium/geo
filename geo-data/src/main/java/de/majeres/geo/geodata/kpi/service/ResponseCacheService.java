package de.majeres.geo.geodata.kpi.service;

import de.majeres.geo.geodata.facade.model.DataRequest;
import de.majeres.geo.geodata.facade.model.DataResponse;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ResponseCacheService {

    private static Long CACHE_MILLIS = 5 * 60 * 1000l * 10; // New db query after 5 min
    private static Map<DataRequest, Pair<Instant, DataResponse>> CACHE = new ConcurrentHashMap<>();

    public DataResponse getCachedResponse(DataRequest request) {
        return CACHE.get(request).getRight();
    }

    public void cacheResponse(DataRequest request, DataResponse response) {
        CACHE.put(request, Pair.of(Instant.now(), response));
    }

    public boolean useCache(DataRequest request) {
        boolean hasKey = CACHE.containsKey(request);
        boolean isInCachingTime = hasKey && CACHE.get(request).getLeft().isBefore(Instant.now().plusMillis(CACHE_MILLIS));
        if (hasKey && !isInCachingTime) CACHE.remove(request);
        return hasKey && isInCachingTime;
    }

    public void increaseCachingTime(DataRequest request) {
        DataResponse instantShapeDataResponsePair = CACHE.get(request).getRight();
        cacheResponse(request, instantShapeDataResponsePair);
    }
}
