package de.majeres.geo.geodata.provider.corona.services;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBeanBuilder;
import de.majeres.geo.geodata.mapper.Mapper;
import de.majeres.geo.geodata.provider.corona.csv.CsvEntry;
import de.majeres.geo.geodata.provider.corona.model.DataSet;
import de.majeres.geo.geodata.provider.corona.repository.model.CoronaKPIData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
public class RKICsvParserService {

    @Value("classpath:RKI_COVID19.csv")
    private Resource resourceFile;

    public List<CoronaKPIData> loadFromCsv() {

        List<CsvEntry> beans = new ArrayList<>();
        try {
            beans = new CsvToBeanBuilder(new CSVReader(new InputStreamReader(resourceFile.getInputStream())))
                    .withType(CsvEntry.class)
                    .build()
                    .parse();

           return beans.stream().collect(Collectors.groupingBy(CsvEntry::getMeldedatum,
                    Collectors.groupingBy(CsvEntry::getAltersgruppe,
                                    Collectors.groupingBy(CsvEntry::getIdLandkreis,
                                            Collectors.mapping(Function.identity(), Collectors.reducing(CsvEntry::reduce)))))).values().stream().flatMap(y -> y.values().stream().flatMap(x -> x.values().stream())).filter(Optional::isPresent)
                   .map(Optional::get).map((csv) -> Mapper.fromCsvEntry(csv, DataSet.RKI.name())).collect(Collectors.toList());

        } catch (IOException e) {
            log.error("could not parse file", e);
        }
        return new ArrayList<>();
    }
}

