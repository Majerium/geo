package de.majeres.geo.geodata.provider.vaccinations.services;

import de.majeres.geo.geodata.facade.model.DataRequest;
import de.majeres.geo.geodata.facade.model.GraphDataRequest;
import de.majeres.geo.geodata.kpi.model.Kpi;
import de.majeres.geo.geodata.mapper.Mapper;
import de.majeres.geo.geodata.provider.corona.model.DataSet;
import de.majeres.geo.geodata.provider.vaccinations.repository.VaccinationKpiRepository;
import de.majeres.geo.geodata.provider.vaccinations.repository.model.AggregatedVaccinationKPIData;
import de.majeres.geo.geodata.provider.vaccinations.repository.model.VaccinationKPIData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;

@Slf4j
@Service
public class VaccinationKPIService implements KPIService {

    @Autowired
    private VaccinationKpiRepository vaccinationKpiRepository;

    public Mono<Kpi> getValueForPolygon(DataRequest request, GeoJsonPolygon geoJsonPolygon) {
        Flux<VaccinationKPIData> byPolygon = vaccinationKpiRepository.findByPolygon(getFilterCriteria(request), geoJsonPolygon);
        return byPolygon.map(Mapper::fromVaccinationKPIData)
            .collect(Collectors.reducing(Mapper::reduceKpi))
            .map(opt -> opt.orElseGet(Kpi::emptyKPI));
    }

    public Mono<List<Kpi>> getValuesForGraph(GraphDataRequest request) {
        Flux<AggregatedVaccinationKPIData> findByAggregation = vaccinationKpiRepository.findByAggregation(getAggregation(request)).sort(Comparator.comparing(AggregatedVaccinationKPIData::getVaccinationDate));
        return findByAggregation.map(Mapper::fromAggregatedVaccinationKPIData)
                .collectList();
    }

    @Override
    public String getDataset() {
        return DataSet.VACCINATIONS.name();
    }

    // Query aufbau: https://lishman.io/spring-data-mongotemplate-queries
    private Criteria getFilterCriteria(DataRequest request) {
        Criteria criteria = Criteria.where("dataset").is(request.getDataSet());
        // Add dynamic filter
        request.getFilterDatasetRequest().getDynamicFilterRequests().forEach(df -> criteria.and(df.getType()).is(df.getValue()));

        if (hasFrom(request) && hasTo(request)) {
            criteria.and("vaccinationDate").gte(request.getFilterDatasetRequest().getFrom().get()).lte(request.getFilterDatasetRequest().getTo().get());
            return criteria;
        }

        if (request.getFilterDatasetRequest().getFrom().isPresent()) {
            criteria.and("vaccinationDate").gte(request.getFilterDatasetRequest().getFrom().get());
        }

        if (hasTo(request)) {
            criteria.and("vaccinationDate").lte(request.getFilterDatasetRequest().getTo().get());
        }

        return criteria;
    }


    private Aggregation getAggregation(DataRequest request) {
        Criteria criteria = getFilterCriteria(request);

        ProjectionOperation projectStage = Aggregation.project()
            .and("vaccinationDate").as("vaccinationDate")
            .and("amount").as("amount");

        GroupOperation groupBy = group("vaccinationDate")
            .sum("amount").as("amount");

        return Aggregation.newAggregation(Aggregation.match(criteria), projectStage, groupBy);
    }

    private boolean hasFrom(DataRequest request) {
        return request.getFilterDatasetRequest().getFrom().isPresent();
    }

    private boolean hasTo(DataRequest request) {
        return request.getFilterDatasetRequest().getTo().isPresent();
    }
}
