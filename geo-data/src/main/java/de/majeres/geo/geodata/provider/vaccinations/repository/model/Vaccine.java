package de.majeres.geo.geodata.provider.vaccinations.repository.model;

import lombok.AllArgsConstructor;

import java.util.Arrays;

@AllArgsConstructor
public enum Vaccine {
    JANSSEN("Janssen"), COMIRNATY("Comirnaty"), MODERNA("Moderna"), ASTRAZENECA("AstraZeneca"), ERROR("Err");
    private String name;

    public static String fromName(String value) {
        return Arrays.stream(Vaccine.values()).filter(s -> s.name.equals(value)).findFirst().orElse(ERROR).name();
    }
}