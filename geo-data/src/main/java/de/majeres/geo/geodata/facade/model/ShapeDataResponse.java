package de.majeres.geo.geodata.facade.model;

import de.majeres.geo.geodata.kpi.model.Kpi;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ShapeDataResponse implements DataResponse {
    private Kpi kpi;
}
