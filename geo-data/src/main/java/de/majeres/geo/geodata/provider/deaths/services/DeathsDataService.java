package de.majeres.geo.geodata.provider.deaths.services;

import de.majeres.geo.geodata.provider.deaths.repository.DeathsRepository;
import de.majeres.geo.geodata.provider.deaths.repository.model.DeathData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.List;

@Slf4j
@Service
public class DeathsDataService {

    @Autowired
    private DeathsRepository geoRepository;

    @Autowired
    private DeathsCsvParserService deathsCsvParserService;

    @Value("${rki.csv.initialload}")
    private boolean initialLoad;

    @PostConstruct
    public void initialize() {
        if (initialLoad) getAndSaveDeathsData();
    }

    public void getAndSaveDeathsData() {
        List<DeathData> deathData = deathsCsvParserService.loadFromCsv();
        log.info("Inserting Deaths Data");
        Mono.just(deathData)
                .flatMapMany(Flux::fromIterable)
                .subscribe(e -> geoRepository.saveDeathData(e));
    }
}
