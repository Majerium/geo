package de.majeres.geo.geodata.provider.corona.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum DataSet {
    RKI("RKI"),
    VACCINATIONS("VACCINATIONS"),
    DEATHS("DEATHS");
    private String value;
}
