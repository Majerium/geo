package de.majeres.geo.geodata.provider.vaccinations.repository;

import de.majeres.geo.geodata.provider.vaccinations.repository.model.VaccinationKPIData;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VaccinationKpiRepository extends ReactiveMongoRepository<VaccinationKPIData, String>, VaccinationKPIDataRepository {
}
