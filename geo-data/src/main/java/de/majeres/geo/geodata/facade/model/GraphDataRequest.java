package de.majeres.geo.geodata.facade.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Objects;

@Getter
@Setter
public class GraphDataRequest implements DataRequest {
    @NotNull private String dataSet;
    @NotNull private FilterDatasetRequest filterDatasetRequest;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GraphDataRequest that = (GraphDataRequest) o;
        return Objects.equals(dataSet, that.dataSet) &&
                Objects.equals(filterDatasetRequest, that.filterDatasetRequest);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dataSet, filterDatasetRequest);
    }
}
