package de.majeres.geo.geodata.provider.vaccinations.repository.model;


import de.majeres.geo.geodata.kpi.model.GeoPoint;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDate;

@Data
@Document(value = "VaccinationKPIData")
public class VaccinationKPIData implements GeoPoint  {

    @Id
    private long id;
    @Indexed
    private String dataset;
    private LocalDate vaccinationDate;
    @Indexed
    @Field("DISTRICT")
    private Long districtId;
    @Indexed
    @Field("VACCINE")
    private String vaccine;
    @Field("VACCINATION_SERIES")
    private String series;
    private Long amount;

    @GeoSpatialIndexed
    private double[] position;

}
