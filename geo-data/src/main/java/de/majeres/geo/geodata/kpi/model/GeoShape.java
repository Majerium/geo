package de.majeres.geo.geodata.kpi.model;

import java.io.Serializable;

public interface GeoShape extends Serializable {
    GeoType getType();
}
