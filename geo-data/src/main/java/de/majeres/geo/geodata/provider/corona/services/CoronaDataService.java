package de.majeres.geo.geodata.provider.corona.services;

import de.majeres.geo.geodata.provider.corona.repository.CoronaKpiRepository;
import de.majeres.geo.geodata.provider.corona.repository.model.CoronaKPIData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.List;

@Slf4j
@Service
public class CoronaDataService {

    @Autowired
    private CoronaKpiRepository geoRepository;

    @Autowired
    private RKICsvParserService rkiCsvParserService;

    @Value("${rki.csv.initialload}")
    private boolean initialLoad;

    @PostConstruct
    public void initialize() {
        if (initialLoad) getAndSaveRKIData();
    }

    public void getAndSaveRKIData() {
        List<CoronaKPIData> coronaKPIData = rkiCsvParserService.loadFromCsv();
        log.info("Inserting COVID Data");
        Mono.just(coronaKPIData)
                .flatMapMany(Flux::fromIterable)
                .subscribe(e -> geoRepository.persistCoronaKPIData(e));
    }
}
