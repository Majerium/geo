package de.majeres.geo.geodata.provider.corona.csv;

import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvDate;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

//ObjectId,IdBundesland,Bundesland,Landkreis,Altersgruppe,Geschlecht,AnzahlFall,AnzahlTodesfall,Meldedatum,IdLandkreis,Datenstand,NeuerFall,NeuerTodesfall,Refdatum,NeuGenesen,AnzahlGenesen,IstErkrankungsbeginn
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CsvEntry {

    public static CsvEntry reduce(CsvEntry kpi1, CsvEntry kpi2) {
        return new CsvEntry(
                kpi2.getObjectId(),
                kpi2.getLandkreis(),
                kpi2.getAltersgruppe(),
//                kpi2.getGeschlecht(),
                getValueOrZero(() -> kpi1.getAnzahlFall()) + getValueOrZero(() -> kpi2.getAnzahlFall()),
                getValueOrZero(() -> kpi1.getAnzahlTodesfall()) + getValueOrZero(() -> kpi2.getAnzahlTodesfall()),
                kpi2.getMeldedatum(),
                kpi2.getIdLandkreis(),
                getValueOrZero(() -> kpi1.getNeuerFall()) + getValueOrZero(() -> kpi2.getNeuerFall()),
                getValueOrZero(() -> kpi1.getNeuerTodesfall()) + getValueOrZero(() -> kpi2.getNeuerTodesfall()),
                getValueOrZero(() -> kpi1.getNeuGenesen()) + getValueOrZero(() -> kpi2.getNeuGenesen()),
                getValueOrZero(() -> kpi1.getAnzahlGenesen()) + getValueOrZero(() -> kpi2.getAnzahlGenesen()),
                getValueOrZero(() -> kpi1.getIstErkrankungsbeginn()) + getValueOrZero(() -> kpi2.getIstErkrankungsbeginn())
        );
    }

    public static Long getValueOrZero(Supplier<Long> supplier) {
        return Optional.ofNullable(supplier.get()).orElse(0L);
    }

    //IdBundesland
    @CsvBindByPosition(position = 7)
    private String objectId;

    //IdBundesland
    //@CsvBindByPosition(position = 1)
    //private String idBundesland;

    //Bundesland
    //@CsvBindByPosition(position = 2)
    //private String bundesland;

    //Landkreis
    @CsvBindByPosition(position = 2)
    private String landkreis;

    //Altersgruppe
    @CsvBindByPosition(position = 3)
    private String altersgruppe;

    //Geschlecht
    //@CsvBindByPosition(position = 5)
    //private String geschlecht;

    //AnzahlFall
    @CsvBindByPosition(position = 5)
    private Long anzahlFall;

    //AnzahlTodesfall
    @CsvBindByPosition(position = 6)
    private Long anzahlTodesfall;

    //Meldedatum
    @CsvDate(value = "yyyy/MM/dd 00:00:00+00")
    @CsvBindByPosition(position = 8)
    private LocalDate meldedatum;

    //IdLandkreis
    @CsvBindByPosition(position = 9)
    private String idLandkreis;

    //Datenstand
    //@CsvBindByPosition(position = 10)
    //private String datenstand;

    //NeuerFall
    @CsvBindByPosition(position = 11)
    private Long neuerFall;

    //NeuerTodesfall
    @CsvBindByPosition(position = 12)
    private Long neuerTodesfall;

    // Refdatum
    //@CsvDate(value = "yyyy/MM/dd 00:00:00+00")
    //@CsvBindByPosition(position = 13)
    //private LocalDate refdatum;

    //NeuGenesen
    @CsvBindByPosition(position = 14)
    private Long neuGenesen;

    //AnzahlGenesen,
    @CsvBindByPosition(position = 15)
    private Long anzahlGenesen;

    //IstErkrankungsbeginn
    //@CsvDate(value = "yyyy/MM/dd 00:00:00+00")
    @CsvBindByPosition(position = 16)
    private Long istErkrankungsbeginn;

    public static void main(String... args) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        String fileName = "C:\\Users\\majer\\git\\geo\\geo-data\\src\\main\\resources\\RKI_COVID19.csv";

        List<CsvEntry> beans = new CsvToBeanBuilder(new FileReader(fileName))
                .withType(CsvEntry.class)
                .build()
                .parse();

        Map<LocalDate, Map<String, Map<String, Optional<CsvEntry>>>> collect = beans.stream()
                .collect(Collectors.groupingBy(CsvEntry::getMeldedatum,
                        Collectors.groupingBy(CsvEntry::getAltersgruppe,
                                Collectors.groupingBy(CsvEntry::getIdLandkreis,
                                        Collectors.mapping(Function.identity(), Collectors.reducing(CsvEntry::reduce))))));


        List<CsvEntry> collect1 = collect.values().stream().flatMap(y -> y.values().stream().flatMap(x -> x.values().stream())).filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());
        Long aLong = collect.values().stream().flatMap(y -> y.values().stream().flatMap(x -> x.values().stream())).filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList()).stream().map(CsvEntry::getAnzahlTodesfall).reduce((a, b) -> a + b).get();

    }
}
