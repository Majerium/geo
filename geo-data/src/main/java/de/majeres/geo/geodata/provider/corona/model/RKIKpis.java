package de.majeres.geo.geodata.provider.corona.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum RKIKpis {
    COUNT_CASE,
    COUNT_DEATH_CASE,
    COUNT_NEW_CASE,
    COUNT_NEW_DEATH_CASE,
    COUNT_NEW_HEALED,
    COUNT_HEALED,
    COUNT_IS_ILLNESS_START
}
