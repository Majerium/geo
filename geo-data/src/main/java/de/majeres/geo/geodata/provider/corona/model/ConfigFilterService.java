package de.majeres.geo.geodata.provider.corona.model;

import de.majeres.geo.geodata.facade.model.ShapeDataConfigResponse;

public interface ConfigFilterService {
    String forDataset();
    ShapeDataConfigResponse getShapeConfig();
}
