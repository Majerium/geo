package de.majeres.geo.geodata.provider.vaccinations.services;

import de.majeres.geo.geodata.facade.model.DataRequest;
import de.majeres.geo.geodata.facade.model.GraphDataRequest;
import de.majeres.geo.geodata.kpi.model.Kpi;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;
import reactor.core.publisher.Mono;

import java.util.List;

public interface KPIService {
    Mono<Kpi> getValueForPolygon(DataRequest request, GeoJsonPolygon geoJsonPolygon);
    Mono<List<Kpi>> getValuesForGraph(GraphDataRequest request);
    String getDataset();
}
