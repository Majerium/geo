package de.majeres.geo.geodata.provider.vaccinations.services;

import de.majeres.geo.geodata.facade.model.FilterConfig;
import de.majeres.geo.geodata.facade.model.FilterType;
import de.majeres.geo.geodata.facade.model.KpiConfig;
import de.majeres.geo.geodata.facade.model.ShapeDataConfigResponse;
import de.majeres.geo.geodata.provider.corona.model.ConfigFilterService;
import de.majeres.geo.geodata.provider.corona.model.DataSet;
import de.majeres.geo.geodata.provider.vaccinations.repository.model.VaccinationKpis;
import de.majeres.geo.geodata.provider.vaccinations.repository.model.VaccinationSeries;
import de.majeres.geo.geodata.provider.vaccinations.repository.model.Vaccine;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class VaccinationFilterConfig implements ConfigFilterService {

    @Override
    public String forDataset() {
        return DataSet.VACCINATIONS.name();
    }

    @Override
    public ShapeDataConfigResponse getShapeConfig() {
        return new ShapeDataConfigResponse(
            Arrays.asList(7), // States
            Arrays.asList(
                    new FilterConfig(FilterType.VACCINATION_SERIES.name(), getVaccinationSeries(), true, true),
                    new FilterConfig(FilterType.VACCINE.name(), getVaccination(), true, true)
                    //new FilterConfig(FilterType.VACCINATION_DISTRICT.name(), getAgeGroups(), true, true)
            ),
            Arrays.asList(),
            Arrays.asList(
                new KpiConfig(VaccinationKpis.VACCINATIONS_AMOUNT.name(), 0l)
            )
        );
    }

    private List<String> getVaccinationSeries() {
        return Arrays.asList(VaccinationSeries.values()).stream().filter(s-> !VaccinationSeries.SERIES_ERR.equals(s)).map(VaccinationSeries::name).collect(Collectors.toList());
    }

    private List<String> getVaccination() {
        return Arrays.asList(Vaccine.values()).stream().filter(s -> !Vaccine.ERROR.equals(s)).map(Vaccine::name).collect(Collectors.toList());
    }

}
