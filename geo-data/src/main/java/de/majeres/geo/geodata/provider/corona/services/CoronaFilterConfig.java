package de.majeres.geo.geodata.provider.corona.services;

import de.majeres.geo.geodata.facade.model.FilterConfig;
import de.majeres.geo.geodata.facade.model.FilterType;
import de.majeres.geo.geodata.facade.model.KpiConfig;
import de.majeres.geo.geodata.facade.model.ShapeDataConfigResponse;
import de.majeres.geo.geodata.provider.corona.model.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CoronaFilterConfig implements ConfigFilterService {

    private static Max MAX = new Max();

    @Override
    public String forDataset() {
        return DataSet.RKI.name();
    }

    @Override
    public ShapeDataConfigResponse getShapeConfig() {
        return new ShapeDataConfigResponse(
            Arrays.asList(7,9), // States and districts
            Arrays.asList(new FilterConfig(FilterType.AGE_GROUP.name(), getAgeGroups(), true, true)),
            Arrays.asList(new FilterConfig(FilterType.DISTRICT.name(), getDistricts(), false, true)),
            Arrays.asList(
                new KpiConfig(RKIKpis.COUNT_CASE.name(), MAX.maxCase),
                new KpiConfig(RKIKpis.COUNT_DEATH_CASE.name(), MAX.maxDeath),
                new KpiConfig(RKIKpis.COUNT_NEW_CASE.name(), MAX.maxNewCase),
                new KpiConfig(RKIKpis.COUNT_NEW_DEATH_CASE.name(), MAX.maxNewDeath),
                new KpiConfig(RKIKpis.COUNT_NEW_HEALED.name(), MAX.maxNewHealed),
                new KpiConfig(RKIKpis.COUNT_HEALED.name(), MAX.maxHealed),
                new KpiConfig(RKIKpis.COUNT_IS_ILLNESS_START.name(), MAX.getIstErkrankunsbeginn())
            )
        );
    }

    private List<String> getAgeGroups() {
        return Arrays.asList(AgeGroup.values()).stream().filter(o -> !AgeGroup.GROUP_ERR.equals(o)).map(AgeGroup::name).collect(Collectors.toList());
    }

    private List<String> getDistricts() {
        return Arrays.asList(District.values()).stream().map(District::getLandkreis).collect(Collectors.toList());
    }

    @Getter
    @Setter
    @NoArgsConstructor
    private static class Max {
        private Long maxCase;
        private Long maxDeath;
        private Long maxNewCase;
        private Long maxNewDeath;
        private Long maxNewHealed;
        private Long maxHealed;
        private Long istErkrankunsbeginn;
    }

}
