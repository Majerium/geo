package de.majeres.geo.geodata.facade.model;

import de.majeres.geo.geodata.kpi.model.Kpi;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class GraphDataResponse implements DataResponse {
    private List<Kpi> kpis;
}
