package de.majeres.geo.geodata.facade.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Getter
@NoArgsConstructor
@Data
public class FilterDatasetRequest {
    private Optional<LocalDate> from = Optional.empty();
    private Optional<LocalDate> to = Optional.empty();
    private List<DynamicFilterRequest> dynamicFilterRequests = new ArrayList<>();
}
