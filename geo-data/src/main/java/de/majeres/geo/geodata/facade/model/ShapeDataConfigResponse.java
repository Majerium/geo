package de.majeres.geo.geodata.facade.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class ShapeDataConfigResponse {
    private List<Integer> rasterZoomLevel = new ArrayList<>();
    private List<FilterConfig> dropdownFilter = new ArrayList<>();
    private List<FilterConfig> autocompleteFilter = new ArrayList<>();
    private List<KpiConfig> kpiConfigs = new ArrayList<>();
}
