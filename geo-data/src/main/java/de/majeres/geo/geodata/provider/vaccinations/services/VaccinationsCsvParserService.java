package de.majeres.geo.geodata.provider.vaccinations.services;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBeanBuilder;
import de.majeres.geo.geodata.mapper.Mapper;
import de.majeres.geo.geodata.provider.corona.model.DataSet;
import de.majeres.geo.geodata.provider.vaccinations.csv.CsvEntry;
import de.majeres.geo.geodata.provider.vaccinations.repository.model.VaccinationKPIData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
public class VaccinationsCsvParserService {

    @Value("classpath:vaxxinations.csv")
    private Resource resourceFile;

    public List<VaccinationKPIData> loadFromCsv() {

        List<CsvEntry> beans = new ArrayList<>();
        try {
            beans = new CsvToBeanBuilder(new CSVReader(new InputStreamReader(resourceFile.getInputStream())))
                    .withType(CsvEntry.class)
                    .build()
                    .parse();

            List<VaccinationKPIData> collect = beans.stream()
                    .collect(Collectors.groupingBy(CsvEntry::getDate,
                            Collectors.groupingBy(CsvEntry::getDistrictId,
                                    Collectors.groupingBy(CsvEntry::getVaccine,
                                            Collectors.groupingBy(CsvEntry::getSeries,
                                                    Collectors.mapping(Function.identity(), Collectors.reducing(CsvEntry::reduce))))))).values().stream().flatMap(y -> y.values().stream().flatMap(x -> x.values().stream().flatMap(z -> z.values().stream()))).filter(Optional::isPresent)

                    .map(Optional::get).map((csv) -> Mapper.fromCsvEntry(csv, DataSet.VACCINATIONS.name())).collect(Collectors.toList());

            for(int i = 0; i < collect.size(); i++) {
                collect.get(i).setId(i);
            }

            return collect;

        } catch (IOException e) {
            log.error("could not parse file", e);
        }
        return new ArrayList<>();
    }
}

