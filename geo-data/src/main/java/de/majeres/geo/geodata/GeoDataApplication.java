package de.majeres.geo.geodata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.config.WebFluxConfigurer;

@EnableWebFlux
@SpringBootApplication
public class GeoDataApplication implements WebFluxConfigurer {

    public static void main(String[] args) {
        SpringApplication.run(GeoDataApplication.class, args);
    }

}
