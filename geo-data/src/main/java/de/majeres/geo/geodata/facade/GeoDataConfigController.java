package de.majeres.geo.geodata.facade;

import de.majeres.geo.geodata.facade.model.DatasetResponse;
import de.majeres.geo.geodata.facade.model.ShapeDataConfigResponse;
import de.majeres.geo.geodata.provider.corona.model.ConfigFilterService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.Set;

@CrossOrigin
@RestController
@AllArgsConstructor

public class GeoDataConfigController {
    private Set<ConfigFilterService> configServices;

    @ResponseBody
    @RequestMapping(value = "/api/config/datasets", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<DatasetResponse> getDatasets() {
        return Mono.just(new DatasetResponse());
    }

    @ResponseBody
    @RequestMapping(value = "/api/config/dataset/{dataset}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ShapeDataConfigResponse> getShapeDataFilterConfig(@Valid @NotNull @PathVariable String dataset) {
        ShapeDataConfigResponse response = getServiceForDataset(dataset).map(service -> service.getShapeConfig()).orElseGet(() -> new ShapeDataConfigResponse());
        return Mono.just(response);
    }

    private Optional<ConfigFilterService> getServiceForDataset(String dataset) {
        return configServices.stream().filter(ds -> dataset.equals(ds.forDataset())).findFirst();
    }

}
