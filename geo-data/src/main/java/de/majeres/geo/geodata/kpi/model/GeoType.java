package de.majeres.geo.geodata.kpi.model;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum GeoType {
    POINT("Point"),
    POLYGON("Polygon");

    @JsonValue
    String text;
}
