package de.majeres.geo.geodata.provider.deaths.repository;

import de.majeres.geo.geodata.provider.deaths.repository.model.AggregatedDeathsData;
import de.majeres.geo.geodata.provider.deaths.repository.model.DeathData;
import lombok.AllArgsConstructor;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import reactor.core.publisher.Flux;

@AllArgsConstructor
public class DeathsDataRepositoryImpl implements DeathsDataRepository {
    private final ReactiveMongoTemplate mongoTemplate;


    @Override
    public Flux<DeathData> findByPolygon(Criteria filterCriteria, GeoJsonPolygon polygon) {
        Query query = new Query(filterCriteria.and("position").within(polygon));
        return mongoTemplate.find(query, DeathData.class);    }

    @Override
    public Flux<AggregatedDeathsData> findByAggregation(Aggregation aggregation) {
        return mongoTemplate.aggregate(aggregation,"DeathData", AggregatedDeathsData.class);
    }

    @Override
    public void saveDeathData(DeathData data) {
        mongoTemplate.save(data).subscribe();
    }
}
