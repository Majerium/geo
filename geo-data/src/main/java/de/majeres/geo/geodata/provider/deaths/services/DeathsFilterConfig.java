package de.majeres.geo.geodata.provider.deaths.services;

import de.majeres.geo.geodata.facade.model.KpiConfig;
import de.majeres.geo.geodata.facade.model.ShapeDataConfigResponse;
import de.majeres.geo.geodata.provider.corona.model.ConfigFilterService;
import de.majeres.geo.geodata.provider.corona.model.DataSet;
import de.majeres.geo.geodata.provider.deaths.repository.model.DeathsKpis;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class DeathsFilterConfig implements ConfigFilterService {

    @Override
    public String forDataset() {
        return DataSet.DEATHS.name();
    }

    @Override
    public ShapeDataConfigResponse getShapeConfig() {
        return new ShapeDataConfigResponse(
            Arrays.asList(), // Country only
            Arrays.asList(), // no filter
            Arrays.asList(), // no filter
            Arrays.asList(
                new KpiConfig(DeathsKpis.DEATH_COUNT.name(), 0l)
            )
        );
    }
}
