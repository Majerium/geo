package de.majeres.geo.geodata.provider.deaths.csv;

import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvDate;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

//death,count
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CsvEntry {

    //death
    @CsvDate(value = "yyyy-MM-dd")
    @CsvBindByPosition(position = 0)
    private LocalDate date;

    //count
    @CsvBindByPosition(position = 1)
    private Long count;

    public static void main(String... args) throws IOException  {
        String fileName = "C:\\Users\\majer\\git\\geo\\geo-data\\src\\main\\resources\\sterbezahlen.csv";

        List<CsvEntry> beans = new CsvToBeanBuilder(new FileReader(fileName))
                .withType(de.majeres.geo.geodata.provider.vaccinations.csv.CsvEntry.class)
                .build()
                .parse();

        System.out.println(beans);
    }

}

