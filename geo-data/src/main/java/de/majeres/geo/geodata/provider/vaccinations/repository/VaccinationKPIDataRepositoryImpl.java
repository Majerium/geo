package de.majeres.geo.geodata.provider.vaccinations.repository;

import de.majeres.geo.geodata.provider.vaccinations.repository.model.AggregatedVaccinationKPIData;
import de.majeres.geo.geodata.provider.vaccinations.repository.model.VaccinationKPIData;
import lombok.AllArgsConstructor;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import reactor.core.publisher.Flux;

@AllArgsConstructor
public class VaccinationKPIDataRepositoryImpl implements VaccinationKPIDataRepository {
    private final ReactiveMongoTemplate mongoTemplate;

    @Override
    public Flux<VaccinationKPIData> findByPolygon(Criteria filterCriteria, GeoJsonPolygon polygon) {
        Query query = new Query(filterCriteria.and("position").within(polygon));
        return mongoTemplate.find(query, VaccinationKPIData.class);
    }

    @Override
    public Flux<AggregatedVaccinationKPIData> findByAggregation(Aggregation aggregation) {
        return mongoTemplate.aggregate(aggregation,"VaccinationKPIData", AggregatedVaccinationKPIData.class);
    }

    @Override
    public void saveVaccinationKPIData(VaccinationKPIData data) {
        mongoTemplate.save(data).subscribe();
    }
}
