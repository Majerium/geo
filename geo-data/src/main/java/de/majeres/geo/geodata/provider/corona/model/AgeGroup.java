package de.majeres.geo.geodata.provider.corona.model;

import de.majeres.geo.geodata.provider.corona.csv.CsvEntry;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

@AllArgsConstructor
@Getter
public enum AgeGroup {
    GROUP_ERR("error"),
    AGE_GROUP_A00_A04("A00-A04"),
    AGE_GROUP_A05_A14("A05-A14"),
    AGE_GROUP_A15_A34("A15-A34"),
    AGE_GROUP_A35_A59("A35-A59"),
    AGE_GROUP_A60_A79("A60-A79"),
    AGE_GROUP_A80_PLUS("A80+"),
    AGE_GROUP_UNKNOWN("unbekannt");

    private String value;


    public static Optional<AgeGroup> fromCsvEntry(CsvEntry csvEntry) {
        return Arrays.asList(AgeGroup.values()).stream()
                .filter(state -> state.value.equals(csvEntry.getAltersgruppe()))
                .findFirst();
    }
}
