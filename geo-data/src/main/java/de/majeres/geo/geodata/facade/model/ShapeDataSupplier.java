package de.majeres.geo.geodata.facade.model;

import reactor.core.publisher.Mono;

@FunctionalInterface
public interface ShapeDataSupplier {
    Mono<DataResponse> getResponse(DataRequest request);
}
