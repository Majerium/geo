package de.majeres.geo.geodata.provider.corona.services;

import de.majeres.geo.geodata.facade.model.DataRequest;
import de.majeres.geo.geodata.facade.model.GraphDataRequest;
import de.majeres.geo.geodata.kpi.model.Kpi;
import de.majeres.geo.geodata.mapper.Mapper;
import de.majeres.geo.geodata.provider.corona.model.DataSet;
import de.majeres.geo.geodata.provider.corona.repository.CoronaKpiRepository;
import de.majeres.geo.geodata.provider.corona.repository.model.AggregatedCoronaKPIData;
import de.majeres.geo.geodata.provider.corona.repository.model.CoronaKPIData;
import de.majeres.geo.geodata.provider.vaccinations.services.KPIService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;

@Slf4j
@Service
@AllArgsConstructor
public class CoronaKPIService implements KPIService {
    private CoronaKpiRepository coronaRepository;

    public Mono<Kpi> getValueForPolygon(DataRequest request, GeoJsonPolygon geoJsonPolygon) {
        Flux<CoronaKPIData> byPolygon = coronaRepository.findByPolygon(getFilterCriteria(request), geoJsonPolygon);
        return byPolygon.map(Mapper::fromShapeKPIData)
            .collect(Collectors.reducing(Mapper::reduceKpi))
            .map(opt -> opt.orElseGet(Kpi::emptyKPI));
    }

    public Mono<List<Kpi>> getValuesForGraph(GraphDataRequest request) {
        Flux<AggregatedCoronaKPIData> findByAggregation = coronaRepository.findByAggregation(getAggregation(request)).sort(Comparator.comparing(AggregatedCoronaKPIData::getMeldedatum));
        return findByAggregation.map(Mapper::fromGraphKpiData)
                .collectList();
    }
    
    @Override
    public String getDataset() {
        return DataSet.RKI.name();
    }

    // Query aufbau: https://lishman.io/spring-data-mongotemplate-queries
    private Criteria getFilterCriteria(DataRequest request) {
        Criteria criteria = Criteria.where("dataset").is(request.getDataSet());
        // Add dynamic filter
        request.getFilterDatasetRequest().getDynamicFilterRequests().forEach(df -> criteria.and(df.getType()).is(df.getValue()));

        if (hasFrom(request) && hasTo(request)) {
            criteria.and("meldedatum").gte(request.getFilterDatasetRequest().getFrom().get()).lte(request.getFilterDatasetRequest().getTo().get());
            return criteria;
        }

        if (request.getFilterDatasetRequest().getFrom().isPresent()) {
            criteria.and("meldedatum").gte(request.getFilterDatasetRequest().getFrom().get());
        }

        if (hasTo(request)) {
            criteria.and("meldedatum").lte(request.getFilterDatasetRequest().getTo().get());
        }

        return criteria;
    }


    private Aggregation getAggregation(DataRequest request) {
        Criteria criteria = getFilterCriteria(request);

        ProjectionOperation projectStage = Aggregation.project()
            .and("anzahlFall").as("anzahlFall")
            .and("meldedatum").as("meldedatum")
            .and("anzahlTodesfall").as("anzahlTodesfall")
            .and("neuerFall").as("neuerFall")
            .and("neuerTodesfall").as("neuerTodesfall")
            .and("neuGenesen").as("neuGenesen")
            .and("anzahlGenesen").as("anzahlGenesen")
            .and("istErkrankungsbeginn").as("istErkrankungsbeginn");

        GroupOperation groupBy = group("meldedatum")
            .sum("anzahlFall").as("anzahlFall")
            .sum("anzahlTodesfall").as("anzahlTodesfall")
            .sum("neuerFall").as("neuerFall")
            .sum("neuerTodesfall").as("neuerTodesfall")
            .sum("neuGenesen").as("neuGenesen")
            .sum("anzahlGenesen").as("anzahlGenesen")
            .sum("istErkrankungsbeginn").as("istErkrankungsbeginn");

        return Aggregation.newAggregation(Aggregation.match(criteria), projectStage, groupBy);
    }

    private boolean hasFrom(DataRequest request) {
        return request.getFilterDatasetRequest().getFrom().isPresent();
    }

    private boolean hasTo(DataRequest request) {
        return request.getFilterDatasetRequest().getTo().isPresent();
    }
}
