package de.majeres.geo.facade;

import de.majeres.geo.config.ConfigService;
import de.majeres.geo.facade.model.ConfigResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ConfigAPIController {

    @Autowired
    private ConfigService configService;

    @RequestMapping(value = "/api/config/rasterZoomLevels", method = RequestMethod.GET)
    public ResponseEntity<ConfigResponse> getConfiguration() {

        List<ConfigService.ZoomLevel> zoomLevelForRaster = configService.getZoomLevelForRaster();

        return ResponseEntity.ok().body(new ConfigResponse(zoomLevelForRaster));
    }
}
