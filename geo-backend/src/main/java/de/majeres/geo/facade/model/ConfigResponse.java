package de.majeres.geo.facade.model;

import de.majeres.geo.config.ConfigService.ZoomLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class ConfigResponse {

    private List<ZoomLevel> rasterZoomLevel;

}
