package de.majeres.geo.facade.model;

import java.util.Set;

public class CountriesResponse {

    private Set<String> countryNames;

    public CountriesResponse() {
    }

    public CountriesResponse(Set<String> countryNames) {
        this.countryNames = countryNames;
    }

    public Set<String> getCountryNames() {
        return countryNames;
    }
}
