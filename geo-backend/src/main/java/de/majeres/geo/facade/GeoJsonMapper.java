package de.majeres.geo.facade;

import de.majeres.geo.countries.model.raster.country.CountryGrid;
import org.geojson.Feature;
import org.geojson.FeatureCollection;
import org.geojson.LngLatAlt;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.geom.Polygon;
import org.opengis.feature.simple.SimpleFeature;

import java.util.*;

public class GeoJsonMapper {

    private static Map<Class, FeatureSupplier> TYPE_FEATURE_SUPPLIER = new HashMap<>();
    static {
        TYPE_FEATURE_SUPPLIER.put(Polygon.class, GeoJsonMapper::toGeoPolygonFeature);
        TYPE_FEATURE_SUPPLIER.put(MultiPolygon.class, GeoJsonMapper::toGeoMultiPolygonFeature);
    }

    public static FeatureCollection toFeatureCollection(List<Feature> features) {
        FeatureCollection featureCollection = new FeatureCollection();
        featureCollection.setFeatures(features);
        return featureCollection;
    }

    public static Feature toGeoMultiPolygonFeature(SimpleFeature simpleFeature) {
        Feature feature = new Feature();
        MultiPolygon multiPolygon = (MultiPolygon) simpleFeature.getDefaultGeometry();
        org.geojson.MultiPolygon geoPolygon = new org.geojson.MultiPolygon();
        for(int i = 0; i < multiPolygon.getNumGeometries(); i++) {
            geoPolygon.add(toGeoPolygon((Polygon) multiPolygon.getGeometryN(i)));
        }
        feature.setGeometry(geoPolygon);

        setProperties(simpleFeature, feature);

        return feature;
    }

    public static FeatureCollection toGeoFeatureCollectionPolygon(SimpleFeatureCollection geoToolsFeatureCollection) {
        FeatureCollection geoFeatureCollection = new FeatureCollection();
        SimpleFeatureIterator iter = geoToolsFeatureCollection.features();
        SimpleFeature next = null;
        while(iter.hasNext()){
            next = iter.next();
            geoFeatureCollection.add(TYPE_FEATURE_SUPPLIER.get(next.getDefaultGeometry().getClass()).forFeatureType(next));
        }
        return geoFeatureCollection;
    }

    public static Feature toGeoPolygonFeature(SimpleFeature simpleFeature) {
        Feature feature = new Feature();
        Polygon polygon = (Polygon) simpleFeature.getDefaultGeometry();
        feature.setGeometry(toGeoPolygon(polygon));
        setProperties(simpleFeature, feature);
        return feature;
    }

    public static org.geojson.Polygon toGeoPolygon(Polygon polygon) {
        org.geojson.Polygon geoPolygon = new org.geojson.Polygon();
        geoPolygon.add(toGeoCoordinates(polygon.getExteriorRing()));
        for(int i = 0; i < polygon.getNumInteriorRing(); i++) {
            geoPolygon.add(toGeoCoordinates(polygon.getInteriorRingN(i)));
        }
        return geoPolygon;
    }

    public static List<LngLatAlt> toGeoCoordinates(LinearRing polygon) {
        List<LngLatAlt> list = new ArrayList<>();
        for(Coordinate coordinate: polygon.getCoordinates()) {
            list.add(toLngLatAlt(coordinate));
        }
        return list;
    }

    public static void setProperties(SimpleFeature feature, Feature geoFeature){
        Optional.ofNullable(feature).map(f -> f.getAttribute("GEO_HASH")).ifPresent( a-> geoFeature.setProperty("GEO_HASH", a.toString()));
        Optional.ofNullable(feature).map(f -> f.getAttribute("CENTER")).ifPresent( a-> geoFeature.setProperty("CENTER", a.toString()));
        Optional.ofNullable(feature).map(f -> f.getAttribute("KEY")).ifPresent( a-> geoFeature.setProperty("KEY", a));
        Optional.ofNullable(feature).map(f -> f.getAttribute("NAME")).ifPresent( a-> geoFeature.setProperty("NAME", a));
    }

    public static LngLatAlt toLngLatAlt(Coordinate coordinate) {
        LngLatAlt latAlt = new LngLatAlt();
        latAlt.setLatitude(coordinate.y);
        latAlt.setLongitude(coordinate.x);
        latAlt.setAltitude(coordinate.z);
        return latAlt;
    }

    @FunctionalInterface
    interface FeatureSupplier {
        Feature forFeatureType(SimpleFeature polygonal);
    }}
