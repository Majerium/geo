package de.majeres.geo.facade.model;

import com.github.davidmoten.geo.GeoHash;
import de.majeres.geo.countries.model.Center;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.grid.GridElement;
import org.geotools.grid.GridFeatureBuilder;
import org.geotools.grid.PolygonElement;
import org.locationtech.jts.geom.Polygon;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import java.util.Map;

public class CustomGridFeatureBuilder extends GridFeatureBuilder {

    private static final SimpleFeatureType TYPE = new HexTypeBuilder().buildFeatureType();

    public CustomGridFeatureBuilder(){
        super(TYPE);
    }

    @Override
    public void setAttributes(GridElement gridElement, Map<String, Object> attributes) {
        PolygonElement polyEl = (PolygonElement) gridElement;
        attributes.put(Type.CENTER.name(), new Center(polyEl.getCenter().getY(), polyEl.getCenter().getX()));
        attributes.put(Type.GEO_HASH.name(), GeoHash.encodeHash(polyEl.getCenter().getY(), polyEl.getCenter().getX()));
    }

    public enum Type {
        CENTER,
        GEO_HASH
    }

    static class HexTypeBuilder extends SimpleFeatureTypeBuilder {

        HexTypeBuilder(){
            super();
            setName("hextype");
            add("hexagon", Polygon.class, (CoordinateReferenceSystem) null);
            add("CENTER", Center.class);
            add("GEO_HASH", String.class);
        }
    }
}
