package de.majeres.geo.facade;

import de.majeres.geo.countries.CountryService;
import de.majeres.geo.facade.model.CountriesResponse;
import org.geojson.FeatureCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
public class GeoAPIController {

    @Autowired
    private CountryService countryService;

    @RequestMapping(value = "/api/countries/feature/{country}", method = RequestMethod.GET)
    public ResponseEntity<FeatureCollection> getCountryFeature(@NotNull @PathVariable String country) {
        FeatureCollection countryFeatures = countryService.getCountryFeatures(country);
        return ResponseEntity.ok().body(countryFeatures);
    }

    @RequestMapping(value = "/api/countries/raster/{zoom}", method = RequestMethod.GET)
    public ResponseEntity<FeatureCollection> getCountryRaster(
            @NotNull @PathVariable int zoom,
            @NotNull @RequestParam double northEastLat,
            @NotNull @RequestParam double northEastLon,
            @NotNull @RequestParam double southWestLat,
            @NotNull @RequestParam double southWestLon) {
        FeatureCollection raster = countryService.getRaster(zoom, northEastLat, northEastLon, southWestLat, southWestLon);
        return ResponseEntity.ok().body(raster);
    }

    @RequestMapping(value = "/api/countries/countriesNearCenter", method = RequestMethod.GET)
    public ResponseEntity<FeatureCollection> getCountriesNearCenter(
            @NotNull @RequestParam double canterLat,
            @NotNull @RequestParam double centerLon) {

        FeatureCollection raster = countryService.getCountries(canterLat, centerLon);
        return ResponseEntity.ok().body(raster);
    }

    @RequestMapping(value = "/api/countries/allCaountries", method = RequestMethod.GET)
    public ResponseEntity<CountriesResponse> getCountries() {
        CountriesResponse countriesResponse = new CountriesResponse(countryService.getAllCountries());
        return ResponseEntity.ok().body(countriesResponse);
    }

}
