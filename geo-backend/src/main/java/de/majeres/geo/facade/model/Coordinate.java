package de.majeres.geo.facade.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Coordinate {
    private double lon, lat, alt;
}
