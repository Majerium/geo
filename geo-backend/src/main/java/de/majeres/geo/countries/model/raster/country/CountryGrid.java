package de.majeres.geo.countries.model.raster.country;

import org.opengis.feature.simple.SimpleFeature;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class CountryGrid extends ConcurrentHashMap<CountryKey, CountryMultipolygonMap> {

    public static final String COUNTRY_PROPERTY_NAME = "ADMIN";
    public static final String ISO_A2_PROPERTY_NAME = "ISO_A2";
    public static final String ISO_A3_PROPERTY_NAME = "ISO_A3";

    public void addFeature(SimpleFeature feature) {
        CountryKey key = generateKey(feature);
        Optional.ofNullable(this.get(key)).orElseGet(() -> this.createNewPolygonMapForKey(key)).addFeature(key.getCountryName(), feature);
    }

    /**
     * Gibt alle Polygone für einen Countrykey zurück.
     * @param countryKey
     * @return
     */
    public List<SimpleFeature> getFeaturesForCountryKey(CountryKey countryKey) {
        return new ArrayList<>(this.get(countryKey).values());
    }

    public List<SimpleFeature> getFeaturesForCountryName(String countryName) {
        List<SimpleFeature> features = new ArrayList<>();
        this.keySet().stream().filter(key -> key.getCountryName().equals(countryName)).forEach(key ->  features.addAll(this.get(key).values()));
        return features;
    }

    public List<SimpleFeature> getAllFeatures() {
        return this.values().stream()
                .map(Map::values)
                .flatMap(Collection::parallelStream)
                .collect(Collectors.toList());
    }

    private CountryMultipolygonMap createNewPolygonMapForKey(CountryKey key) {
        CountryMultipolygonMap countryGeometryMap = new CountryMultipolygonMap();
        this.put(key, countryGeometryMap);
        return countryGeometryMap;
    }

    private CountryKey generateKey(SimpleFeature feature){
        return new CountryKey(
            (String) feature.getAttribute(COUNTRY_PROPERTY_NAME),
            (String) feature.getAttribute(ISO_A2_PROPERTY_NAME),
            (String) feature.getAttribute(ISO_A3_PROPERTY_NAME)
        );
    }

    public void clear() {
       super.clear();
    }
}
