package de.majeres.geo.countries.modules;

import org.geotools.data.simple.SimpleFeatureCollection;

@FunctionalInterface
public interface FeatureCollectionSupplier {
    SimpleFeatureCollection getFeatureCollection(String shapeFile);
}
