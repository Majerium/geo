package de.majeres.geo.countries.model;

import de.majeres.geo.config.ConfigService;
import org.geotools.data.simple.SimpleFeatureCollection;

@FunctionalInterface
public interface FeatureRasterConsumer {
    void applyRasterForCountryZoomAndFeature(long startTime, String country, ConfigService.ZoomLevel zoom, SimpleFeatureCollection feature);
}
