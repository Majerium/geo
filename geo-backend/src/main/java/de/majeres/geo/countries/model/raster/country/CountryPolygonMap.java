package de.majeres.geo.countries.model.raster.country;

import org.locationtech.jts.geom.Polygon;
import org.opengis.feature.simple.SimpleFeature;

public class CountryPolygonMap extends CountryGeometryMap {

    @Override
    public void addFeature(String countryName, SimpleFeature feature) {
        putFeatureFromPolygon(countryName, (Polygon) feature.getDefaultGeometry());
    }
}
