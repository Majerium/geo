package de.majeres.geo.countries.model.raster;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@Data
public class RasterKey {

    private double latMin, latMax, lonMin, lonMax;

}
