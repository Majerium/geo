package de.majeres.geo.countries.model.raster;

public class RasterConfig {

    /*
   One iteration
    0
   0X0
    0

   Two iterations
     0
    000
   00X00
    000
     0

    ..and so on
    */
    public static int COUNTRY_RASTER_SEARCH_ITERATIONS = 3; //Iterations for raster around the search raster

    public static double COUNTRY_RASTER_WIDTH = 5;
    public static double HEXAGON_RASTER_WIDTH = 0.5;


    public static double MAX_LAT = 90, MAX_LON = 90;
    public static double MIN_LAT = -90, MIN_LON = -90;

}
