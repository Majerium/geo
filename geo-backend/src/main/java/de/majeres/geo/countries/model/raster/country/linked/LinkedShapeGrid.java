package de.majeres.geo.countries.model.raster.country.linked;

import de.majeres.geo.countries.model.Center;
import de.majeres.geo.countries.model.raster.RasterConfig;
import de.majeres.geo.countries.model.raster.country.CountryGrid;
import de.majeres.geo.countries.model.raster.country.CountryPolygonMap;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.opengis.feature.simple.SimpleFeature;

import java.util.*;
import java.util.stream.Collectors;

import static de.majeres.geo.countries.model.raster.RasterConfig.*;

@Slf4j
public class LinkedShapeGrid extends HashMap<LinkedRasterKey, CountryPolygonMap> {

    // This is reused for the staging of the countries in order to have single country parts splitted
    private static final CountryGrid STAGING_TABLE = new CountryGrid();

    public LinkedShapeGrid() {
        log.info("Initializing Linked country grid. This can take some time....");
        this.initializeLinkedRaster();
        this.linkAllRasterKeys();
    }

    public List<SimpleFeature> getCountriesByCenter(double lat, double lon) {
        LinkedRasterKey key = centerToLinkedRasterKey(new Center(lat, lon));
        return  Arrays.asList(LinkedRasterKey.Direction.values()).stream()
                .map(dir -> key.getNeighbourByDir(dir))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(this::getCloseRasterKeys)
                .flatMap(Collection::stream)
                .map(this::get)
                .filter(Objects::nonNull)
                .map(Map::values)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(feature -> this.toFeatureDistance(feature, lat, lon))
                .sorted(Comparator.comparing(Pair::getRight))
                .map(Pair::getKey)
                .limit(50)
                .collect(Collectors.toList());
    }

    private Pair<SimpleFeature, Double> toFeatureDistance(SimpleFeature feature, double lat, double lon) {
        Center center = (Center) feature.getAttribute("CENTER");

        double maxLat = lat > center.getCenterLat() ? lat : center.getCenterLat();
        double minLat = lat < center.getCenterLat() ? lat : center.getCenterLat();
        double maxLon = lon > center.getCenterLon() ? lon : center.getCenterLon();
        double minLon = lon < center.getCenterLon() ? lon : center.getCenterLon();

        double distanceLat = maxLat - minLat;
        double distanceLon = maxLon - minLon;

        double cSquare = (distanceLat * distanceLat) + (distanceLon * distanceLon);

        return Pair.of(feature, Math.sqrt(cSquare)); // Square is not needed actually
    }

    public void addFeature(SimpleFeature feature) {
        STAGING_TABLE.addFeature(feature);
    }

    public void linkShapeRaster() {
        STAGING_TABLE.getAllFeatures().stream().map(this::toKeyAndSimpleFeaturePair).forEach(this::addFeature);
        STAGING_TABLE.clear();
    }

    public List<SimpleFeature> getFeaturesForCountryName(String countryName) {
        return this.values().stream()
                .filter(Objects::nonNull)
                .map(Map::values)
                .flatMap(Collection::parallelStream)
                .filter(f -> countryName.equals((String) f.getAttribute("NAME")))
                .collect(Collectors.toList());
    }

    public Set<String> getAllCountryNames() {
        return this.values().stream()
                .filter(Objects::nonNull)
                .map(Map::values)
                .flatMap(Collection::parallelStream)
                .map(f -> (String) f.getAttribute("NAME"))
                .collect(Collectors.toSet());
    }

    /**
     * Sollte alle naheliegenden Keys aus dem Raster finden.
     * @param key
     * @return
     */
    private Set<LinkedRasterKey> getCloseRasterKeys(LinkedRasterKey key) {
        Set<LinkedRasterKey> keys = new HashSet<>();
        keys.add(key);
        Set<LinkedRasterKey> tempKeys = null;
        List<LinkedRasterKey> keysNearKey;
        for(int i = 0; i < RasterConfig.COUNTRY_RASTER_SEARCH_ITERATIONS; i++) {
            tempKeys = new HashSet<>(keys);
            for(LinkedRasterKey hashKey : tempKeys) {
                getKeysNearKey(hashKey).forEach(closeKey -> keys.add(closeKey));
            }
        }
        return keys;
    }

    private List<LinkedRasterKey> getKeysNearKey(LinkedRasterKey key) {
        return  Arrays.asList(LinkedRasterKey.Direction.values()).stream()
                .map(dir -> key.getNeighbourByDir(dir))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    private void addFeature(Pair<LinkedRasterKey, SimpleFeature> pair) {
        Optional.ofNullable(this.get(pair.getLeft()))
                .orElseGet(() -> this.createNewPolygonMapForKey(pair.getLeft()))
                .addFeature((String) pair.getRight().getAttribute("NAME"), pair.getRight());
    }

    private CountryPolygonMap createNewPolygonMapForKey(LinkedRasterKey key) {
        CountryPolygonMap countryGeometryMap = new CountryPolygonMap();
        this.put(key, countryGeometryMap);
        return countryGeometryMap;
    }

    private Pair<LinkedRasterKey, SimpleFeature> toKeyAndSimpleFeaturePair(SimpleFeature feature) {
        Center center = (Center) feature.getAttribute("CENTER");
        return Pair.of(centerToLinkedRasterKey(center), feature);
    }

    private LinkedRasterKey centerToLinkedRasterKey(Center center) {
        double minLat = center.getCenterLat() - (center.getCenterLat() % COUNTRY_RASTER_WIDTH);
        double maxLat = minLat + COUNTRY_RASTER_WIDTH;
        double minLon = center.getCenterLon() - (center.getCenterLon() % RasterConfig.COUNTRY_RASTER_WIDTH);
        double maxLon = minLon + COUNTRY_RASTER_WIDTH;
        return new LinkedRasterKey(minLat, maxLat, minLon, maxLon);
    }

    private void initializeLinkedRaster() {
        for(double latIndex = MIN_LAT; latIndex < MAX_LAT; latIndex += RasterConfig.COUNTRY_RASTER_WIDTH) {
            for (double lonIndex = MIN_LON; lonIndex < MAX_LON; lonIndex += RasterConfig.COUNTRY_RASTER_WIDTH) {
                this.put(new LinkedRasterKey(latIndex, latIndex + RasterConfig.COUNTRY_RASTER_WIDTH, lonIndex, lonIndex + RasterConfig.COUNTRY_RASTER_WIDTH), null);
            }
        }
    }

    private void linkAllRasterKeys() {
        for(double latIndex = MIN_LAT; latIndex < MAX_LAT; latIndex += RasterConfig.COUNTRY_RASTER_WIDTH) {
            for (double lonIndex = MIN_LON; lonIndex < MAX_LON; lonIndex += RasterConfig.COUNTRY_RASTER_WIDTH) {
                linkNeighbours(new LinkedRasterKey(latIndex, latIndex + RasterConfig.COUNTRY_RASTER_WIDTH, lonIndex, lonIndex + RasterConfig.COUNTRY_RASTER_WIDTH));
            }
        }
    }

    private void linkNeighbours(LinkedRasterKey key) {
        //key.debug("X");
        for(LinkedRasterKey.Direction dir: LinkedRasterKey.Direction.values()) {
            Optional<LinkedRasterKey> neighbourByDir = key.getNeighbourByDir(dir);
            if(neighbourByDir.isPresent() && containsKey(neighbourByDir.get())) {
                LinkedRasterKey actualNeighbourKey = getActualKey(neighbourByDir.get());
                // actualNeighbourKey.debug("O");
                mergeKeys(actualNeighbourKey, neighbourByDir.get()); // weist dem neigbour den besuchten key zu
                put(actualNeighbourKey, null);
            }
        }
    }
    // Lat = Y Long = X
    private void mergeKeys(LinkedRasterKey actualKey, LinkedRasterKey toBeMerged) {

        if(!actualKey.getUp().isPresent()) {
            actualKey.setUp(toBeMerged.getUp());
        }

        if(!actualKey.getDown().isPresent()) {
            actualKey.setDown(toBeMerged.getDown());
        }

        if(!actualKey.getRight().isPresent()) {
            actualKey.setRight(toBeMerged.getRight());
        }

        if(!actualKey.getLeft().isPresent()) {
            actualKey.setLeft(toBeMerged.getLeft());
        }
    }

    private LinkedRasterKey getActualKey(LinkedRasterKey neighbourByDir) {
        return this.keySet().stream()
                .filter(x -> x.equals(neighbourByDir))
                .findFirst()
                .get();
    }

}
