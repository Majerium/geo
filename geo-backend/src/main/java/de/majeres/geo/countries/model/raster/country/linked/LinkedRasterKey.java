package de.majeres.geo.countries.model.raster.country.linked;

import de.majeres.geo.countries.model.raster.RasterConfig;
import de.majeres.geo.countries.model.raster.RasterKey;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;
import java.util.function.Function;

import static de.majeres.geo.countries.model.raster.RasterConfig.*;

@Slf4j
@Getter
@Setter
public class LinkedRasterKey extends RasterKey {

    private Optional<LinkedRasterKey> up = Optional.empty(), right = Optional.empty(), down = Optional.empty(), left = Optional.empty();

    public LinkedRasterKey(double latMin, double latMax, double lonMin, double lonMax) {
        super(latMin, latMax, lonMin, lonMax);
    }

    public Optional<LinkedRasterKey> getNeighbourByDir(Direction dir) {
        switch (dir) {
            case UP: return up.isPresent() ? up : linkAndReturnKey(Direction.UP, Direction.UP.NEIGHBOUR.apply(this));
            case DOWN: return down.isPresent() ? down : linkAndReturnKey(Direction.DOWN, Direction.DOWN.NEIGHBOUR.apply(this));
            case LEFT: return left.isPresent() ? left : linkAndReturnKey(Direction.LEFT, Direction.LEFT.NEIGHBOUR.apply(this));
            case RIGHT: return right.isPresent() ? right : linkAndReturnKey(Direction.RIGHT, Direction.RIGHT.NEIGHBOUR.apply(this));
        }
        return null; //Should never happen
    }

    public Optional<LinkedRasterKey> getNeighbourByDirEmpty(Direction dir) {
        switch (dir) {
            case UP: return up;
            case DOWN: return down;
            case LEFT: return left;
            case RIGHT: return right;
        }
        return Optional.empty(); //Should never happen
    }

    private Optional<LinkedRasterKey> linkAndReturnKey(Direction dirToLink, LinkedRasterKey neighbour) {
        switch (dirToLink) {
            case UP: {
                if(neighbour.getLatMax() <= MAX_LAT) {
                    this.up = Optional.of(neighbour);
                    neighbour.down = Optional.of(this);
                    return Optional.of(neighbour);
                }
                break;
            }
            case DOWN:  {
                if(MIN_LAT <= neighbour.getLatMin()) {
                    this.down = Optional.of(neighbour);
                    neighbour.up = Optional.of(this);
                    return Optional.of(neighbour);
                }
                break;
            }
            case LEFT:  {
                if(MIN_LON <= neighbour.getLonMin()) {
                    this.left = Optional.of(neighbour);
                    neighbour.right = Optional.of(this);
                    return Optional.of(neighbour);
                }
                break;
            }
            case RIGHT:  {
                if(neighbour.getLonMax() <= MAX_LON) {
                    this.right = Optional.of(neighbour);
                    neighbour.left = Optional.of(this);
                    return Optional.of(neighbour);
                }
                break;
            }
        }
        return Optional.empty();
    }


    // lat = y
    // lon = x
    // Das Debugging in der Form funktioniert nur bei einer Seitenlänge von 5 also 90 * 2 / 5 = 36 raster breite
    public void debug(String value) {
        double sideLen = MAX_LAT * 2 / COUNTRY_RASTER_WIDTH;
        if(this.getLatMin() == MIN_LAT)  {// Minimum y also unterste reihe
            if(this.getLonMin() == MIN_LON) {  // Erstes ganz unten {
                log.info("===============");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("["+value+"][ ][ ][ ][ ]");
            }else if(this.getLonMin() == (MIN_LON + COUNTRY_RASTER_WIDTH )) {
                log.info("===============");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ]["+value+"][ ][ ][ ]");
            }else if (this.getLonMin() == (MIN_LON + (COUNTRY_RASTER_WIDTH * 2))) {
                log.info("===============");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ]["+value+"][ ][ ]");
            }else if (this.getLonMin() == (MIN_LON + (COUNTRY_RASTER_WIDTH * 3))) {
                log.info("===============");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ]["+value+"][ ]");
            }else if (this.getLonMin() == (MIN_LON + (COUNTRY_RASTER_WIDTH * 4))) {
                log.info("===============");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ]["+value+"]");
            }
        }else if(this.getLatMin() == (MIN_LAT + COUNTRY_RASTER_WIDTH)) { //Mittlere reihe
            if(this.getLonMin() == MIN_LON) {  // Erstes mitte {
                log.info("===============");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("["+value+"][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
            }else if(this.getLonMin() == (MIN_LON + COUNTRY_RASTER_WIDTH )) {
                log.info("===============");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ]["+value+"][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
            }else if (this.getLonMin() == (MIN_LON + (COUNTRY_RASTER_WIDTH * 2))) {
                log.info("===============");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ]["+value+"][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
            }else if (this.getLonMin() == (MIN_LON + (COUNTRY_RASTER_WIDTH * 3))) {
                log.info("===============");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ]["+value+"][ ]");
                log.info("[ ][ ][ ][ ][ ]");
            }else if (this.getLonMin() == (MIN_LON + (COUNTRY_RASTER_WIDTH * 4))) {
                log.info("===============");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ]["+value+"]");
                log.info("[ ][ ][ ][ ][ ]");
            }
        } else if(this.getLatMin() == (MIN_LAT + (COUNTRY_RASTER_WIDTH * 2))) { // Oberste reihe
            if(this.getLonMin() == MIN_LON) {  // Erstes ganz unten {
                log.info("===============");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("["+value+"][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
            }else if(this.getLonMin() == (MIN_LON + COUNTRY_RASTER_WIDTH )) {
                log.info("===============");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ]["+value+"][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
            }else if (this.getLonMin() == (MIN_LON + (COUNTRY_RASTER_WIDTH * 2))) {
                log.info("===============");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ]["+value+"][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
            }else if (this.getLonMin() == (MIN_LON + (COUNTRY_RASTER_WIDTH * 3))) {
                log.info("===============");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ]["+value+"][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
            }else if (this.getLonMin() == (MIN_LON + (COUNTRY_RASTER_WIDTH * 4))) {
                log.info("===============");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ]["+value+"]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
            }

        }else if(this.getLatMin() == (MIN_LAT + (COUNTRY_RASTER_WIDTH * 3))) { // Oberste reihe
            if(this.getLonMin() == MIN_LON) {  // Erstes ganz unten {
                log.info("===============");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("["+value+"][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
            }else if(this.getLonMin() == (MIN_LON + COUNTRY_RASTER_WIDTH )) {
                log.info("===============");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ]["+value+"][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
            }else if (this.getLonMin() == (MIN_LON + (COUNTRY_RASTER_WIDTH * 2))) {
                log.info("===============");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ]["+value+"][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
            }else if (this.getLonMin() == (MIN_LON + (COUNTRY_RASTER_WIDTH * 3))) {
                log.info("===============");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ]["+value+"][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
            }else if (this.getLonMin() == (MIN_LON + (COUNTRY_RASTER_WIDTH * 4))) {
                log.info("===============");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ]["+value+"]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
            }
            }else if(this.getLatMin() == (MIN_LAT + (COUNTRY_RASTER_WIDTH * 4))) { // Oberste reihe
            if(this.getLonMin() == MIN_LON) {  // Erstes ganz unten {
                log.info("===============");
                log.info("["+value+"][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
            }else if(this.getLonMin() == (MIN_LON + COUNTRY_RASTER_WIDTH )) {
                log.info("===============");
                log.info("[ ]["+value+"][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
            }else if (this.getLonMin() == (MIN_LON + (COUNTRY_RASTER_WIDTH * 2))) {
                log.info("===============");
                log.info("[ ][ ]["+value+"][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
            }else if (this.getLonMin() == (MIN_LON + (COUNTRY_RASTER_WIDTH * 3))) {
                log.info("===============");
                log.info("[ ][ ][ ]["+value+"][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
            }else if (this.getLonMin() == (MIN_LON + (COUNTRY_RASTER_WIDTH * 4))) {
                log.info("===============");
                log.info("[ ][ ][ ][ ]["+value+"]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
                log.info("[ ][ ][ ][ ][ ]");
            }
        }

    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Getter
    @AllArgsConstructor
    public enum Direction {
        UP((key) -> new LinkedRasterKey(key.getLatMin() + RasterConfig.COUNTRY_RASTER_WIDTH, key.getLatMax() + RasterConfig.COUNTRY_RASTER_WIDTH, key.getLonMin(), key.getLonMax())),
        RIGHT((key) -> new LinkedRasterKey(key.getLatMin(), key.getLatMax(), key.getLonMin() + RasterConfig.COUNTRY_RASTER_WIDTH, key.getLonMax() + RasterConfig.COUNTRY_RASTER_WIDTH)),
        DOWN((key) -> new LinkedRasterKey(key.getLatMin() - RasterConfig.COUNTRY_RASTER_WIDTH, key.getLatMax() - RasterConfig.COUNTRY_RASTER_WIDTH, key.getLonMin(), key.getLonMax())),
        LEFT((key) -> new LinkedRasterKey(key.getLatMin(), key.getLatMax(), key.getLonMin() - RasterConfig.COUNTRY_RASTER_WIDTH, key.getLonMax() - RasterConfig.COUNTRY_RASTER_WIDTH));

        public final Function<LinkedRasterKey, LinkedRasterKey> NEIGHBOUR;
    }
}
