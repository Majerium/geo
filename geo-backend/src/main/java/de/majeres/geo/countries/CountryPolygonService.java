package de.majeres.geo.countries;

import de.majeres.geo.config.ConfigService;
import de.majeres.geo.countries.model.FeatureRasterConsumer;
import de.majeres.geo.countries.modules.CountryShapeModule;
import de.majeres.geo.facade.model.CustomGridFeatureBuilder;
import lombok.extern.slf4j.Slf4j;
import org.geotools.data.DataUtilities;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.grid.Grids;
import org.opengis.feature.simple.SimpleFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

@Service
@Slf4j
public class CountryPolygonService {

    private static CustomGridFeatureBuilder GRID_BUILDER = new CustomGridFeatureBuilder();

    @Autowired
    private Set<CountryShapeModule> countryModules;

    //@Async("rasterCreatorSmall")
    public void smallRaster(SimpleFeature feature, ConfigService.ZoomLevel zoom, String countryname, FeatureRasterConsumer consumer) throws IOException {
        this.createHexGridsForCountry(feature, zoom, countryname, consumer);
    }

    //@Async("rasterCreatorTall")
    public void tallRaster(SimpleFeature feature, ConfigService.ZoomLevel zoom, String countryname, FeatureRasterConsumer consumer) throws IOException {
        this.createHexGridsForCountry(feature, zoom, countryname, consumer);
    }

    private void createHexGridsForCountry(SimpleFeature feature, ConfigService.ZoomLevel zoom, String countryName, FeatureRasterConsumer consumer) throws IOException {
        long start = new Date().getTime();
        if (!hasCustomCountryModuleForCountryAndZoom(countryName, zoom)) createHexagonRaster(feature, zoom, countryName, consumer, start);
        else createCustomShapeRaster(start, zoom, countryName, consumer);
    }

    private void createCustomShapeRaster(long start, ConfigService.ZoomLevel zoom, String countryName, FeatureRasterConsumer consumer) {
        CompletableFuture.completedFuture(countryModules.stream().filter(module -> module.getCountyName().equals(countryName)).findFirst().get().getFeatureCollectionForZoom(zoom))
                .thenAccept(featureCollection -> consumer.applyRasterForCountryZoomAndFeature(start, countryName, zoom, featureCollection));
    }

    private boolean hasCustomCountryModuleForCountryAndZoom(String country, ConfigService.ZoomLevel zoom) {
        return countryModules.stream()
                .filter(module -> module.hasCustomShapesForZoomlevelAndCountry(country, zoom))
                .findFirst()
                .isPresent();
    }

    private void createHexagonRaster(SimpleFeature feature, ConfigService.ZoomLevel zoom, String countryname, FeatureRasterConsumer consumer, long start) throws IOException {
        ReferencedEnvelope gridBounds =
                new ReferencedEnvelope(feature.getBounds().getMinX(), feature.getBounds().getMaxX(), feature.getBounds().getMinY(), feature.getBounds().getMaxY(), null);

        CompletableFuture.completedFuture(filterFeaturesByCountryBoarders(feature, Grids.createHexagonalGrid(gridBounds, zoom.SIDE_LENGTH, -1, GRID_BUILDER).getFeatures(), zoom))
                .thenAccept(featureCollection -> consumer.applyRasterForCountryZoomAndFeature(start, countryname, zoom, featureCollection));
    }

    private SimpleFeatureCollection filterFeaturesByCountryBoarders(SimpleFeature countryFeature, SimpleFeatureCollection raster, ConfigService.ZoomLevel zoom) {

        int featuresToProcess = raster.size();

        log.info("Creating feature collection {}-{} for {} elements", countryFeature.getAttribute(0), zoom.ZOOM, featuresToProcess);

        List<SimpleFeature> filteredPolygons = new ArrayList<>();
        org.locationtech.jts.geom.MultiPolygon multiPolygon = (org.locationtech.jts.geom.MultiPolygon) countryFeature.getDefaultGeometry();

        DataUtilities.collection(raster).parallelStream()
                .filter(feature -> multiPolygon.contains((org.locationtech.jts.geom.Polygon) feature.getDefaultGeometry()))
                .forEach(feature -> filteredPolygons.add(feature)
        );

        return DataUtilities.collection(filteredPolygons);
    }
}

