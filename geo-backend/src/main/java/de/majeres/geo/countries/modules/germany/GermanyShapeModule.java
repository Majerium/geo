package de.majeres.geo.countries.modules.germany;

import de.majeres.geo.config.ConfigService;
import de.majeres.geo.countries.model.PolygonFeatureBuilder;
import de.majeres.geo.countries.modules.CountryShapeModule;
import de.majeres.geo.countries.modules.FeatureCollectionSupplier;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.geotools.data.DataUtilities;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.geojson.feature.FeatureJSON;
import org.locationtech.jts.geom.Polygonal;
import org.opengis.feature.simple.SimpleFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

@Slf4j
@Service
public class GermanyShapeModule implements CountryShapeModule {

    final String country = "Germany";
    final String GERMANY_STATE_SHAPES_JSON_PATH = "classpath:germany/bundeslaender_simplify200.geojson";
    final String GERMANY_DISTRICT_SHAPES_JSON_PATH = "classpath:germany/gemeinden_simplify200.geojson";

    private Map<ConfigService.ZoomLevel, Pair<String, FeatureCollectionSupplier>> SUPPLIER = new HashMap();

    @Autowired
    private ResourceLoader resourceLoader;

    @PostConstruct
    public void loadConfigurations() {
        SUPPLIER.put(ConfigService.ZoomLevel.SEVEN, Pair.of(GERMANY_STATE_SHAPES_JSON_PATH, (x) -> initiateDistrictMap(x)));
        SUPPLIER.put(ConfigService.ZoomLevel.NINE, Pair.of(GERMANY_DISTRICT_SHAPES_JSON_PATH, (x) -> initiateDistrictMap(x)));
        log.info("Loading custom shapes for Germany {}", SUPPLIER.keySet());
    }

    private SimpleFeatureCollection initiateDistrictMap(String shapeFile) {

        Resource resource = resourceLoader.getResource(shapeFile);

        FeatureJSON io = new FeatureJSON();

        List<SimpleFeature> features = new ArrayList<>();

        try(FeatureIterator<SimpleFeature> featureIterator = io.streamFeatureCollection(resource.getInputStream())) {
            while(featureIterator.hasNext()) addFeature(features, featureIterator.next(), country);
        } catch (IOException e) {
            log.error("Could not load shapes for {}", shapeFile, e);
        }

        return DataUtilities.collection(features);
    }

    public SimpleFeatureCollection getFeatureCollectionForZoom(ConfigService.ZoomLevel zoom) {
        Pair<String, FeatureCollectionSupplier> supplier = SUPPLIER.get(zoom);
        return supplier.getRight().getFeatureCollection(supplier.getLeft());
    }

    @Override
    public boolean hasCustomShapesForZoomlevelAndCountry(String country, ConfigService.ZoomLevel zoomLevel) {
        return this.country.equals(country) && SUPPLIER.keySet().contains(zoomLevel);
    }

    @Override
    public String getCountyName() {
        return country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GermanyShapeModule that = (GermanyShapeModule) o;
        return country.equals(that.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(country);
    }

}
