package de.majeres.geo.countries.model.raster.latlon;

import de.majeres.geo.config.ConfigService;
import de.majeres.geo.countries.model.raster.RasterKey;
import de.majeres.geo.countries.model.raster.RasterConfig;
import org.geotools.data.DataUtilities;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.opengis.feature.simple.SimpleFeature;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class HexagonGrid extends ConcurrentHashMap<HexagonKey, RasterPolygonMap> {

    public SimpleFeatureCollection getByCoordinates(int zoom, double northEastLat, double northEastLon, double southWestLat, double southWestLon) {
       return DataUtilities.collection(containsKey(new HexagonKey(zoom)) ?
               byCoordinates(zoom, northEastLat, northEastLon, southWestLat, southWestLon) :
               Arrays.asList()
       );
    }

    private List<SimpleFeature> byCoordinates(int zoom, double northEastLat, double northEastLon, double southWestLat, double southWestLon) {

        List<SimpleFeature> simpleFeatureList = new ArrayList<>();

        RasterPolygonMap rasterPolygonMap = get(new HexagonKey(zoom));

        double minLat = southWestLat - (southWestLat % RasterConfig.HEXAGON_RASTER_WIDTH);
        double maxLat = (northEastLat - (northEastLat % RasterConfig.HEXAGON_RASTER_WIDTH)) + (northEastLat % RasterConfig.HEXAGON_RASTER_WIDTH == 0 ? 0 : RasterConfig.HEXAGON_RASTER_WIDTH);

        double minLon = southWestLon - (southWestLon % RasterConfig.HEXAGON_RASTER_WIDTH);
        double maxLon = (northEastLon - (northEastLon % RasterConfig.HEXAGON_RASTER_WIDTH)) + (northEastLon % RasterConfig.HEXAGON_RASTER_WIDTH == 0d ? 0 : RasterConfig.HEXAGON_RASTER_WIDTH);

        List<RasterKey> rasterKeys = calculateRasterKeys(minLat, maxLat, minLon, maxLon);

        rasterKeys.forEach(key -> addFeatureCollectionToFeatureList(simpleFeatureList, rasterPolygonMap.get(key)));

        return simpleFeatureList;
    }

    private void addFeatureCollectionToFeatureList(List<SimpleFeature> simpleFeatureList, SimpleFeatureCollection featureCollection) {
        if(featureCollection != null){
            try(SimpleFeatureIterator featuresIter = featureCollection.features()) {
                while (featuresIter.hasNext()) {
                    simpleFeatureList.add(featuresIter.next());
                }
            }
        }
    }

    private List<RasterKey> calculateRasterKeys(double minLat, double maxLat, double minLon, double maxLon) {
        List<RasterKey> rasterKeys = new ArrayList<>();
        for(double indexLat = minLat; indexLat <= (maxLat - RasterConfig.HEXAGON_RASTER_WIDTH); indexLat += RasterConfig.HEXAGON_RASTER_WIDTH) {
            for(double indexLon = minLon; indexLon <= (maxLon - RasterConfig.HEXAGON_RASTER_WIDTH); indexLon += RasterConfig.HEXAGON_RASTER_WIDTH) {
                rasterKeys.add( new RasterKey(indexLat, indexLat + RasterConfig.HEXAGON_RASTER_WIDTH, indexLon, indexLon + RasterConfig.HEXAGON_RASTER_WIDTH));
            }
        }
        return rasterKeys;
    }

    public void addFeature(ConfigService.ZoomLevel zoom, SimpleFeatureCollection featureCollection) {
        HexagonKey key = new HexagonKey(zoom.ZOOM);
        if(!containsKey(key)) put(key, new RasterPolygonMap());
        get(key).addFeature(featureCollection);
    }
}
