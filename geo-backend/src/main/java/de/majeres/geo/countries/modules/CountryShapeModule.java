package de.majeres.geo.countries.modules;

import de.majeres.geo.config.ConfigService;
import de.majeres.geo.countries.model.PolygonFeatureBuilder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.geom.Polygonal;
import org.opengis.feature.simple.SimpleFeature;

import java.util.List;
import java.util.Optional;

public interface CountryShapeModule {


    default boolean hasCustomShapesForZoomlevelAndCountry(String country, ConfigService.ZoomLevel zoomLevel) {
        return false;
    }

    SimpleFeatureCollection getFeatureCollectionForZoom(ConfigService.ZoomLevel zoom);

    String getCountyName();

    boolean equals(Object o);

    int hashCode();

    default void addFeature(List<SimpleFeature> features, SimpleFeature feature, String country)  {
        switch (feature.getDefaultGeometry().getClass().getSimpleName()) {
            case "MultiPolygon": addMultiPolygonFeature(features, feature, country); break;
            case "Polygon": addPolygonFeature(features, (Polygon) feature.getDefaultGeometry(), country, Optional.of((String)feature.getAttribute("AGS"))); break;
        }
    }

    default void addMultiPolygonFeature(List<SimpleFeature> features, SimpleFeature feature, String country){
        MultiPolygon multiPolygon = (MultiPolygon) feature.getDefaultGeometry();
        Polygon polygon;
        for(int i = 0; i < multiPolygon.getNumGeometries(); i++) {
            polygon = (Polygon) multiPolygon.getGeometryN(i);
            addPolygonFeature(features, polygon, country, Optional.empty());
        }
    }

    default void addPolygonFeature(List<SimpleFeature> features, Polygon polygon, String country, Optional<String> ags) {
        PolygonFeatureBuilder builder = new PolygonFeatureBuilder();
        builder.addPolygon(polygon, country, 1);
        SimpleFeature simpleFeature = builder.getSimpleFeature(null);
        //ags.ifPresent(a -> System.out.println(String.format("GROUP_%s(\"%s\", new double[]{%s, %s}),", bla.inc(), a, builder.center.getCenterLon(), builder.center.getCenterLat())));
        features.add(simpleFeature);
    }

    /*
    static Bla bla = new Bla();
    @Getter
    class Bla {
        int i = 0;
        public int inc() {
            return i++;
        }
    }
    */

}
