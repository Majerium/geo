package de.majeres.geo.countries.model.raster.country;

import de.majeres.geo.countries.model.PolygonFeatureBuilder;
import org.locationtech.jts.geom.Polygon;
import org.opengis.feature.simple.SimpleFeature;

import java.util.concurrent.ConcurrentHashMap;

public abstract class CountryGeometryMap extends ConcurrentHashMap<Integer, SimpleFeature> {

    // Add an feature for each polygon in the country
    public  abstract void addFeature(String countryName, SimpleFeature feature);

    protected void putFeatureFromPolygon(String countryName, Polygon feature) {
        Integer nextKey = getNextKey();
        PolygonFeatureBuilder builder = new PolygonFeatureBuilder();
        builder.addPolygon(feature, countryName, nextKey);
        this.put(nextKey, builder.getSimpleFeature(nextKey.toString()));
    }

    private Integer getNextKey() {
        return this.keySet().size();
    }

}
