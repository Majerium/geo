package de.majeres.geo.countries.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@AllArgsConstructor
@Data
public class Center {
    double centerLat;
    double centerLon;
}
