package de.majeres.geo.countries.model.raster.latlon;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class HexagonKey {

    private int zoom;

    @Override
    public String toString() {
        return "RasterKey{" +
                "zoom=" + zoom +
                '}';
    }

}
