package de.majeres.geo.countries.model.raster.country;

import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.geom.Polygon;
import org.opengis.feature.simple.SimpleFeature;

public class CountryMultipolygonMap extends CountryGeometryMap {

    @Override
    public void addFeature(String countryName, SimpleFeature feature) {
        MultiPolygon multiPolygon = (MultiPolygon) feature.getDefaultGeometry();
        for(int i = 0; i < multiPolygon.getNumGeometries(); i++) {
            putFeatureFromPolygon(countryName, (Polygon) multiPolygon.getGeometryN(i));
        }
    }
}
