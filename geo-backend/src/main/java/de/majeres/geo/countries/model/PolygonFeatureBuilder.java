package de.majeres.geo.countries.model;

import com.github.davidmoten.geo.GeoHash;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.geom.Polygonal;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import java.util.HashMap;
import java.util.Map;

public class PolygonFeatureBuilder {

    private static Map<Class, CenterSupplier> TYPE_CENTER_SUPPLIER = new HashMap<>();
    private static Map<Class, GeoHashSupplier> TYPE_GEOHASH_SUPPLIER = new HashMap<>();
    static {
        TYPE_CENTER_SUPPLIER.put(Polygon.class, PolygonFeatureBuilder::fromPolygon);
        TYPE_CENTER_SUPPLIER.put(MultiPolygon.class, PolygonFeatureBuilder::fromMultipolygon);
        TYPE_GEOHASH_SUPPLIER.put(Polygon.class, PolygonFeatureBuilder::hashFromPolygon);
        TYPE_GEOHASH_SUPPLIER.put(MultiPolygon.class, PolygonFeatureBuilder::hashFromMultiPolygon);
    }

    private SimpleFeatureBuilder builder;

    public PolygonFeatureBuilder() {
        this.builder = new SimpleFeatureBuilder(new PolygonTypeBuilder().buildFeatureType());
    }

    public Center center;
    public void addPolygon(Polygonal polygon, String countryName, Integer countryKey) {
        center = calculateCenter(polygon);
        builder.add(polygon); // Order matters
        builder.add(countryName);
        builder.add(countryKey);
        builder.add(center);
        builder.add(calculateGeohash(polygon));

    }

    private String calculateGeohash(Polygonal polygonal) {
        Class clazz = polygonal.getClass();
        return TYPE_GEOHASH_SUPPLIER.get(clazz).calculateGeoHash(polygonal);
    }

    private Center calculateCenter(Polygonal polygon) {
        Class clazz = polygon.getClass();
        return TYPE_CENTER_SUPPLIER.get(clazz).calculateCenter(polygon); // new Center(polygon.getCentroid().getY(), polygon.getCentroid().getX());
    }

    public SimpleFeature getSimpleFeature(String featureId) {
        return builder.buildFeature(featureId);
    }

    static class PolygonTypeBuilder extends SimpleFeatureTypeBuilder {

        PolygonTypeBuilder(){
            super();
            setName("polygontype");
            add("country", Polygonal.class, (CoordinateReferenceSystem) null);
            add("NAME", String.class);
            add("KEY", Integer.class);
            add("CENTER", Center.class);
            add("GEO_HASH", String.class);
        }
    }

    static String hashFromPolygon(Polygonal polygonal) {
        return GeoHash.encodeHash(((Polygon)polygonal).getCentroid().getY(), ((Polygon)polygonal).getCentroid().getX());
    }

    static String hashFromMultiPolygon(Polygonal polygonal) {
        MultiPolygon multiPolygon = (MultiPolygon) polygonal;
        Polygon mainPolygon = (Polygon) multiPolygon.getGeometryN(0); //Should be the first (Main) polygon
        return PolygonFeatureBuilder.hashFromPolygon(mainPolygon);
    }

    static Center fromPolygon(Polygonal polygonal) {
        return new Center(((Polygon)polygonal).getCentroid().getY(), ((Polygon)polygonal).getCentroid().getX());
    }

    private static Center fromMultipolygon(Polygonal polygonal) {
        MultiPolygon multiPolygon = (MultiPolygon) polygonal;
        Polygon mainPolygon = (Polygon) multiPolygon.getGeometryN(0); //Should be the first (Main) polygon
        return PolygonFeatureBuilder.fromPolygon(mainPolygon);
    }

    @FunctionalInterface
    interface CenterSupplier {
         Center calculateCenter(Polygonal polygonal);
    }

    @FunctionalInterface
    interface GeoHashSupplier {
        String calculateGeoHash(Polygonal polygonal);
    }


}
