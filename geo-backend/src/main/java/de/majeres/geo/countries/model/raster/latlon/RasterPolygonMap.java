package de.majeres.geo.countries.model.raster.latlon;

import de.majeres.geo.countries.model.Center;
import de.majeres.geo.countries.model.raster.RasterKey;
import de.majeres.geo.facade.model.Coordinate;
import de.majeres.geo.facade.model.CustomGridFeatureBuilder;
import de.majeres.geo.countries.model.raster.RasterConfig;
import lombok.extern.slf4j.Slf4j;
import org.geotools.data.DataUtilities;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.opengis.feature.simple.SimpleFeature;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class RasterPolygonMap extends ConcurrentHashMap<RasterKey, SimpleFeatureCollection> {

    public void addFeature(SimpleFeatureCollection features) {
        SimpleFeature simpleFeature;
        try(SimpleFeatureIterator featureIter = features.features()) {
            while(featureIter.hasNext()) {
                simpleFeature = featureIter.next();
                //log.info("Adding feature type {}", simpleFeature.getDefaultGeometry().getClass());
                putFeatureFromPolygon((Center) simpleFeature.getAttribute(CustomGridFeatureBuilder.Type.CENTER.name()), simpleFeature);
            }
        }
    }

    private void putFeatureFromPolygon(Center center, SimpleFeature feature) {
        List<SimpleFeature> features = new ArrayList<>();
        RasterKey key = getKeyFromCoordinate(center);
        if(!this.containsKey(key)) this.initializeFeatureCollectionForKey(key);
        SimpleFeatureCollection simpleFeatureCollection = get(key);
        try(SimpleFeatureIterator featureIter = simpleFeatureCollection.features()) {
            while (featureIter.hasNext()) features.add(featureIter.next());
            features.add(feature);
        }
        put(key, DataUtilities.collection(features));
    }

    private void initializeFeatureCollectionForKey(RasterKey key) {
        put(key, DataUtilities.collection(Arrays.asList()));
    }

    private RasterKey getKeyFromCoordinate(Center coordinate) {
        double latMin  = (coordinate.getCenterLat() - (coordinate.getCenterLat() % RasterConfig.HEXAGON_RASTER_WIDTH));
        double lonMin = (coordinate.getCenterLon() - (coordinate.getCenterLon() % RasterConfig.HEXAGON_RASTER_WIDTH));
        return new RasterKey(latMin, latMin + RasterConfig.HEXAGON_RASTER_WIDTH, lonMin, lonMin + RasterConfig.HEXAGON_RASTER_WIDTH);
    }

}
