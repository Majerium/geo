package de.majeres.geo.countries;

import de.majeres.geo.config.ConfigService;
import de.majeres.geo.countries.model.raster.country.CountryGrid;
import de.majeres.geo.countries.model.raster.country.linked.LinkedShapeGrid;
import de.majeres.geo.countries.model.raster.latlon.HexagonGrid;
import de.majeres.geo.countries.model.raster.latlon.HexagonKey;
import de.majeres.geo.facade.GeoJsonMapper;
import lombok.extern.slf4j.Slf4j;
import org.geojson.*;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.geojson.feature.FeatureJSON;
import org.opengis.feature.simple.SimpleFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CountryService {

    private static final String COUNTRIES_JSON_PATH = "classpath:countries.geojson";

    private static final LinkedShapeGrid LINKED_SHAPE_GRID = new LinkedShapeGrid();

    private static final HexagonGrid RASTER = new HexagonGrid();

    private static final Comparator<Feature> COUNTRY_POLYGON_LENGTH_COMPARATOR = Comparator.comparing(Feature::getGeometry, Comparator.comparingInt(s -> ((Polygon) s).getCoordinates().get(0).size() * -1));

    private static Map<Class, RasterFilter> TYPE_FILTER_SUPPLIER = new HashMap<>();
    static {
        TYPE_FILTER_SUPPLIER.put(Polygon.class, CountryService::filterPolygonByBox);
        TYPE_FILTER_SUPPLIER.put(MultiPolygon.class, CountryService::filterMultipolygonByBox);
    }

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private CountryPolygonService countryPolygonService;

    @Autowired
    private ConfigService configService;
    
    @PostConstruct
    private void postConstruct() {

        Resource resource = resourceLoader.getResource(COUNTRIES_JSON_PATH);

        FeatureJSON io = new FeatureJSON();

        SimpleFeature feature = null;
        try(FeatureIterator<SimpleFeature> featureIterator = io.streamFeatureCollection(resource.getInputStream())) {
            while(featureIterator.hasNext()) {
                feature = featureIterator.next();
                if(feature != null && ((String)feature.getAttribute(CountryGrid.COUNTRY_PROPERTY_NAME)).equals("Germany")) {
                    LINKED_SHAPE_GRID.addFeature(feature);
                    countriesToHexGrid(feature);
                }
            }
        } catch (IOException e) {
            log.error("Could not read countries shape file", e);
        }
        LINKED_SHAPE_GRID.linkShapeRaster();
    }

    public void countriesToHexGrid(SimpleFeature feature) {
        String countryName = (String) feature.getAttribute(CountryGrid.COUNTRY_PROPERTY_NAME);
        configService.getZoomLevelForRaster().forEach( zoom -> {
            try {
                if (zoom.ZOOM <= 10) {
                    countryPolygonService.smallRaster(feature, zoom, countryName, this::addRaster);
                } else if (zoom.ZOOM > 10) {
                    countryPolygonService.tallRaster(feature, zoom, countryName, this::addRaster);
                }
            } catch (IOException e) {
                log.error("Exception when creating raster", e);
            }
        });
    }

    private void addRaster(long startTime, String country, ConfigService.ZoomLevel zoom, SimpleFeatureCollection featureCollection) {

        HexagonKey key = new HexagonKey(zoom.ZOOM);

        RASTER.addFeature(zoom, featureCollection);

        long duration = (new Date().getTime() - startTime);

        String time = String.format("%02d min, %02d sec",
                TimeUnit.MILLISECONDS.toMinutes(duration),
                TimeUnit.MILLISECONDS.toSeconds(duration) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration))
        );

        log.info("{} created raster for {} and Zoom-{} ({} elements) in {} ms.", Thread.currentThread().getName(), country, key.getZoom(), RASTER.get(key).size(), time);
    }

    public Set<String> getAllCountries() {
        return LINKED_SHAPE_GRID.getAllCountryNames();
    }

    public FeatureCollection getCountries(double lat, double lon) {

        FeatureCollection featureCollection = new FeatureCollection();

        LINKED_SHAPE_GRID.getCountriesByCenter(lat, lon).stream().map(GeoJsonMapper::toGeoPolygonFeature).forEach(feature -> featureCollection.add(feature));

        removeSmallerCountiesAndKeep(featureCollection, 15);

        return featureCollection;
    }

    private void removeSmallerCountiesAndKeep(FeatureCollection featureCollection, int maxCountries) {

        featureCollection.getFeatures().sort(COUNTRY_POLYGON_LENGTH_COMPARATOR);

        int max = maxCountries < featureCollection.getFeatures().size() ? maxCountries : featureCollection.getFeatures().size();

        featureCollection.setFeatures(featureCollection.getFeatures().subList(0, max));
    }

    public FeatureCollection getCountryFeatures(String country) {
        return GeoJsonMapper.toFeatureCollection(LINKED_SHAPE_GRID.getFeaturesForCountryName(country).stream()
                .map(GeoJsonMapper::toGeoPolygonFeature)
                .collect(Collectors.toList()));
    }

    public FeatureCollection getRaster(int zoom, double northEastLat, double northEastLon, double southWestLat, double southWestLon) {
        FeatureCollection featureCollection = GeoJsonMapper.toGeoFeatureCollectionPolygon(RASTER.getByCoordinates(zoom, northEastLat, northEastLon, southWestLat, southWestLon));

        filterByBox(featureCollection, northEastLat, northEastLon, southWestLat, southWestLon);

        return featureCollection;
    }

    private void filterByBox(FeatureCollection featureCollection, double northEastLat, double northEastLon, double southWestLat, double southWestLon) {
        List<Feature> filteredFeatures = featureCollection.getFeatures().parallelStream()
                .filter((feature) -> TYPE_FILTER_SUPPLIER.get(feature.getGeometry().getClass()).filterPolygonByBox(feature, northEastLat, northEastLon, southWestLat, southWestLon)).collect(Collectors.toList());

        featureCollection.setFeatures(filteredFeatures);
    }

    private static boolean filterPolygonByBox(Feature feature, double northEastLat, double northEastLon, double southWestLat, double southWestLon) {

        Optional<Double> minLon = ((Polygon) feature.getGeometry()).getCoordinates().stream().flatMap(Collection::stream).map(LngLatAlt::getLongitude).min(Comparator.comparing(Double::doubleValue));
        Optional<Double> maxLon = ((Polygon) feature.getGeometry()).getCoordinates().stream().flatMap(Collection::stream).map(LngLatAlt::getLongitude).max(Comparator.comparing(Double::doubleValue));
        Optional<Double> minLat = ((Polygon) feature.getGeometry()).getCoordinates().stream().flatMap(Collection::stream).map(LngLatAlt::getLatitude).min(Comparator.comparing(Double::doubleValue));
        Optional<Double> maxLat = ((Polygon) feature.getGeometry()).getCoordinates().stream().flatMap(Collection::stream).map(LngLatAlt::getLatitude).max(Comparator.comparing(Double::doubleValue));

        return  minLon.isPresent() && maxLon.isPresent() && minLat.isPresent() && maxLat.isPresent() &&
                maxLat.get() <= northEastLat && minLat.get() >= southWestLat &&
                maxLon.get() <= northEastLon && minLon.get() >= southWestLon;
    }

    private static boolean filterMultipolygonByBox(Feature feature, double northEastLat, double northEastLon, double southWestLat, double southWestLon) {
        return true;
    }

    @FunctionalInterface
    interface RasterFilter {
        boolean filterPolygonByBox(Feature feature, double northEastLat, double northEastLon, double southWestLat, double southWestLon);
    }

}
