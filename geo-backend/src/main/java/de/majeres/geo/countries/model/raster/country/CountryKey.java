package de.majeres.geo.countries.model.raster.country;

import java.util.Objects;

public class CountryKey {

    private String countryName;
    private String ISO_A2;
    private String ISO_A3;

    public CountryKey(String countryName, String ISO_A2, String ISO_A3) {
        this.countryName = countryName;
        this.ISO_A2 = ISO_A2;
        this.ISO_A3 = ISO_A3;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getISO_A2() {
        return ISO_A2;
    }

    public String getISO_A3() {
        return ISO_A3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryKey that = (CountryKey) o;
        return  Objects.equals(countryName, that.countryName) &&
                Objects.equals(ISO_A2, that.ISO_A2) &&
                Objects.equals(ISO_A3, that.ISO_A3);
    }

    @Override
    public int hashCode() {
        return Objects.hash(countryName, ISO_A2, ISO_A3);
    }
}
