package de.majeres.geo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.concurrent.Executor;

@Slf4j
//@EnableAsync
@SpringBootApplication
public class GeoTool extends SpringBootServletInitializer implements WebMvcConfigurer {

    public static void main(String[] args) {
        SpringApplication.run(GeoTool.class, args);
    }

    //@Bean(name = "rasterCreatorSmall")
    public Executor taskExecutorSmall() {
        int maxThreadsAvailable = Runtime.getRuntime().availableProcessors();
        log.info("Setting executor thread pool for {} threads", maxThreadsAvailable);
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(maxThreadsAvailable / 2);
        executor.setMaxPoolSize(maxThreadsAvailable / 2);
        executor.setThreadNamePrefix("RasterCreatorSmall-");
        executor.initialize();
        return executor;
    }

    //@Bean(name = "rasterCreatorTall")
    public Executor taskExecutorTall() {
        int maxThreadsAvailable = Runtime.getRuntime().availableProcessors();
        log.info("Setting executor thread pool for {} threads", maxThreadsAvailable);
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(maxThreadsAvailable / 2);
        executor.setMaxPoolSize(maxThreadsAvailable / 2);
        executor.setThreadNamePrefix("RasterCreatorTall-");
        executor.initialize();
        return executor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new CorsInterceptor());
    }
}
