package de.majeres.geo.config;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ConfigService {

    public List<ZoomLevel> getZoomLevelForRaster() {
        return  Arrays.asList(ZoomLevel.values())
                .stream().filter(ZoomLevel::isCALCULATE_RASTER)
                .collect(Collectors.toList());
    }

    @AllArgsConstructor
    @Getter
    public enum ZoomLevel {

        THREE(3, 1, false), //Ok
        FOUR(4, 1, false), //Ok
        FIVE(5, 1, false), //Ok
        SIX(6, 1, false), //Ok
        SEVEN(7, 0.2, true), //Ok
        EIGHT(8, 0.1, false), //Ok
        NINE(9, 0.05, true), //Ok
        TEN(10, 0.05, false), //Ok
        ELEVEN(11, 0.1, false), //Ok
        TWELVE(12, 0.025, false), //Ok
        THIRTEEN(13, 0.1, false), //Ok
        FOURTEEN(14, 0.001, false), //<==
        FIFTEEN(15, 0.1, false), //Ok
        SIXTEEN(16, 0.0125, false), //Ok
        SEVENTEEN(17, 0.1, false), //Ok
        EIGHTEEN(18, 0.1, false), //Ok
        NINETEEN(19, 0.1, false); //Ok

        @JsonValue
        public final int ZOOM;
        public transient final double SIDE_LENGTH;
        public transient final boolean CALCULATE_RASTER;
    }

}
