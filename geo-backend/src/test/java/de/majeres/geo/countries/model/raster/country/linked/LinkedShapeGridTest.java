package de.majeres.geo.countries.model.raster.country.linked;

import de.majeres.geo.countries.model.raster.RasterConfig;
import org.junit.Assert;
import org.junit.Test;

public class LinkedShapeGridTest {

    LinkedShapeGrid grid = new LinkedShapeGrid();

    @Test
    public void testCountryGridCentral()  {

        double corners = 4;
        double max = ((RasterConfig.MAX_LAT * 2) / RasterConfig.COUNTRY_RASTER_WIDTH) * ((RasterConfig.MAX_LAT * 2) / RasterConfig.COUNTRY_RASTER_WIDTH);
        double threeNeighbours = ((((RasterConfig.MAX_LAT *  2) / RasterConfig.COUNTRY_RASTER_WIDTH) * 4) - (corners * 2));
        double fourNeighbours = (max - threeNeighbours) - corners;

        Assert.assertTrue(max == (corners + threeNeighbours + fourNeighbours));

        long count = grid.keySet().stream().filter(key -> numOfOptionals(key, 4)).count();
        System.out.println(count);
        Assert.assertEquals(count, Double.valueOf(fourNeighbours).longValue());

        count = grid.keySet().stream().filter(key -> numOfOptionals(key, 2)).count();
        System.out.println(count);
        Assert.assertEquals(count, Double.valueOf(corners).longValue());

        count = grid.keySet().stream().filter(key -> numOfOptionals(key, 3)).count();
        System.out.println(count);
        Assert.assertEquals(count, Double.valueOf(threeNeighbours).longValue());

    }

    private boolean numOfOptionals(LinkedRasterKey key, int numOptionals) {
        int counter = 0;
        for(LinkedRasterKey.Direction dir: LinkedRasterKey.Direction.values()) {
           if(key.getNeighbourByDirEmpty(dir).isPresent()) {
               counter++;
           }
        }
        return numOptionals == counter;
    }

}