import {BBox, Feature, MultiPolygon, Polygon} from "geojson";

export interface RasterOptions {
  units: string,
  properties?: any,
  mask?: Feature<Polygon | MultiPolygon>,
  triangles?: boolean
}

export interface RasterConfig {
  box?: BBox;
  cellSide: number;
  options?: RasterOptions
}
