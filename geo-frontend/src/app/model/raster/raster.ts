import {Feature, FeatureCollection, GeoJsonProperties, Geometry} from "geojson";
import {LatLngExpression} from "leaflet";

export type LeafletRepresentation = LatLngExpression[][] | LatLngExpression[];

export abstract class Raster<G extends Geometry | null = Geometry, P = GeoJsonProperties> {

  protected featureCollection: FeatureCollection<G, P>;

  public abstract getLeafLetRepresentation(): LeafletRepresentation;

  public getFeatureCollection(): FeatureCollection<G, P> {
    return this.featureCollection
  };

  protected getGeometries(): Array<Feature<G, P>> {
    return this.featureCollection.features;
  }

  protected getGeometry(feature: Feature<G, P>): G {
    return feature.geometry;
  }

}
