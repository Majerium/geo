import {HexGridRaster} from "./hex-grid-raster";
import {RasterConfig} from "./raster-config";

export type RasterType = "Hexagon" | "None"

export class RasterBuilder {

  private static DEFAULT_RASTER_TYPE: RasterType = "Hexagon";

  public static readonly createRaster = (type: RasterType = RasterBuilder.DEFAULT_RASTER_TYPE, config: RasterConfig) => {
    switch (type) {
      case "Hexagon":{
        return new HexGridRaster(config);
      }
    }
  }
}
