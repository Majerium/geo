import {Raster} from "./raster";
import {RasterConfig} from "./raster-config";
import {Feature, GeoJsonProperties, Polygon, Position} from "geojson";
import {LatLng, LatLngExpression} from "leaflet";
import hexgrid from "@turf/hex-grid";

export interface PolygonProperty extends GeoJsonProperties { }

export class HexGridRaster extends Raster<Polygon, PolygonProperty> {

  constructor(private readonly config: RasterConfig) {
    super();

    this.featureCollection = hexgrid(config.box, config.cellSide, config.options);
    console.log("bla")
  }

  public getLeafLetRepresentation(): LatLngExpression[][] {
    const features: Array<Feature<Polygon, PolygonProperty>> = this.getGeometries();
    return features.map(g => g.geometry.coordinates)
      .map(pos => pos[0])
      .map(this.toLatLonExressionArray);
  }

  private toLatLonExressionArray(positions: Position[]): LatLngExpression[] {
    return positions.map((pos: Position) => new LatLng(pos[0], pos[1]));
  }

}
