import {LatLngBounds} from "leaflet";
import {BBox, Feature} from "geojson";
import bbox from "@turf/bbox"

export class RasterUtil {

  public static getBBox(bounds: LatLngBounds): BBox {
    return [
      bounds.getSouthWest().lng,
      bounds.getSouthWest().lat,
      bounds.getNorthEast().lng,
      bounds.getNorthEast().lat
    ];
  }

  public static getBBoxFor(value: Feature): BBox {
    return bbox(value) as BBox;
  }

  public static getKilometersForZoom(zoomLevel: number): number {
    switch (zoomLevel) {
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7: return 100;
      case 8: return 50;
      case 9: return 20;
      case 10: return 10;
      case 11: return 5;
      case 12: return 3;
      case 13: return 1;
      case 14: return 0.5;
      case 15: return 0.3;
      case 16: return 0.2;
      case 17: return 0.1;
      case 18: return 0.05;
      case 19: return 0.02;
    }
  }

}
