import {ShapeType} from "./shape-type";
import {GeoJSON} from "geojson";
import {Kpi} from "../../services/api/generated/data";
import {ColorPaletteService} from "../../services/color-palette/color-palette.service";
import {MapConfig} from "../map/config";

export type CompareId = 'COMPARE_1'

export class Shape {

  private _isSelected = false;
  private _kpi: Kpi;
  private _kpiCompare: Kpi;

  get id(): string {
    return this._id;
  }

  get kpi(): Kpi {
    return this._kpi;
  }

  get feature(): GeoJSON {
    return this._feature;
  }

  get type(): ShapeType {
    return this._type;
  }

  get isSelected(): boolean {
    return this._isSelected;
  }

  get hasData(): boolean {
    return this._kpi !== undefined;
  }

  get compareKpi(): Kpi {
    return this._kpiCompare;
  }

  get leafLetLayer(): any {
    return this._leafletLayer;
  }
  public setCompareKpi(compareKpi: Kpi, colorScheme: string, kpi: string, isCompare: boolean) {
    this._kpiCompare = compareKpi;
    this.updateStyle(kpi, colorScheme, isCompare);
  }

  constructor(private readonly colorPaletteService: ColorPaletteService, private _id: string, private _type: ShapeType, private readonly _feature: GeoJSON, private readonly _leafletLayer: any) { }

  public setFilterKpi(filterKpi: Kpi, colorScheme: string, kpi: string, isCompare: boolean) {
    this._kpi = filterKpi;
    this.updateStyle(kpi, colorScheme, isCompare);
  }

  public updateStyle(kpi: string, colorScheme: string, isCompare: boolean): void {
    if (isCompare && this._kpiCompare && this._kpi) {
      const color = this.colorPaletteService.getCompareColor(this.getValueForKpi(this._kpi, kpi), this.getValueForKpi(this._kpiCompare, kpi));
      this._leafletLayer.setStyle({
        weight: this._leafletLayer.isSelected ? MapConfig.DEFAULT_LAYER_COLOR.weight + 3 : this._leafletLayer.options.style().weight,
        fillColor: color,
        fillOpacity: this._leafletLayer.options.style().fillOpacity,
        color: this._leafletLayer.options.style().color
      });
    } else if (!isCompare && this._kpi) {
      const color = this.colorPaletteService.getPalettesColor(2);
      this._leafletLayer.setStyle({
        weight: this._leafletLayer.isSelected ? MapConfig.DEFAULT_LAYER_COLOR.weight + 3 : this._leafletLayer.options.style().weight,
        fillColor: color,
        fillOpacity: this._leafletLayer.options.style().fillOpacity,
        color: this._leafletLayer.options.style().color
      });
    }
  }

  public highlightShape(): void {
    this._leafletLayer.isSelected = true;
    this._leafletLayer.setStyle({ 'weight': MapConfig.DEFAULT_LAYER_COLOR.weight + 3})
  }

  public unsetShape(): void {
    this._leafletLayer.isSelected = false;
    this._leafletLayer.setStyle({ 'weight': MapConfig.DEFAULT_LAYER_COLOR.weight});
  }

  public toggle(): void {
    this._isSelected = !this._isSelected;
  }

  private getValueForKpi(kpiData: Kpi, kpi: string): number {
    switch (kpi) {
      case 'COUNT_CASE': return kpiData.anzahlFall;
      case 'COUNT_NEW_CASE': return kpiData.neuerFall;
      case 'COUNT_HEALED': return kpiData.anzahlGenesen;
      case 'COUNT_NEW_HEALED': return kpiData.neuGenesen;
      case 'COUNT_DEATH_CASE': return kpiData.anzahlTodesfall;
      case 'COUNT_NEW_DEATH_CASE': return kpiData.neuerTodesfall;
      case 'COUNT_IS_ILLNESS_START': return kpiData.isErkrankunsbeginn;
      case 'VACCINATIONS_AMOUNT': return kpiData.vaccinationAmount;
      case 'DEATH_COUNT': return kpiData.deaths;

    }
  }

}
