import {MapOptions, TileLayerOptions} from "leaflet";

export class MapConfig {

  public static CALCULATE_GRIDS_FROM = 7;

  public static MAP_OPTIONS: MapOptions = {
    center: [ 39.8282, -98.5795 ],
    zoom: 3,
    tap: false
  }

  public static LAYER_OPTIONS: TileLayerOptions = {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
  };

  public static DEFAULT_LAYER_COLOR = {
    weight: 1,
    fillColor: '#ffffff',
    fillOpacity: 0.4,
    color: '#ffffff'
  }

  public static SHAPE_OPTIONS = {
    COUNTRIES: {
      DRAW_COUNTRIES: (zoom: number): boolean => zoom > 5 && zoom < MapConfig.CALCULATE_GRIDS_FROM,
      MAX_COUNTRIES: 15,
      FEATURE_OPTIONS: {
        PROPERTIES: {
          COUNTRY: 'NAME',
          COUNTRY_KEY: 'KEY'
        }
      }
    },
    RASTER: {
      DRAW_RASTER: (zoom: number): boolean => zoom >= MapConfig.CALCULATE_GRIDS_FROM,
      FEATURE_OPTIONS: {
        PROPERTIES: {
          CENTER: 'GEO_HASH'
        }
      }
    }
  }
}
