import * as L from "leaflet";
import {Map as LeafletMap} from "leaflet";
import {Layer} from "./layer";
import {Feature, FeatureCollection, MultiPolygon, Polygon} from "geojson";
import {PolygonProperty} from "../../datastore/geo/model/polygon-property";
import {MapConfig} from "../map/config";
import {ShapeService} from "../../services/shape/shape.service";
import {ShapeType} from "../shape/shape-type";
import {RouterDatastoreService} from "../../datastore/router/services/router-datastore.service";

export class CountryLayer extends Layer {

  private type = 'country';

  constructor(
    protected map: LeafletMap,
    protected shapeService: ShapeService,
    protected routerDatastoreService: RouterDatastoreService
  ) {
    super(map, shapeService, routerDatastoreService);
  }

  public addByCountries(countries: FeatureCollection<Polygon, PolygonProperty>): void {
    countries.features
      .filter((feature: Feature<Polygon, PolygonProperty>) => !this.hasLayer(feature.properties['GEO_HASH']))
      .filter((feature: Feature<Polygon, PolygonProperty>) => !this.isAlreadyVisible(feature.properties['GEO_HASH']))
      .forEach((feature: Feature<Polygon, PolygonProperty>) => this.addLayer(this.type, feature.properties['GEO_HASH'], feature, true))
  }

  public removeAllCountryLayer(): void {
    this.layerGroup.eachLayer((layer: L.Layer) => {
      // if (this.id in layer && layer[this.id].type === this.type) {
        this.removeLayer(layer[this.id]);
     // }
    })
  }

  private isAlreadyVisible(countryId: string): boolean {
    return this.shapeService.getGeoHashesOfDisplayedFeatures().includes(countryId);
  }

  public removeLayerByCountry(countries: FeatureCollection<MultiPolygon, PolygonProperty>): void {
    countries.features.forEach((feature: Feature<MultiPolygon, PolygonProperty>) => this.removeLayer(feature.properties[MapConfig.SHAPE_OPTIONS.COUNTRIES.FEATURE_OPTIONS.PROPERTIES.COUNTRY]))
  }

  protected createLayer(feature: Feature<Polygon, PolygonProperty>, layerId: string): any {
    const featureId: string = feature.properties['GEO_HASH'];
    const raster = L.geoJSON(feature, {
      style: function (feature) {
        return MapConfig.DEFAULT_LAYER_COLOR
      }
    });
    this.addShapeConfig(raster, featureId);
    this.shapeService.addShape(ShapeType.COUNTRY, layerId, feature, raster);
    return raster;
  }

  public static getCountryKeyFromProperties(propertry: PolygonProperty): string {
    return `${propertry[MapConfig.SHAPE_OPTIONS.COUNTRIES.FEATURE_OPTIONS.PROPERTIES.COUNTRY]}_${propertry[MapConfig.SHAPE_OPTIONS.COUNTRIES.FEATURE_OPTIONS.PROPERTIES.COUNTRY_KEY]}`;
  }

}
