import {Layer} from "./layer";
import * as L from "leaflet";
import {Layer as LeafletLayer, Map as LeafletMap} from "leaflet";
import {Feature, FeatureCollection, Polygon} from "geojson";
import {PolygonProperty} from "../../datastore/geo/model/polygon-property";
import {ShapeService} from "../../services/shape/shape.service";
import {ShapeType} from "../shape/shape-type";
import {MapConfig} from "../map/config";
import {RouterDatastoreService} from "../../datastore/router/services/router-datastore.service";

export class RasterLayer extends Layer {

  private type = 'raster';

  constructor(
    protected map: LeafletMap,
    protected shapeService: ShapeService,
    protected routerDatastoreService: RouterDatastoreService
  ) {
    super(map, shapeService, routerDatastoreService);
  }

  public addByZoomAndFeatureCollection(zoomLevel: number, featureCollection: FeatureCollection<Polygon, PolygonProperty>): void {
    const layerId = `raster_${zoomLevel}`;
    if (!this.hasLayer(layerId)) {
      this.removeRasterLayer();
      this.addLayer(this.type, layerId, featureCollection);
    } else {
      this.addFeaturesToLayer(layerId, featureCollection);
    }
  }

  public removeRasterLayer(): void {
    this.layerGroup.eachLayer((layer: L.Layer) => {
     // if (this.id in layer && layer[this.id].type === this.type) {
        this.removeLayer(layer[this.id]);
     // }
    })
  }

  private addFeaturesToLayer(layerId: string, featureCollection: FeatureCollection<Polygon, PolygonProperty>): void {
    const layer = this.getLayer(layerId) as any;
    const oldFeatures: Array<Feature<Polygon, PolygonProperty>> = layer.getLayers().map(layer => layer.feature);
    oldFeatures.forEach((feature: Feature<Polygon, PolygonProperty>) => featureCollection.features.push(feature));
    this.removeDoubles(featureCollection);
    this.addLayer(this.type, layerId, featureCollection, false)
  }

  private removeDoubles(featureCollection: FeatureCollection<Polygon, PolygonProperty>): void {
    const newFeatureList: Map<string, Feature<Polygon, PolygonProperty>> = new Map();
    featureCollection.features.forEach((feature: Feature<Polygon, PolygonProperty>) => newFeatureList.set(feature.properties[MapConfig.SHAPE_OPTIONS.RASTER.FEATURE_OPTIONS.PROPERTIES.CENTER], feature));
    this.shapeService.getGeoHashesOfDisplayedFeatures().forEach((geoHash: string) => this.deleteFromObject(newFeatureList, geoHash));
    featureCollection.features = Array.from(newFeatureList.values());
  }

  private deleteFromObject(object: Map<string, Feature<Polygon, PolygonProperty>>, key: string): void {
    if (object.has(key)) {
      object.delete(key);
    }
  }

  protected createLayer(feature : FeatureCollection<Polygon, PolygonProperty>, id?: string): any {
    const raster = L.geoJSON(feature, {
      onEachFeature: (feature: Feature<Polygon>, layer: LeafletLayer) => this.onEachFeature(feature, layer),
      style: (feature) => this.onEachStyle()});
    return raster;
  }

  private onEachStyle = () => MapConfig.DEFAULT_LAYER_COLOR;

  private onEachFeature(feature: Feature<Polygon, PolygonProperty>, layer: any): void {
    const featureId: string = feature.properties['GEO_HASH'];
    this.addShapeConfig(layer, featureId);
    this.shapeService.addShape(ShapeType.HEXAGON, featureId, feature, layer);
  }

  protected removeChildren(layer: any): void {
    if (layer) {
      layer.getLayers().forEach((lay) => {
        if (lay.feature && lay.feature.properties && lay.feature.properties['GEO_HASH']) {
          this.shapeService.removeShape(lay.feature.properties['GEO_HASH'])
        }
      })
    }
  }
}
