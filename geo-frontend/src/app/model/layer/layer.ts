import * as L from "leaflet";
import {LayerGroup, Map} from "leaflet";
import {GeoJSON} from "geojson";
import {ShapeService} from "../../services/shape/shape.service";
import {MapConfig} from "../map/config";
import {RouterDatastoreService} from "../../datastore/router/services/router-datastore.service";
import {QUERY_NAMES} from "../../datastore/router/reducer";

export abstract class Layer {

  protected id = 'go_id_field';
  protected layerGroup: LayerGroup;

  protected constructor(
    protected map: Map,
    protected shapeService: ShapeService,
    protected routerDatastoreService: RouterDatastoreService
  ) {
    this.layerGroup = L.layerGroup();
    this.layerGroup.addTo(map);
  }

  protected getLayer(layerId: string): L.Layer | undefined {
    const layers: Array<L.Layer> = this.layerGroup.getLayers().filter((layer: L.Layer) => layer[this.id] === layerId);
    return layers.length > 0 ? layers[0]: undefined;
  }

  protected abstract createLayer(feature: GeoJSON, id?: string): any;
  protected removeChildren(layer: any): void { }

  protected addShapeConfig(layer: any, geoHash): void {
    layer.isSelected = false;
    this.setOnSelectLayer(layer, geoHash)
    this.setOnMouseOverLayer(layer);
    this.setOnMouseOutLayer(layer);
  }

  private setOnMouseOutLayer(layer: any): void {
    layer.on('mouseout', () => {
      if(!layer.isSelected)
        layer.setStyle({ 'weight': MapConfig.DEFAULT_LAYER_COLOR.weight})
    });
  }

  private setOnMouseOverLayer(layer: any): void {
    layer.on('mouseover', () => {
      if(!layer.isSelected)
        layer.setStyle({ 'weight': MapConfig.DEFAULT_LAYER_COLOR.weight + 3})
    });
  }

  private setOnSelectLayer(layer: any, geoHash: string): void {
    layer.on('click', () => {
      layer.isSelected = !layer.isSelected;
      if (layer.isSelected) this.selectLayer(layer, geoHash);
      else this.deselectLayer(layer, geoHash);
    });
  }

  private selectLayer(layer: any, geoHash: string): void {
    this.routerDatastoreService.setSimpleArrayParam(QUERY_NAMES.MAP.SHAPES, geoHash);
  }

  private deselectLayer(layer: any, geoHash: string): void {
    this.routerDatastoreService.removeSimpleArrayParam(QUERY_NAMES.MAP.SHAPES, geoHash);
    layer.setStyle({ 'weight': MapConfig.DEFAULT_LAYER_COLOR.weight});
  }

  protected addLayer(type: string, layerId: string, feature: GeoJSON, withLeafletRemove: boolean = false): void {
    if(withLeafletRemove && this.hasLayer(layerId)) {
      this.removeLayer(layerId);
    }
    const rasterLayer = this.createLayer(feature, layerId);
    rasterLayer.type = type;
    this.layerGroup.addLayer(rasterLayer);
    this.setLayerId(layerId);
  }

  protected hasLayer(layerName: string): boolean {
    let hasLayer = false;
    this.layerGroup.eachLayer( layer => {
      if (layer[this.id] === layerName) {
        hasLayer = true;
      }
    })
    return hasLayer;
  }

  protected setLayerId(layerId: string): void {
    this.layerGroup.eachLayer( (layer: L.Layer) => {
      if(!layer[this.id]) {
        layer[this.id] = layerId;
      }
    })
  }

  protected removeLayer(id: string): void {
    this.layerGroup.getLayers().forEach((layer: L.Layer) => {
      if (layer[this.id] === id){
        this.shapeService.removeShape(id);
        this.removeChildren(layer);
        this.layerGroup.removeLayer(layer);
      }
    })
  }
}
