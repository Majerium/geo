import {Injectable} from '@angular/core';
import * as L from "leaflet";
import {LeafletEvent, LocationEvent, Map as LeafletMap} from "leaflet";
import {MapConfig} from "../../model/map/config";
import {LocationDatastoreService} from "../../datastore/location/services/location-datastore.service";
import {Location} from "../../datastore/location/model/location";
import {CountryLayer} from "../../model/layer/country-layer";
import {RasterLayer} from "../../model/layer/raster-layer";
import {ForBbox} from "../../datastore/location/model/for-bbox";
import {FeatureCollection, Polygon} from "geojson";
import {PolygonProperty} from "../../datastore/geo/model/polygon-property";
import {ShapeService} from "../shape/shape.service";
import {GeoDatastoreService} from "../../datastore/geo/services/geo-datastore.service";
import {AppConfigDatastoreService} from "../../datastore/config/services/app-config-datastore.service";
import {AppConfig} from "../../datastore/config/model/appConfig";
import {combineLatest, Subject} from "rxjs";
import {debounceTime, takeUntil} from "rxjs/operators";
import {RouterDatastoreService} from "../../datastore/router/services/router-datastore.service";
import {Params} from "@angular/router";
import {QUERY_NAMES} from "../../datastore/router/reducer";

@Injectable({
  providedIn: 'root'
})
export class MapService {

  private unsubscribe$: Subject<void>;

  private config: AppConfig;

  private map: LeafletMap;
  private countryLayer: CountryLayer;
  private rasterLayer: RasterLayer;
  private isInitiated = false;
  private location: ForBbox;

  constructor(
    private readonly locationDataService: LocationDatastoreService,
    private readonly shapeService: ShapeService,
    private readonly geoDataService: GeoDatastoreService,
    private readonly appConfigDataService: AppConfigDatastoreService,
    private readonly routerDatastoreService: RouterDatastoreService
  ) { }

  public initiateMap(lat: number, lon: number, zoom: number): void {
    this.map = L.map('map', MapConfig.MAP_OPTIONS);
    this.map.doubleClickZoom.disable();
    this.map.setView([lat, lon], zoom); //Germany
    this.countryLayer = new CountryLayer(this.map, this.shapeService,  this.routerDatastoreService);
    this.rasterLayer = new RasterLayer(this.map, this.shapeService, this.routerDatastoreService);
    L.control.scale().addTo(this.map);
    this.initiateTiles();
    this.initiateEventListener();
    this.setInitialLocation();
    this.isInitiated = true;
    this.unsubscribe$ = new Subject();
    this.subscribeToMapChanges();
    this.subscribeToOtherChanges();
  }

  private subscribeToMapChanges(): void {
    // Subscribtion for the fetching of the servers raster
    combineLatest([this.appConfigDataService.getRasterZoomLevel(), this.appConfigDataService.getDatasetZoomLevel(), this.locationDataService.getLocationForBbox()]).pipe(
      takeUntil(this.unsubscribe$),
      debounceTime(300)
    ).subscribe(([rasterZoomLevel, datasetZoomLevel, location]) => this.fetchGeometry(rasterZoomLevel, datasetZoomLevel, location));
  }

  private subscribeToOtherChanges(): void {
    this.locationDataService.getLocationForBbox().pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe((location: ForBbox) => this.location = location);
    this.appConfigDataService.getConfig().pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe((config: AppConfig) => this.config = config);
  }

  public destroy(): void {
    this.map.off();
    this.map.remove();
    this.unsubscribe$.next();
    this.unsubscribe$.unsubscribe();
    this.shapeService.discardData();
  }

  public drawCountries(countries: FeatureCollection<Polygon, PolygonProperty>): void {
    this.countryLayer.addByCountries(countries); // Zeichnet sich nur neu, wenn land vorher nicht da war
    this.rasterLayer.removeRasterLayer();
  }

  public drawFeatureCollection(zoomLevel: number, featureCollection: FeatureCollection<Polygon, PolygonProperty>): void {
    this.countryLayer.removeAllCountryLayer();
    this.rasterLayer.addByZoomAndFeatureCollection(zoomLevel, featureCollection);
  }

  private fetchGeometry(rasterZoomLevel: Array<number> = [], datasetZoomLevel: Array<number> = [], location: ForBbox, withDeleteRaster: boolean = true): void {
    if (MapConfig.SHAPE_OPTIONS.RASTER.DRAW_RASTER(location.zoom) && rasterZoomLevel.includes(location.zoom) && datasetZoomLevel.includes(location.zoom)) {
      this.geoDataService.fetchRasterByBbox(location);
    } else if(MapConfig.SHAPE_OPTIONS.RASTER.DRAW_RASTER(location.zoom) && rasterZoomLevel.includes(location.zoom) && !datasetZoomLevel.includes(location.zoom)) {
      // Es wurde das dataset geändert, die aktuelle Zoomstufe ist aber zu hoch
      if (withDeleteRaster) this.rasterLayer.removeRasterLayer();
      this.geoDataService.fetchRasterByBbox({...location, zoom: datasetZoomLevel[0]});
    } else if (MapConfig.SHAPE_OPTIONS.COUNTRIES.DRAW_COUNTRIES(location.zoom)) {
      this.geoDataService.fetchCountryNearCenter(this.map.getCenter());
    } else this.loadNextHigherRaster();
  }

  private initiateTiles(): void {
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', MapConfig.LAYER_OPTIONS).addTo(this.map);
  }

  private setInitialLocation(): void {
    this.locationDataService.setLocation({
      zoomLevel: this.map.getZoom(),
      northWest: this.map.getBounds().getNorthWest(),
      southWest: this.map.getBounds().getSouthWest(),
      southEast: this.map.getBounds().getSouthEast(),
      northEast: this.map.getBounds().getNorthEast()
    });
  }

  private initiateEventListener(): void {
    // this.map.on('click', this.onClick);
    this.map.on('moveend', this.onDrag);
    this.map.on('zoomend', this.onZoom)
  }

  private readonly onZoom = (event: LeafletEvent) => {
    // Checks if there is a other raster is to be loaded but the user is to close to make the zoom meachnism work
    if (this.checkHasLastRasterLoadedForZoom()) {
      this.loadNextHigherRaster();
    }
  }

  private readonly onDrag = (event: LocationEvent) => {
    this.setActualLocation({
        zoomLevel: this.map.getZoom(),
        northWest: event.target.getBounds().getNorthWest(),
        southWest: event.target.getBounds().getSouthWest(),
        southEast: event.target.getBounds().getSouthEast(),
        northEast: event.target.getBounds().getNorthEast()
      }
    );
    if (this.checkHasLastRasterLoadedForDrag()) {
      this.loadNextHigherRaster();
    }
  }

  private loadNextHigherRaster(): void {
    console.log("Try load next higher raster")
    if (this.config.datasetRasterZoomLevel && this.config.datasetRasterZoomLevel[0]) {
      const firstRasterLevel = this.config.datasetRasterZoomLevel![0];
      for (let i = this.map.getZoom(); i >= firstRasterLevel; i--) {
        if (this.config.rasterZoomLevel.indexOf(i) > 0) {
          this.fetchGeometry(this.config.rasterZoomLevel, this.config.datasetRasterZoomLevel, {...this.location, zoom: i}, false)
        }
      }
    }
  }

  private checkHasLastRasterLoadedForZoom(): boolean {
    if (this.config.datasetRasterZoomLevel && this.config.datasetRasterZoomLevel[0]) {
      const firstRasterLevel = this.config.datasetRasterZoomLevel[0];
      const rasterHasToBeLoaded = this.map.getZoom() >= firstRasterLevel && !this.config.rasterZoomLevel.includes(this.map.getZoom());
      const isRasterLoaded = this.shapeService.isRasterLoaded();
      return rasterHasToBeLoaded && !isRasterLoaded;
    }
    return false;
  }

  private checkHasLastRasterLoadedForDrag(): boolean {
    const firstRasterLevel = this.config.rasterZoomLevel[0];
    const rasterHasToBeLoaded = this.map.getZoom() >= firstRasterLevel && !this.config.rasterZoomLevel.includes(this.map.getZoom());
    return rasterHasToBeLoaded;
  }

  private setActualLocation(location: Location): void {
    this.locationDataService.setLocation(location);
    const params: Params = {};
    params[QUERY_NAMES.MAP.LAT] = this.map.getCenter().lat;
    params[QUERY_NAMES.MAP.LON] = this.map.getCenter().lng;
    params[QUERY_NAMES.MAP.ZOOM] = this.map.getZoom();
    this.routerDatastoreService.setParams(params);
  }

}


