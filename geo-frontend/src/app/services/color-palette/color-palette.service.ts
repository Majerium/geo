import {Injectable} from '@angular/core';
import {Palette} from "../../component/color-scheme/model/palette";
import {ColorPaletteBlue, ColorPaletteYellowOrangeRed} from "../../component/color-scheme/model/colorPalette";
import {RouterDatastoreService} from "../../datastore/router/services/router-datastore.service";

export type ColorScheme = 'WHITE_TO_RED' | 'WHITE_TO_BLUE';

@Injectable({
  providedIn: 'root'
})
export class ColorPaletteService {

  private selectedColorScheme: ColorScheme;

  constructor(private readonly routerDatastoreService: RouterDatastoreService) {
    this.routerDatastoreService.getColorScheme().subscribe((scheme: ColorScheme) => this.selectedColorScheme = scheme);
  }

  private palettes: { [key: string]: Palette} = {
    'WHITE_TO_RED': new ColorPaletteYellowOrangeRed(),
    'WHITE_TO_BLUE': new ColorPaletteBlue()
  }

  public getColorSchemeNames(): Array<ColorScheme> {
    return Object.keys(this.palettes) as Array<ColorScheme>;
  }

  public getPalettesColor(value: number): string {
    return this.palettes[this.selectedColorScheme].getColor(value);
  }

  public getCompareColor(filter: number, compare: number): string {
    return this.palettes[this.selectedColorScheme].getColorForCompare(filter, compare);
  }

  public getCompareColors(): [string, string, string] {
    return this.palettes[this.selectedColorScheme].getColorsCompareColors();
  }
}
