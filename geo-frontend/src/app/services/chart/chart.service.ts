import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';

import * as chartActions from '../../datastore/chart/actions/chart';
import {GraphResponse, PointResponse} from "../api/generated/data";
import {TranslateService} from "@ngx-translate/core";
import * as moment from "moment";
import * as Highcharts from 'highcharts';

type SeriesPoint = {
  time: number,
  actualValue?: number,
  isInPercentage?: boolean,
  isCompareMode?: boolean,
  x?: number,
  y: number,
  isDummy?: boolean
}

@Injectable({
  providedIn: 'root'
})
export class ChartService {

  constructor(public datePipe: DatePipe, private translate: TranslateService) { }

  public getTitle(title: string): chartActions.SetTitel {
     return new chartActions.SetTitel({ text: title });
  }

  public processKpis(
    kpis: Array<GraphResponse>,
    selectedKpi: string,
    isCompareGraph: boolean,
    isCompareMode: boolean,
    isInPercentage: boolean,
    hasDifferentKpi: boolean,
    isCumulative: boolean,
    from: moment.Moment,
    to: moment.Moment
  ): chartActions.AddMultipleSeries {
    const kpiSeries: Array<Highcharts.SeriesOptionsType> = new Array<Highcharts.SeriesOptionsType>();
    if (!isInPercentage) this.addPointSeries(kpis, kpiSeries, selectedKpi, isCompareGraph, isCompareMode, hasDifferentKpi, from, to, isInPercentage, isCumulative);
    else this.addPercentagePointSeries(kpis, kpiSeries, selectedKpi, isCompareGraph, isCompareMode, hasDifferentKpi, from, to, isCumulative);
    return new chartActions.AddMultipleSeries(kpiSeries, isCompareGraph);
  }

  private getEmptyDummySeries(from: moment.Moment, to: moment.Moment, isCompareMode: boolean, isPercentage: boolean): { [key: number]: SeriesPoint } {
    const indexedEmptySeries: { [key: number]: SeriesPoint } = {};
    let time: number;
    for(;from.isBefore(to); from.add(1, 'days')) {
      time = from.unix() * 1000;
      indexedEmptySeries[time] = { time: time, y: 0, x: time, isDummy: true, isCompareMode: isCompareMode, isInPercentage: isPercentage};
    }
    console.log(Object.entries(indexedEmptySeries).length)
    return indexedEmptySeries;
  }

  private getSeries(graphResponse: GraphResponse, selectedKpi: string, isCompareGraph: boolean, isCompareMode, hasDifferentKpi: boolean, from: moment.Moment, to: moment.Moment, isInPercentage: boolean, isCumulative: boolean): Highcharts.SeriesOptionsType {
    if (graphResponse.name !== selectedKpi) return;
    const name: string = this.translate.instant(`KEY_${graphResponse.name}`)
    const indexedDummySeries: { [key: number]: SeriesPoint } = this.getEmptyDummySeries(from, to, isCompareMode, false);
    let responsePoint: SeriesPoint;
    let relevantDummySeriesIndex: number;
    let sum = 0;
    for (let i = 0; i < graphResponse.graph.length; i++) {
      responsePoint = this.getPoint(isCompareMode, isCumulative, graphResponse.graph[i], sum);
      if (isCumulative) sum += (responsePoint.y - sum);
      relevantDummySeriesIndex = responsePoint.x;
      indexedDummySeries[relevantDummySeriesIndex] = responsePoint;
    }
    return {
      visible: graphResponse.name === selectedKpi,
      name:  isCompareGraph && !hasDifferentKpi ? `${name}-2` : name,
      data: isCompareMode ?
        this.toCompareSeries(Object.values(indexedDummySeries)) :
        Object.values(indexedDummySeries)
    } as any;
  }

  private toCompareSeries(series: Array<SeriesPoint>): Array<SeriesPoint> {
    const compareSeries: Array<SeriesPoint> = [];
    for (let i = 0; i < series.length; i++) compareSeries.push({ ...series[i], x: i})
    return compareSeries;
  }

  private getSumValue(graphResponse: GraphResponse): number {
    return graphResponse.graph.map((value: PointResponse) => value.yvalue).reduce((a, b) => a+b, 0)
  }

  private getMaxValue(graphResponse: GraphResponse): number {
    return graphResponse.graph.length > 1 ?
        graphResponse.graph.sort((first: PointResponse, second: PointResponse) => first.yvalue - second.yvalue)[graphResponse.graph.length - 1].yvalue :
        graphResponse.graph.length === 0 ?
        0 :
        graphResponse.graph[0].yvalue
        ;
  }

  // Bei Prozent (also index) wird es an die falsche stelle gesetzt
  private getPercentageSeries(graphResponse: GraphResponse, selectedKpi: string, isCompareGraph: boolean, isCompareMode, hasDifferentKpi: boolean, from: moment.Moment, to: moment.Moment, isCumulative: boolean): Highcharts.SeriesOptionsType {
    if (graphResponse.name !== selectedKpi) return;
    const name: string = this.translate.instant(`KEY_${graphResponse.name}_PERCENTAGE`)
    const indexedDummySeries: { [key: number]: SeriesPoint } = this.getEmptyDummySeries(from, to, isCompareMode, true);
    const maxValue: number = isCumulative ? this.getSumValue(graphResponse) : this.getMaxValue(graphResponse);
    let responsePoint: SeriesPoint;
    let relevantDummySeriesIndex: number;
    let actualPoint: PointResponse;
    let sum = 0;
    for (let i = 0; i < graphResponse.graph.length; i++) {
      actualPoint = graphResponse.graph[i];
      responsePoint = this.getPercentagePoint(isCompareMode, isCumulative, i, actualPoint, sum, maxValue);
      relevantDummySeriesIndex = isCompareMode ? responsePoint.time : responsePoint.x;
      if (isCumulative) sum += (responsePoint.y - sum);
      indexedDummySeries[relevantDummySeriesIndex] = responsePoint;
    }
    return {
      visible: graphResponse.name === selectedKpi,
      name:  isCompareGraph && !hasDifferentKpi ? `${name}-2` : name,
      data: isCompareMode ?
        this.toCompareSeries(Object.values(indexedDummySeries)) :
        Object.values(indexedDummySeries)
    } as any;
  }

  private addPointSeries(kpis: Array<GraphResponse>,
     kpiSeries: Array<Highcharts.SeriesOptionsType>,
     selectedKpi: string,
     isCompareGraph: boolean,
     isCompareMode: boolean,
     hasDifferentKpi: boolean,
     from: moment.Moment,
     to: moment.Moment,
     isInPercentage: boolean,
     isCumulative: boolean
  ): void {
    kpis.forEach(graph => {
      let series: Highcharts.SeriesOptionsType = this.getSeries(graph, selectedKpi, isCompareGraph, isCompareMode, hasDifferentKpi, from, to, isInPercentage, isCumulative);
      if (series)kpiSeries.push(this.getMaxValues(series));
    })
  }

  private addPercentagePointSeries(
    kpis: Array<GraphResponse>,
    kpiSeries: Array<Highcharts.SeriesOptionsType>,
    selectedKpi: string,
    isCompareGraph: boolean,
    isCompareMode: boolean,
    hasDifferentKpi: boolean,
    from: moment.Moment,
    to: moment.Moment,
    isCumulative: boolean
  ): void  {
    kpis.forEach(graph => {
      let series: Highcharts.SeriesOptionsType = this.getPercentageSeries(graph, selectedKpi, isCompareGraph, isCompareMode, hasDifferentKpi, from, to, isCumulative);
      if (series) kpiSeries.push(this.getMaxValues(series));
    })
  }

  private getMaxValues(series: Highcharts.SeriesOptionsType, maxValues: number = 999) {
    if(series['data'].length > 999) {
      series['data'] = series['data'].splice( series['data'].length - maxValues, series['data'].length);
      console.log("Exceeded max values of " + maxValues + " using last " + maxValues + " values.")
    }
    return series;
  }

  private getPoint(isCompareMode: boolean, isCumulative: boolean, point: PointResponse, sum: number): SeriesPoint {
    const time: moment.Moment = moment(point.xValue);
    const newPoint: SeriesPoint = {
      time: time.unix() * 1000,
      isCompareMode: isCompareMode,
      isInPercentage: false,
      x: time.unix() * 1000,
      y: isCumulative ? (sum + point.yvalue) : point.yvalue
    };
    return newPoint;
  }

  private getPercentagePoint(isCompareMode: boolean, isCumulative: boolean, index: number, actual: PointResponse, sum: number, maxValue: number): SeriesPoint {
    const time: moment.Moment = moment(actual.xValue);
    const value: number = actual.yvalue / maxValue;
    const valuePercent: number = Math.round((value) * 1000 ) / 1000;
    return {
      actualValue: actual.yvalue,
      time: time.unix() * 1000,
      isInPercentage: true,
      isCompareMode: isCompareMode,
      x: isCompareMode ? index : time.unix() * 1000,
      y: isCumulative ? ( sum + (value * 100)): value * 100
    }
  }
}


