import {Injectable} from '@angular/core';
import {CompareId, Shape} from "../../model/shape/shape";
import {ShapeType} from "../../model/shape/shape-type";
import {Feature, GeoJSON} from "geojson";
import {BehaviorSubject, combineLatest, Observable, Subject} from "rxjs";
import {debounceTime, filter, takeUntil} from "rxjs/operators";
import {
  DynamicFilterRequest,
  GeoDataApiControllerService,
  ShapeDataRequest,
  ShapeDataResponse
} from "../api/generated/data";
import {FilterQueryParams} from "../../datastore/router/model/filterQueryParams";
import {ColorPaletteService, ColorScheme} from "../color-palette/color-palette.service";
import {AppConfigDatastoreService} from "../../datastore/config/services/app-config-datastore.service";
import {DynamicFilter} from "../../datastore/router/model/dynamic-filter";
import {RouterDatastoreService} from "../../datastore/router/services/router-datastore.service";

/**
 * Bevor ich mich wieder frage, warum ich das nicht im store habe:
 * - Da die Features (leaflet features) bei leaflet liegen und dagegen kann ich erstmal nichts machen.
 * Würde ich sie in den Store legen, müsste ich sie immer wieder synchronisieren und der browser hat mit doppelten Datenmengen (geojson) zu kämpfen.
 */
@Injectable({
  providedIn: 'root'
})
export class ShapeService {

  static WAIT_UNTIL_API_CALL = 1000;

  private shapes$: Observable<{ [key: string]: Shape }>;
  private shapeMap: { [key: string]: Shape } = {};
  private shapeSubject: BehaviorSubject<{[key: string]: Shape}> = new BehaviorSubject({});
  private shapeUpdate: Subject<void> = new Subject<void>();
  private shapeUpdate$: Observable<void>;
  private filterShapeHttpSubscriptions$: { [key: string]: Subject<void> } = { };
  private compareShapeHttpSubscriptions$: { [key: string]: Subject<void> } = { };

  private filter$: Observable<FilterQueryParams>;
  private filter: FilterQueryParams;
  private isCompare: boolean;
  private selectedKpi: string;
  private selectedColorScheme: ColorScheme;
  private rasterLoaded: boolean = false;
  private countryLoaded: boolean = false;

  constructor(
    private readonly colorPaletteService: ColorPaletteService,
    private readonly geoDataApi: GeoDataApiControllerService,
    private readonly configDataService: AppConfigDatastoreService,
    private readonly routerDatastoreService: RouterDatastoreService
  ) {
    this.filter$ = this.routerDatastoreService.getFilterQueryParams();
    this.shapes$ = this.shapeSubject.asObservable();
    this.shapeUpdate$ = this.shapeUpdate.asObservable();
    this.shapes$.subscribe((shapeMap: {[key: string]: Shape}) => this.shapeMap = shapeMap);
    this.filter$.subscribe((filter: FilterQueryParams) => this.filter = filter);
    this.routerDatastoreService.isCompare().subscribe((isCompare: boolean) => this.isCompare = isCompare);
    this.routerDatastoreService.getColorScheme().subscribe((scheme: ColorScheme) => this.selectedColorScheme = scheme);
    this.routerDatastoreService.getFilterKpi().subscribe((kpi: string) => this.selectedKpi = kpi);

    combineLatest([
      this.routerDatastoreService.getFilterKpi(),
      this.routerDatastoreService.getColorScheme(),
      this.routerDatastoreService.isCompare()
    ]).subscribe(([kpi, colorScheme, isCompare]) =>  this.updateShapeStyles(kpi, colorScheme, isCompare));

    combineLatest([this.filter$, this.routerDatastoreService.isCompare(), this.routerDatastoreService.getLayoutType()])
      .pipe(
        filter(([filter, isCompare, layoutType]) =>  { return layoutType === 'map' }),
        debounceTime(ShapeService.WAIT_UNTIL_API_CALL),
      ).subscribe(([filter, isCompare, isMapVisible]) => this.reloadShapeData(isCompare, false, true));

    combineLatest([this.shapes$, this.routerDatastoreService.isCompare(), this.routerDatastoreService.getLayoutType()]).pipe(
      filter(([filter, isCompare, layoutType]) => { return layoutType === 'map' }),
      debounceTime(ShapeService.WAIT_UNTIL_API_CALL)
    ).subscribe(([shapes, isCompare, isMapVisible]) => this.reloadShapeData(isCompare,true)); //Here API call
  }

  public discardData(): void {
    const allSubscriptions = [...Object.values(this.filterShapeHttpSubscriptions$), ...Object.values(this.compareShapeHttpSubscriptions$)]
    console.log('Num of http subscriptions to cancel: ' + allSubscriptions.length)
    allSubscriptions.forEach(subscription =>  this.abortRequest(subscription));
    this.shapeSubject.next({});
  }

  public getShapeUpdates$(): Observable<any> {
    return this.shapeUpdate$;
  }

  public getShapes$(): Observable<{ [key: string]: Shape }> {
    return this.shapes$;
  }

  public getShapes(): { [key: string]: Shape } {
    return this.shapeMap;
  }

  public addShape(shapeType: ShapeType, key: string, feature: GeoJSON, layer: any): void {
    if (!this.shapeMap.hasOwnProperty(key)) {
      this.shapeMap[key] = new Shape(this.colorPaletteService, key, shapeType, feature, layer);
      this.shapeSubject.next(this.shapeMap);
      this.setLastTypeLoaded(shapeType);
    }
  }

  public unselectAllShape(): void {
    Object.values(this.shapeMap)
      .filter((shape:Shape) => shape.leafLetLayer.isSelected)
      .forEach((shape:Shape) => shape.unsetShape());
  }

  public isRasterLoaded(): boolean {
    return this.rasterLoaded;
  }

  public isCountryLoaded(): boolean {
    return this.countryLoaded;
  }

  private setLastTypeLoaded(shapeType: ShapeType): void {
    if (shapeType === ShapeType.COUNTRY) {
      this.countryLoaded = true;
      this.rasterLoaded = false;
    } else if (shapeType === ShapeType.HEXAGON) {
      this.rasterLoaded = true;
      this.countryLoaded = false;
    }
  }

  private updateShapeStyles(kpi: string, colorScheme: string, isCompare: boolean): void {
    Object.values(this.shapeMap).forEach((shape:Shape) => shape.updateStyle(kpi, colorScheme, isCompare))
  }

  public getGeoHashesOfDisplayedFeatures(): Array<String> {
    return Object.keys(this.shapeMap);
  }

  private reloadShapeData(isCompare: boolean, deltaOnly: boolean = false, abortPendingRequests = false): void {
    Object.values(this.shapeMap).filter((shape: Shape) => (deltaOnly && !shape.hasData) || !deltaOnly).forEach((shape: Shape) => this.loadShapeData(shape.id, shape.feature, isCompare, abortPendingRequests));
  }

  private loadShapeData(key: string, feature: GeoJSON, isCompare: boolean, abortPendingRequests = false): void {
    if (isCompare) this.loadCompareShapeData(key, feature, abortPendingRequests);
    this.loadFilterShapeData(key, feature, abortPendingRequests)
  }

  private loadFilterShapeData(key: string, feature: GeoJSON, abortPendingRequests = false): void {
    const shapeRequest: ShapeDataRequest = this.getShapeDataRequest(feature, false);
    if (abortPendingRequests) {
      let subscription$: Subject<void> = this.filterShapeHttpSubscriptions$[key];
      this.abortRequest(subscription$);
      subscription$ = new Subject<void>();
      this.filterShapeHttpSubscriptions$[key] = subscription$;
      this.geoDataApi.getKPIbyFilterShapeRequest(shapeRequest).pipe(takeUntil(this.filterShapeHttpSubscriptions$[key])).subscribe((response: ShapeDataResponse) => this.onSuccess(key, response))
    }
    else this.geoDataApi.getKPIbyFilterShapeRequest(shapeRequest).subscribe((response: ShapeDataResponse) => this.onSuccess(key, response));
  }

  private loadCompareShapeData(key: string, feature: GeoJSON, abortPendingRequests = false): void {
    const shapeRequest: ShapeDataRequest = this.getShapeDataRequest(feature, true);
    if (abortPendingRequests) {
      let subscription$: Subject<void> = this.compareShapeHttpSubscriptions$[key];
      this.abortRequest(subscription$);
      subscription$ = new Subject<void>();
      this.compareShapeHttpSubscriptions$[key] = subscription$;
      this.geoDataApi.getKPIbyFilterShapeRequest(shapeRequest).pipe(takeUntil(this.compareShapeHttpSubscriptions$[key])).subscribe((response: ShapeDataResponse) => this.onSuccess(key, response, "COMPARE_1"));
    }
    else this.geoDataApi.getKPIbyFilterShapeRequest(shapeRequest).subscribe((response: ShapeDataResponse) => this.onSuccess(key, response, "COMPARE_1"));
  }

  private abortRequest(subject: Subject<void>): void {
    if (subject && !subject.isStopped) {
      subject.next();
      subject.unsubscribe();
      console.log('HTTP Subscription canceled')
    }
  }

  private onSuccess(key: string, response: ShapeDataResponse, compareId?: CompareId): void {
    if (Object.keys(this.shapeMap).indexOf(key) >= 0) {
      compareId ? this.setResponseFromCompare(key, response) : this.setResponseFromFilter(key, response);
    }
  }

  private setResponseFromFilter(key: string, response: ShapeDataResponse): void {
    this.shapeMap![key].setFilterKpi(response.kpi, this.selectedColorScheme, this.selectedKpi, this.isCompare);
    this.shapeUpdate.next();
  }

  private setResponseFromCompare(key: string, response: ShapeDataResponse): void {
    this.shapeMap![key].setCompareKpi(response.kpi, this.selectedColorScheme, this.selectedKpi, this.isCompare);
    this.shapeUpdate.next();
  }

  public removeShape(key: string): void {
    if (this.shapeMap.hasOwnProperty(key)) {
      delete this.shapeMap[key];
      this.shapeSubject.next(this.shapeMap);
    }
  }

  public forceUpdate(): void {
    this.shapeSubject.next(this.shapeMap);
  }

  private getShapeDataRequest(feature: GeoJSON, isCompare: boolean): ShapeDataRequest {
    const request: ShapeDataRequest = {
      geoHash: (feature as Feature).properties['GEO_HASH'],
      feature: feature as any,
      dataSet: this.filter.filterDataset,
      filterDatasetRequest: { }
    }
    isCompare ? this.setCompareRequest(request) : this.setFilterRequest(request);
    return request;
  }

  private setFilterRequest(shapeDataRequest: ShapeDataRequest): void {
    shapeDataRequest.filterDatasetRequest = { from: this.filter.from ? this.filter.from.toDate().toISOString() : null, to: this.filter.to ? this.filter.to.toISOString() : null, dynamicFilterRequests: []};
    this.setDynamicFilter(shapeDataRequest.filterDatasetRequest.dynamicFilterRequests, this.filter.dynamicFilter);
  }

  private setCompareRequest(shapeDataRequest: ShapeDataRequest): void {
    shapeDataRequest.filterDatasetRequest = { from: this.filter.fromCompare ? this.filter.fromCompare.toISOString() : null, to: this.filter.toCompare ? this.filter.toCompare.toISOString() : null, dynamicFilterRequests: []};
    this.setDynamicFilter(shapeDataRequest.filterDatasetRequest.dynamicFilterRequests, this.filter.dynamicCompare);
  }

  private setDynamicFilter(dynamicFilter: Array<DynamicFilterRequest>, filter: { [key: string]: DynamicFilter }) {
    Object.entries(filter).filter(([key, value]) => value.useForMap).forEach(entry => dynamicFilter.push({type: entry[0], value: entry[1].value}))
  }

}
