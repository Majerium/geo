import { Injectable } from '@angular/core';
import {LayoutDataStoreService} from "../../datastore/layout/services/layout-datastore.service";
import {ActivatedRoute, Params} from "@angular/router";
import {takeUntil} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ParamsService {

  constructor(private readonly route: ActivatedRoute) { }

  private subscribeToQueryParams(): void {
    this.route.queryParams.pipe(

    ).subscribe(params => {
      this.setFilterFromParams(params);
      this.setLayoutFromParams(params);
      }
    );
  }

  private setFilterFromParams(params: Params): void {
    console.log('Params: ' + JSON.stringify(params));
  }

  private setLayoutFromParams(params: Params): void {
    console.log('Params: ' + JSON.stringify(params));
  }
}
