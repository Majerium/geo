import {ChangeDetectionStrategy, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AppConfigDatastoreService} from "./datastore/config/services/app-config-datastore.service";
import {Observable, Subject} from "rxjs";
import {TranslateService} from '@ngx-translate/core';
import {DateAdapter} from "@angular/material/core";
import {FilterLayout, RouterDatastoreService} from "./datastore/router/services/router-datastore.service";
import {Route} from "./datastore/router/model/router";
import {takeUntil} from "rxjs/operators";
import {QUERY_NAMES} from "./datastore/router/reducer";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit, OnDestroy {

  @ViewChild('drawer', { read: ElementRef }) drawer: ElementRef;

  filterLayout$: Observable<FilterLayout>;
  showSpinner$: Observable<boolean>;
  showDetails$: Observable<boolean>;
  routeData$: Observable<Route>;
  showInfo$: Observable<boolean>;

  private unsubscribe$: Subject<void> = new Subject();
  private unsubscribeFirstAction$: Subject<void> = new Subject();

  constructor(
    private readonly _adapter: DateAdapter<any>,
    private readonly translate: TranslateService,
    private readonly appConfigDataService: AppConfigDatastoreService,
    private readonly routeDataStore: RouterDatastoreService,
  ) {
    this.showDetails$ = this.routeDataStore.showDetails();
    this.showInfo$ = this.appConfigDataService.getInfoState();
    this.filterLayout$ = this.routeDataStore.getFilterLayout();
    this.routeData$ = this.routeDataStore.getRoute();
    this.showSpinner$ = this.appConfigDataService.getSpinnerState();
    this.appConfigDataService.fetchZoomLevel();
    this.appConfigDataService.fetchDatasets();
    this.translate.addLangs(['en']);
    this.translate.setDefaultLang('de');
    this._adapter.setLocale('de');
  }

  ngOnInit(): void {
    this.subscribeToRouteData();
  }

  private subscribeToRouteData(): void {
    this.routeData$.pipe(takeUntil(this.unsubscribeFirstAction$)).subscribe(route => this.setInitialFilterWhenCalledWithoutQueryParams(route));
  }

  ngOnDestroy(): void {
    if (this.unsubscribe$) {
      this.unsubscribe$.next();
      this.unsubscribe$.unsubscribe();
    }
  }

  private unsubscribeInitialAction(): void {
    if(this.unsubscribeFirstAction$ && !this.unsubscribeFirstAction$.isStopped) {
      this.unsubscribeFirstAction$.next();
      this.unsubscribeFirstAction$.unsubscribe();
    }
  }

  private setInitialFilterWhenCalledWithoutQueryParams(route: Route): void {
    if (route.url && !route.url.includes("?")) {
      this.resetChartAppConfig();
      this.unsubscribeInitialAction(); // Make sure it only work once
    };
  }

  public resetChartAppConfig(): void {
    this.routeDataStore.setDefaultConfig();
  }

  public getSidebarWidth(isSidebarVisible: boolean): string {
    const isSet: boolean = this.drawer && this.drawer.nativeElement && this.drawer.nativeElement !== undefined
    const left: string = isSet ? isSidebarVisible ? this.drawer.nativeElement.offsetWidth : 0 : 0;
    return `${left}px`;
  }

  public toggleSidebar(isSidebarVisible: boolean): void {
    this.routeDataStore.setParam(QUERY_NAMES.LAYOUT.IS_SIDEBAR_VISIBLE, RouterDatastoreService.ofBoolean(!isSidebarVisible));
  }

  public showInfo(): void {
    this.appConfigDataService.showInfo();
  }

  public showMap(): void {
    this.routeDataStore.showMap()
  }

  public showGraph(): void {
    this.routeDataStore.showGraph()
  }

  public toggleFilterVisible(isFilterExpanded: boolean): void {
    this.routeDataStore.setParam(QUERY_NAMES.LAYOUT.IS_EXPAND_FILTER, RouterDatastoreService.ofBoolean(isFilterExpanded));
  }

  public toggleColorSchemeVisible(isColorSchemeExpanded: boolean): void {
    this.routeDataStore.setParam(QUERY_NAMES.LAYOUT.IS_EXPAND_COLOR_SCHEME, RouterDatastoreService.ofBoolean(isColorSchemeExpanded));
  }

  public closeSidebar(): void {
    this.routeDataStore.setParam(QUERY_NAMES.LAYOUT.IS_SIDEBAR_VISIBLE, RouterDatastoreService.ofBoolean(false));
  }

  public clearSight(): void {
    this.routeDataStore.clearSight();
  }

}
