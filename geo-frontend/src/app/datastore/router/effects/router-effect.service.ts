import {Injectable} from '@angular/core';
import {ActivatedRoute, ParamMap, Params, Router} from "@angular/router";
import {Actions, Effect, ofType} from "@ngrx/effects";
import {Observable} from "rxjs";
import {Action} from "@ngrx/store";
import * as ParamActions from "../actions/router";
import {switchMap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class RouterEffectService {

  constructor(
    private readonly actions$: Actions,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute
  ) { }

  @Effect()
  loadParam$: Observable<Action> = this.actions$.pipe(
    ofType(ParamActions.SET_PARAM),
    switchMap((action: ParamActions.SetParam) => [this.mergeQueryParam(action)])
  );

  @Effect()
  loadParams$: Observable<Action> = this.actions$.pipe(
    ofType(ParamActions.SET_PARAMS),
    switchMap((action: ParamActions.SetMultipleParams) => [this.mergeParams(action.params)])
  );

  @Effect()
  loadComplexArrayParams$: Observable<Action> = this.actions$.pipe(
    ofType(ParamActions.SET_COMPLEX_ARRAY_PARAMS),
    switchMap((action: ParamActions.SetComplexArrayParam) => [this.mergeComplexArrayQueryParam(action)])
  );

  @Effect()
  removeArrayParam$: Observable<Action> = this.actions$.pipe(
    ofType(ParamActions.REMOVE_COMPLEX_ARRAY_PARAM),
    switchMap((action: ParamActions.RemoveComplexArrayParam) => [this.removeComplexArrayQueryParam(action)])
  );

  @Effect()
  loadSimpleArrayParams$: Observable<Action> = this.actions$.pipe(
    ofType(ParamActions.SET_SIMPLE_ARRAY_PARAM),
    switchMap((action: ParamActions.SetSimpleArrayParam) => [this.mergeSimpleArrayParam(action)])
  );

  @Effect()
  removeSimpleArrayParams$: Observable<Action> = this.actions$.pipe(
    ofType(ParamActions.REMOVE_SIMPLE_ARRAY_PARAM),
    switchMap((action: ParamActions.RemoveSimpleArrayParam) => [this.removeSimpleArrayQueryParam(action)])
  );

  private mergeSimpleArrayParam(action: ParamActions.SetSimpleArrayParam): ParamActions.NoAction {
    const queryParams: Params = {};
    const paramMap: ParamMap = this.activatedRoute.snapshot.queryParamMap;
    let valueToAdd: string = `${action.value};`
    if (paramMap.has(action.array) && !paramMap.get(action.array).includes(valueToAdd)) {
      valueToAdd = paramMap.get(action.array) + valueToAdd;
    }
    queryParams[action.array] = valueToAdd;
    return this.mergeParams(queryParams)
  }

  private removeSimpleArrayQueryParam(action: ParamActions.RemoveSimpleArrayParam): ParamActions.NoAction {
    const paramMap: ParamMap = this.activatedRoute.snapshot.queryParamMap;
    let valueToRemove: string = `${action.value};`
    const queryParams: Params = {};
    const removeNecessary: boolean = paramMap.has(action.array);
    if(removeNecessary) {
      let arrString: string = paramMap.get(action.array);
      arrString = arrString.replace(valueToRemove, '');
      queryParams[action.array] = arrString;
      if(arrString === '') {
        queryParams[action.array] = null;
      }
    }
    return removeNecessary ? this.mergeParams(queryParams) : new ParamActions.NoAction();
  }

  private mergeQueryParam(action: ParamActions.SetParam): ParamActions.NoAction {
    const queryParams: Params = { };
    queryParams[action.paramName] = action.paramValue;
    return this.mergeParams(queryParams);
  }

  private removeComplexArrayQueryParam(action: ParamActions.RemoveComplexArrayParam): ParamActions.NoAction {
    const regexTemplate = `${action.param}([^;]+);`;
    const regex: RegExp = new RegExp(regexTemplate, 'g');
    const paramMap: ParamMap = this.activatedRoute.snapshot.queryParamMap;
    const params: Params = {};
    const removeNecessary: boolean = paramMap.has(action.array);
    if (removeNecessary) {
      let arrString: string = paramMap.get(action.array);
      if (regex.test(arrString)) {
        arrString = arrString.replace(regex, '');
        params[action.array] = arrString;
      } if (arrString === "") {
        params[action.array] = null;
      }
    }
    return removeNecessary ? this.mergeParams(params) : new ParamActions.NoAction();
  }

  private mergeComplexArrayQueryParam(action: ParamActions.SetComplexArrayParam): ParamActions.NoAction {
    const regexTemplate = `${action.param}([^;]+);`;
    const regex: RegExp = new RegExp(regexTemplate, 'g');
    const paramMap: ParamMap = this.activatedRoute.snapshot.queryParamMap;
    const param: Params = {};
    const value: string = `${action.param}:${action.paramValue};`;
    if (paramMap.has(action.array)) {
      let arrString: string = paramMap.get(action.array);
      if (regex.test(arrString)) {
        arrString = arrString.replace(regex, value);
      }
      else {
        arrString += `${value}`;
      }
      param[action.array] = arrString;
    } else if(!paramMap.has(action.array)) {
      param[action.array] = value;
    }
    return this.mergeParams(param);
  }

  private mergeParams(params: Params): ParamActions.NoAction {
    console.log(JSON.stringify(params));
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: params,
      queryParamsHandling: "merge"
    })
    return new ParamActions.NoAction();
  }
}
