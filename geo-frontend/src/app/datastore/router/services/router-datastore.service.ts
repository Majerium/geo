import {Injectable} from '@angular/core';
import {Store} from "@ngrx/store";
import {Route} from "../model/router";
import {combineLatest, Observable} from "rxjs";
import {
  getColorScheme,
  getCompareDataset,
  getCompareDynamic,
  getCompareFrom,
  getCompareKpi,
  getCompareTo,
  getFilterDataset,
  getFilterDynamic,
  getFilterFrom,
  getFilterKpi,
  getFilterTo,
  getGraphType,
  getIsCompare,
  getIsCumulative,
  getIsFilterColorSchemeExpanded,
  getIsFilterExpanded,
  getIsPercentage,
  getIsSideBarVisible,
  getLayoutType,
  getMapQueryParams,
  getQueryParams,
  getRouteState, getSelectedShapes, getZoomLevel,
  QUERY_NAMES
} from "../reducer";
import {
  RemoveComplexArrayParam, RemoveSimpleArrayParam,
  SetComplexArrayParam,
  SetMultipleParams,
  SetParam,
  SetSimpleArrayParam
} from "../actions/router";
import {Params} from "@angular/router";
import {FilterQueryParams} from "../model/filterQueryParams";
import {filter, map, tap} from "rxjs/operators";
import * as moment from "moment";
import {DynamicFilter} from "../model/dynamic-filter";
import {ColorScheme} from "../../../services/color-palette/color-palette.service";
import {ConfigQueryParams} from "../model/configQueryParams";
import {GraphType} from "../../config/model/appConfig";
import {LayoutQueryParams, LayoutType, StrBoolean} from "../model/layoutQueryParams";
import {isNumeric} from "rxjs/internal-compatibility";

export type FilterLayoutConfig = {
  filter: FilterQueryParams,
  layout: LayoutQueryParams,
  config: ConfigQueryParams
}

export type FilterLayout = {
  filter: FilterQueryParams,
  layout: LayoutQueryParams
}

export type ConfigLayout = {
  config: ConfigQueryParams,
  layout: LayoutQueryParams
}

export type GraphConfig = {
  isInPercentage: boolean,
  cumulative: boolean
}

@Injectable({
  providedIn: 'root'
})
export class RouterDatastoreService {

  constructor(private _store: Store<Route>) { }

  public getRoute(): Observable<Route> {
    return this._store.select(getRouteState);
  }

  public getGraphConfig(): Observable<GraphConfig> {
    return combineLatest([
      this._store.select(getIsPercentage),
      this._store.select(getIsCumulative)
    ]).pipe(
      map(([isPercentage, isCumulative]) => {
        return {
          cumulative: RouterDatastoreService.ofStrBoolean(isCumulative),
          isInPercentage: RouterDatastoreService.ofStrBoolean(isPercentage),
        }
      })
    )
  }

  public getFilterLayout(): Observable<FilterLayout> {
    return combineLatest([this.getFilterQueryParams(), this.getLayoutQueryParams()])
      .pipe(
        map(([filter, layout]) => {
          return {
            filter: filter,
            layout: layout,
          }
        })
      );
  }

  public getConfigLayout(): Observable<ConfigLayout> {
    return combineLatest([this.getConfigQueryParams(), this.getLayoutQueryParams()])
      .pipe(
        map(([config, layout]) => {
          return {
            config: config,
            layout: layout,
          }
        })
      );
  }

  public getFilterLayoutConfigQueryParams(): Observable<FilterLayoutConfig> {
    return combineLatest([this.getFilterQueryParams(), this.getLayoutQueryParams(), this.getConfigQueryParams()])
      .pipe(
        map(([filter, layout, config]) => {
          return {
            filter: filter,
            layout: layout,
            config: config
          }
        })
      );
  }

  public getConfigQueryParams(): Observable<ConfigQueryParams> {
    return combineLatest([
      this._store.select(getColorScheme),
      this._store.select(getIsPercentage),
      this._store.select(getIsCumulative),
      this._store.select(getGraphType),
      this._store.select(getFilterKpi),
      this._store.select(getCompareKpi)
    ]).pipe(
      map(([colorScheme, isInPercentage, isCumulative, graphType, filterKpi, compareKpi]) => {
        return {
          selectedColorScheme: colorScheme,
          inPercentage: RouterDatastoreService.ofStrBoolean(isInPercentage),
          cumulative: RouterDatastoreService.ofStrBoolean(isCumulative),
          graphType: graphType,
          filterKpi: filterKpi,
          compareKpi: compareKpi
        }
      })
    )
  }

  /**
   * Gets the Layout rellevant filter options.
   * Its constructed here in order to have the subscribtions when therer are changes only
   */
  public getLayoutQueryParams(): Observable<LayoutQueryParams> {
    return combineLatest([
      this._store.select(getIsCompare),
      this._store.select(getIsSideBarVisible),
      this._store.select(getLayoutType),
      this._store.select(getIsFilterExpanded),
      this._store.select(getIsFilterColorSchemeExpanded),
    ]).pipe(
      map(([isCompare, isSidebarVisible, layoutType, isExpandFilter, isExpandColorScheme]) => {
        return {
          compare: RouterDatastoreService.ofStrBoolean(isCompare),
          sidebarVisible: RouterDatastoreService.ofStrBoolean(isSidebarVisible),
          layoutType: layoutType,
          expandFilter: RouterDatastoreService.ofStrBoolean(isExpandFilter),
          expandColorScheme: RouterDatastoreService.ofStrBoolean(isExpandColorScheme)
        }
      })
    )
  }


  /**
   * Get the FilterQueryParams from query params.
   * Subcribtion fires, when the filter values are changed only
   */
  public getFilterQueryParams(): Observable<FilterQueryParams> {
    return combineLatest([
      this._store.select(getFilterFrom),
      this._store.select(getFilterTo),
      this._store.select(getCompareFrom),
      this._store.select(getCompareTo),
      this._store.select(getFilterDataset),
      this._store.select(getCompareDataset),
      this._store.select(getFilterDynamic),
      this._store.select(getCompareDynamic)
    ]).pipe(
      map(([filterFrom, filterTo, compareFrom, compareTo, filterDataset, compareDataset, dynamicFilter, dynamicCompare]) => {
        return {
          from: this.toDateOrNull(filterFrom),
          to: this.toDateOrNull(filterTo),
          toCompare: this.toDateOrNull(compareTo),
          fromCompare: this.toDateOrNull(compareFrom),
          filterDataset: filterDataset,
          compareDataset: compareDataset,
          dynamicFilter: this.mergeDynamicFilter(dynamicFilter),
          dynamicCompare: this.mergeDynamicFilter(dynamicCompare)
        }
      })
    )
  }

  /**
   * Get the FilterQueryParams from query params.
   * Subcribtion fires, when the filter values are changed only
   */
  public getShapesArray(): Observable<Array<string>> {
    return this._store.select(getSelectedShapes).pipe(
      map((arrayString: string) => {
        return this.arrStringToArray(arrayString)
      })
    )
  }

  public showDetails(): Observable<boolean> {
    return this.getShapesArray().pipe(
      map((shapes: Array<string>) => {
        return shapes.length > 0;
      })
    )
  }

  private arrStringToArray(arrString: string): Array<string> {
    return arrString ? arrString.split(';').length === 2 ? [arrString.split(';')[0]] : [...arrString.split(';')] : []
  }

  public getGraphType(): Observable<GraphType> {
    return this._store.select(getGraphType)
  }

  public getZoomLevel(): Observable<string> {
    return this._store.select(getZoomLevel)
  }

  private toDateOrNull(date: string): null | moment.Moment {
    return date ? moment(date, 'DD.MM.YYYY') : null;
  }

  public getFilterKpi(): Observable<string> {
    return this._store.select(getFilterKpi);
  }

  public getCompareKpi(): Observable<string> {
    return this._store.select(getCompareKpi);
  }

  public removeMapRelevantParams(): void {
    const params: Params = {};
    params[QUERY_NAMES.MAP.ZOOM] = null;
    params[QUERY_NAMES.MAP.LAT] = null;
    params[QUERY_NAMES.MAP.LON] = null;
    params[QUERY_NAMES.MAP.SHAPES] = null;
    params[QUERY_NAMES.CONFIG.GRAPH_TYPE] = 'area';
    this.setParams(params);
  }

  public removeChartRelevantParams(): void {
    const params: Params = {};
    params[QUERY_NAMES.CONFIG.GRAPH_TYPE] = null;
    params[QUERY_NAMES.MAP.ZOOM] = '6';
    params[QUERY_NAMES.MAP.LAT] = '51.09473074607536';
    params[QUERY_NAMES.MAP.LON] = '10.270452086348811';
    this.setParams(params);
  }

  private mergeDynamicFilter(dynamicFilter: string): { [key: string]: DynamicFilter } {
    const dynamicFilters: { [key: string]: DynamicFilter } = { };
    let filter: { type: string, filter: DynamicFilter };
    if (dynamicFilter && typeof dynamicFilter === 'string') {
      const filterConfigs: Array<string> = dynamicFilter.split(';');
      for (let i = 0; i < filterConfigs.length - 1; i++) {
        filter = this.toDynamicFilter(filterConfigs[i]);
        dynamicFilters[filter.type] = filter.filter;
      }
    }
    return dynamicFilters;
  }

  private toDynamicFilter(filterConfig: string): { type: string, filter: DynamicFilter } {
    const filterValues: Array<string> = filterConfig.replace(';', '').split(':');
    return  {
      type: filterValues[0],
      filter: {
        type: filterValues[0],
        value: filterValues[1],
        useForGraph: filterValues[2] === 'true',
        useForMap: filterValues[3] === 'true'
      }
    }
  }

  public isGraph(): Observable<boolean> {
    return this._store.select(getLayoutType).pipe(
      map((layoutType: LayoutType) => {
        return layoutType === 'graph';
      })
    )
  }

  public getLayoutType(): Observable<LayoutType> {
    return this._store.select(getLayoutType);
  }

  public getQueryParams(): Observable<Params> {
   return this._store.select(getQueryParams)
  }

  public isCompare(): Observable<boolean> {
    return this._store.select(getIsCompare).pipe(
      map((isCompare: StrBoolean) => RouterDatastoreService.ofStrBoolean(isCompare))
    )
  }

  public getColorScheme(): Observable<ColorScheme> {
    return this._store.select(getColorScheme)
  }

  public getMapQueryParams(): Observable<Params> {
    return this._store.select(getMapQueryParams)
  }

  public setParam(paramName: string, paramValue: string): void {
    this._store.dispatch(new SetParam(paramName, paramValue));
  }

  public setComplexArrayParam(array: string, param: string, paramValue: string): void {
    this._store.dispatch(new SetComplexArrayParam(array, param, paramValue));
  }

  public removeComplexArrayParam(array: string, param: string): void {
    this._store.dispatch(new RemoveComplexArrayParam(array, param))
  }

  public setSimpleArrayParam(array: string, value: string): void {
    this._store.dispatch(new SetSimpleArrayParam(array, value));
  }

  public removeSimpleArrayParam(array: string, value: string): void {
    this._store.dispatch(new RemoveSimpleArrayParam(array, value))
  }

  public setParams(paras: Params): void {
    this._store.dispatch(new SetMultipleParams(paras));
  }

  public showMap(): void {
    this.setParam(QUERY_NAMES.LAYOUT.LAYOUT_TYPE, 'map')
  }

  public showGraph(): void  {
    const params: Params = {};
    params[QUERY_NAMES.LAYOUT.LAYOUT_TYPE] = 'graph';
    params[QUERY_NAMES.MAP.SHAPES] = null;
    this.setParams(params);
  }

  public setDefaultConfig(): void {
    console.log("Set initial values")
    // Set Initial value to params
    const from: moment.Moment = moment().subtract(20, "days").startOf('day');
    const to: moment.Moment = moment().startOf('day');
    const params: Params = {};
    params[QUERY_NAMES.FILTER.FILTER_DATASET] = 'RKI';
    params[QUERY_NAMES.FILTER.FILTER_FROM] = from.format("DD.MM.YYYY");
    params[QUERY_NAMES.FILTER.FILTER_TO] = to.format("DD.MM.YYYY");
    params[QUERY_NAMES.LAYOUT.IS_COMPARE] = 'false';
    params[QUERY_NAMES.CONFIG.COLOR_SCHEME] = 'WHITE_TO_RED';
    params[QUERY_NAMES.CONFIG.FILTER_KPI] = 'COUNT_CASE';
    params[QUERY_NAMES.MAP.ZOOM] = '6';
    params[QUERY_NAMES.MAP.LAT] = '51.09473074607536';
    params[QUERY_NAMES.MAP.LON] = '10.270452086348811';
    params[QUERY_NAMES.LAYOUT.LAYOUT_TYPE] = 'map';
    params[QUERY_NAMES.CONFIG.GRAPH_TYPE] = 'area';
    params[QUERY_NAMES.FILTER.DYNAMIC.COMPARE] = null;
    params[QUERY_NAMES.FILTER.DYNAMIC.FILTER] = null;
    params[QUERY_NAMES.MAP.SHAPES] = null;
    this.setParams(params);
  }

  public setCompareMode(queryConfig: FilterLayoutConfig): void {
    const params: Params = {};
    params[QUERY_NAMES.LAYOUT.IS_COMPARE] = RouterDatastoreService.ofBoolean(true);
    params[QUERY_NAMES.FILTER.COMPARE_DATASET] = queryConfig.filter.filterDataset;
    params[QUERY_NAMES.FILTER.COMPARE_FROM] = this.getDateFromMomentOrNull(queryConfig.filter.from);
    params[QUERY_NAMES.FILTER.COMPARE_TO] = this.getDateFromMomentOrNull(queryConfig.filter.to);
    params[QUERY_NAMES.CONFIG.COMPARE_KPI] = queryConfig.config.filterKpi;
    params[QUERY_NAMES.FILTER.DYNAMIC.COMPARE] =  Object.values(queryConfig.filter.dynamicFilter)
      .reduce((previousValue, currentValue) =>
      previousValue + `${currentValue.type}:${currentValue.value}:${currentValue.useForGraph}:${currentValue.useForMap};`
    , '');
    this.setParams(params);
  }

  public setSingleMode(queryConfig: FilterLayoutConfig): void {
    const params: Params = {};
    params[QUERY_NAMES.FILTER.COMPARE_DATASET] = null;
    params[QUERY_NAMES.FILTER.COMPARE_FROM] = null;
    params[QUERY_NAMES.FILTER.COMPARE_TO] = null;
    params[QUERY_NAMES.FILTER.DYNAMIC.COMPARE] = null;
    params[QUERY_NAMES.CONFIG.COMPARE_KPI] = null;
    params[QUERY_NAMES.LAYOUT.IS_COMPARE] = RouterDatastoreService.ofBoolean(false);
    this.setParams(params);
  }

  private getDateFromMomentOrNull(moment: moment.Moment): string | null {
    return moment ? moment.format("DD.MM.YYYY") : null;
  }

  public getFilterDataset(): Observable<string> {
    return this._store.select(getFilterDataset);
  }

  public getCompareDataset(): Observable<string> {
    return this._store.select(getCompareDataset);
  }

  public clearSight(): void {
    const params: Params = { };
    params[QUERY_NAMES.LAYOUT.IS_SIDEBAR_VISIBLE] = 'false';
    params[QUERY_NAMES.MAP.SHAPES] = null;
    this.setParams(params);
  }

  static ofStrBoolean(strBoolean: StrBoolean): boolean {
    return strBoolean === 'true';
  }

  static ofBoolean(boolean: boolean): StrBoolean {
    return boolean ? 'true' : 'false';
  }

}
