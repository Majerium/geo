import { TestBed } from '@angular/core/testing';

import { RouterDatastoreService } from './router-datastore.service';

describe('RouterDatastoreService', () => {
  let service: RouterDatastoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RouterDatastoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
