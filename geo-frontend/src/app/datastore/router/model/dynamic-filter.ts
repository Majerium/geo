export interface DynamicFilter {
  type: string;
  value: string;
  useForMap: boolean;
  useForGraph: boolean;
}
