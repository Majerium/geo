import {Data, Params} from '@angular/router';
import {RouterReducerState} from '@ngrx/router-store';

export type MergedRouteReducerState = RouterReducerState<Route>;

export interface Route {
  url: string;
  queryParams: Params;
  params: Params;
  data: Data;
}
