import {ColorScheme} from "../../../services/color-palette/color-palette.service";

export type GraphType = 'line' | 'column' | 'area';

export interface ConfigQueryParams {
  filterKpi?: string,
  compareKpi?: string,
  selectedColorScheme?: ColorScheme;
  graphType?: GraphType,
  inPercentage: boolean,
  cumulative: boolean
}
