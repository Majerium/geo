import * as moment from 'moment';
import {DynamicFilter} from "./dynamic-filter";

export interface FilterQueryParams {
  fromCompare?: moment.Moment;
  toCompare?: moment.Moment,
  from?: moment.Moment;
  to?: moment.Moment,
  filterDataset?: string,
  compareDataset?: string,
  dynamicFilter?: { [key: string]: DynamicFilter },
  dynamicCompare?: { [key: string]: DynamicFilter }
}
