export type LayoutType = 'map' | 'graph';
export type StrBoolean = 'true' | 'false'

export interface LayoutQueryParams {
  sidebarVisible?: boolean;
  compare?: boolean;
  expandFilter?: boolean;
  expandColorScheme?: boolean;
  layoutType?: LayoutType;
}
