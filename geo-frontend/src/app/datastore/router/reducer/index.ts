import { routerReducer, RouterReducerState } from "@ngrx/router-store";
import { ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import {MergedRouteReducerState, Route} from "../model/router";

export interface State {
  router: RouterReducerState<any>;
}

export const routeReducer: ActionReducerMap<State> = {
  router: routerReducer
};

export const getRouterReducerState = createFeatureSelector<MergedRouteReducerState>('router');

export const getRouteState = createSelector(getRouterReducerState, (routerReducerState) => routerReducerState ? routerReducerState.state : {} as Route);
export const getQueryParams = createSelector(getRouterReducerState, (routerReducerState) => routerReducerState ? routerReducerState.state.queryParams : {});

export const getFilterQueryParams = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams : {});

export const getSelectedShapes = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams[QUERY_NAMES.MAP.SHAPES] : '');

/* Here are the config queryParams  */

export const getColorScheme = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams[QUERY_NAMES.CONFIG.COLOR_SCHEME] : '');

export const getIsPercentage = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams[QUERY_NAMES.CONFIG.IN_PERCENTAGE] : 'false');

export const getIsCumulative = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams[QUERY_NAMES.CONFIG.CUMULATIVE] : 'false');

export const getGraphType = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams[QUERY_NAMES.CONFIG.GRAPH_TYPE] : '');

export const getFilterKpi = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams[QUERY_NAMES.CONFIG.FILTER_KPI] : '');

export const getCompareKpi = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams[QUERY_NAMES.CONFIG.COMPARE_KPI] : '');

/* Here are the FilterQueryParams queryParams and Selectors for it. */

export const getCompareDynamic = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams[QUERY_NAMES.FILTER.DYNAMIC.COMPARE] : {});

export const getFilterDynamic = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams[QUERY_NAMES.FILTER.DYNAMIC.FILTER] : {});

export const getFilterFrom = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams[QUERY_NAMES.FILTER.FILTER_FROM] : {});

export const getFilterTo = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams[QUERY_NAMES.FILTER.FILTER_TO] : {});

export const getCompareFrom = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams[QUERY_NAMES.FILTER.COMPARE_FROM] : {});

export const getCompareTo = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams[QUERY_NAMES.FILTER.COMPARE_TO] : {});

export const getFilterDataset = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams[QUERY_NAMES.FILTER.FILTER_DATASET] : {});

export const getCompareDataset = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams[QUERY_NAMES.FILTER.COMPARE_DATASET] : {});

/* Here are the Layout queryParams and Selectors for it. */

export const getIsFilterExpanded = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams[QUERY_NAMES.LAYOUT.IS_EXPAND_FILTER] : false);

export const getIsFilterColorSchemeExpanded = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams[QUERY_NAMES.LAYOUT.IS_EXPAND_COLOR_SCHEME] : false);

export const getLayoutType = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams[QUERY_NAMES.LAYOUT.LAYOUT_TYPE] : false);

export const getIsCompare = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams[QUERY_NAMES.LAYOUT.IS_COMPARE] : 'false');

export const getIsSideBarVisible = createSelector(getRouterReducerState, (routerReducerState) =>
  routerReducerState ? routerReducerState.state.queryParams[QUERY_NAMES.LAYOUT.IS_SIDEBAR_VISIBLE] : false);

export const getZoomLevel = createSelector(getRouterReducerState, (routerReducerState) => routerReducerState ?
  routerReducerState.state.queryParams.zoom : null);

export const getMapQueryParams = createSelector(getRouterReducerState, (routerReducerState) => routerReducerState ? {
  'lat': routerReducerState.state.queryParams.lat,
  'lon': routerReducerState.state.queryParams.lon,
  'zoom': routerReducerState.state.queryParams.zoom
} : { });

export const QUERY_NAMES = {
  MAP: {
    LAT: 'lat',
    LON: 'lon',
    ZOOM: 'zoom',
    SHAPES: 'shapes'
  },
  FILTER: {
    FILTER_DATASET: 'filterDataset',
    FILTER_FROM: 'filterFrom',
    FILTER_TO: 'filterTo',
    COMPARE_FROM: 'compareFrom',
    COMPARE_TO: 'compareTo',
    COMPARE_DATASET: 'compareDataset',
    DYNAMIC: {
      COMPARE: 'dC',
      FILTER: 'dF'
    }
  },
  LAYOUT: {
    IS_COMPARE: "isCompare",
    LAYOUT_TYPE: "layoutType",
    IS_SIDEBAR_VISIBLE: 'sidebarVisible',
    IS_EXPAND_FILTER: 'expandFilter',
    IS_EXPAND_COLOR_SCHEME: 'expandColorScheme'
  },
  CONFIG: {
    GRAPH_TYPE: 'graphType',
    IN_PERCENTAGE: 'inPercentage',
    CUMULATIVE: 'cumulative',
    FILTER_KPI: 'filterKpi',
    COMPARE_KPI: 'compareKpi',
    COLOR_SCHEME: 'colorScheme'
  }
}
