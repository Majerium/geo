import {Action} from "@ngrx/store";
import {Params} from "@angular/router";

export const TYPE = '[PARAMS]';
export const SET_PARAM = `${TYPE} set param`;
export const SET_PARAMS = `${TYPE} set params`;
export const NO_ACTION = `${TYPE} no action`;

export const SET_COMPLEX_ARRAY_PARAMS = `${TYPE} set complex array params`;
export const REMOVE_COMPLEX_ARRAY_PARAM = `${TYPE} remove complex array param`;

export const SET_SIMPLE_ARRAY_PARAM = `${TYPE} set simple array params`;
export const REMOVE_SIMPLE_ARRAY_PARAM = `${TYPE} remove simple array params`;

export class SetSimpleArrayParam implements Action {
  readonly type = SET_SIMPLE_ARRAY_PARAM;
  constructor(public array: string, public value: string) { }
}

export class RemoveSimpleArrayParam implements Action {
  readonly type = REMOVE_SIMPLE_ARRAY_PARAM;
  constructor(public array: string, public value: string) { }
}

export class SetComplexArrayParam implements Action {
  readonly type = SET_COMPLEX_ARRAY_PARAMS;
  constructor(public array: string, public param: string, public paramValue: string) { }
}

export class RemoveComplexArrayParam implements Action {
  readonly type = REMOVE_COMPLEX_ARRAY_PARAM;
  constructor(public array: string, public param: string) { }
}

export class SetParam implements Action {
  readonly type = SET_PARAM;
  constructor(public paramName: string, public paramValue: string) { }
}

export class SetMultipleParams implements Action {
  readonly type = SET_PARAMS;
  constructor(public params: Params) { }
}

export class NoAction implements Action {
  readonly type = NO_ACTION;
}
