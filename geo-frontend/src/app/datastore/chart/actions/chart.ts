import { Action } from '@ngrx/store'
import {FilterQueryParams} from "../../router/model/filterQueryParams";
import {GraphType} from "../../config/model/appConfig";
import {SeriesOptionsType} from "highcharts";
import * as Highcharts from 'highcharts';
import {GraphConfig} from "../../router/services/router-datastore.service";

export const TYPE: string = '[CHART]';

export const ADD_SERIES     =   `${TYPE} Add series`;
export const ADD_SERIES_MUL =   `${TYPE} Add multible series`;

export const DEL_SERIES     =   `${TYPE} Remove series`;
export const CLEAR_SERIES   =   `${TYPE} Clear series`;

export const SET_TITEL      =   `${TYPE} Set titel`;
export const SET_SUBTITEL   =   `${TYPE} Set subtitel`;
export const SET_YAXIS      =   `${TYPE} Set y-axis`;
export const SET_XAXIS      =   `${TYPE} Set x-axis`;
export const SET_GRAPH_TYPE =   `${TYPE} Set graph type`;
export const SET_TOOLTIP_OPTIONS =   `${TYPE} Set tooltip options`;

export const RESET_CHART    =   `${TYPE} Reset the chart`;
export const REMOVE_CLOSING_SERIES = `${TYPE} Remove closing series`;

export const GET_FILTER_GRAPH      =    `${TYPE} Get the filter chart data`;
export const GET_COMPARE_GRAPH      =    `${TYPE} Get the compare chart data`;

export class GetFilterGraph implements Action {
  readonly type = GET_FILTER_GRAPH;
  constructor(public filter: FilterQueryParams, public selectedKpi: string, public isCompareMode: boolean, public graphConfig: GraphConfig, public hasDifferentKpi: boolean) {}
}

export class GetCompareGraph implements Action {
  readonly type = GET_COMPARE_GRAPH;
  constructor(public filter: FilterQueryParams, public selectedKpi: string, public isCompareMode: boolean, public graphConfig: GraphConfig, public hasDifferentKpi: boolean) {}
}

export class SetGraphType implements Action {
  readonly type = SET_GRAPH_TYPE;
  constructor(public payload: GraphType) {}
}

export class ResetChart implements Action {
    readonly type = RESET_CHART;
    constructor() {}
}

export class AddSeries implements Action {
    readonly type = ADD_SERIES;
    constructor(public payload: Highcharts.SeriesOptionsType) { }
}

export class SetTooltipOptions implements Action {
    readonly type = SET_TOOLTIP_OPTIONS;
    constructor(public payload: Highcharts.TooltipOptions) { }
}

export class AddMultipleSeries implements Action {
  readonly type = ADD_SERIES_MUL;
  constructor(public series: Array<SeriesOptionsType>, public isCompareGraph: boolean) { }
}

export class DeleteSeries implements Action {
    readonly type = DEL_SERIES;
    constructor(public payload: SeriesOptionsType) { }
}

export class ClearSeries implements Action {
    readonly type = CLEAR_SERIES;
    constructor() { }
}

export class SetTitel implements Action {
    readonly type = SET_TITEL;
    constructor(public payload: Highcharts.TitleOptions){ }
}

export class SetSubTitel implements Action {
    readonly type = SET_SUBTITEL;
    constructor(public payload: Highcharts.SubtitleOptions) { }
}

export class SetYAxis implements Action {
    readonly type = SET_YAXIS;
    constructor(public payload: Highcharts.YAxisOptions) { }
}

export class SetXAxis implements Action {
    readonly type = SET_XAXIS;
    constructor(public payload: Highcharts.XAxisOptions) { }
}

export type Actions =
    AddSeries
    | SetTitel
    | SetSubTitel
    | SetYAxis
    | SetXAxis
    | DeleteSeries
    | ClearSeries
    | ResetChart;
