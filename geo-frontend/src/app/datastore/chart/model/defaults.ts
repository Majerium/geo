import * as Highcharts from 'highcharts';
import * as moment from "moment";


export const DefaultYAxis: Highcharts.YAxisOptions = {
    title: {
        text: 'Kurs'
    }
}

export const DefaultXAxis: Highcharts.XAxisOptions  = {
    type: 'datetime',
    crosshair: true
}

export const DefaultChartConfig: Highcharts.ChartOptions = {
    type: 'area'
}

export const DefaultChartTitel: Highcharts.TitleOptions = {
     text: 'Titel'
}

export const DefaultSubTitel: Highcharts.SubtitleOptions ={
    text: 'Subtitel'
}

export const DefaultLegend: Highcharts.LegendOptions = {
        layout: 'horizontal',
        itemMarginTop: 10,
        itemMarginBottom: 10
}

export const DefaultPlotOptions: Highcharts.PlotOptions = {
    series: {
        label: {
            connectorAllowed: false
        },
        pointStart: 2010
    }
}

export const DefaultPTooltipOptions: Highcharts.TooltipOptions = {
  shared: true,
  formatter: function (): any {
    return this.points.reduce(function (s, point) {
      const time = (point.point as any).isCompareMode ? (point.point as any).time : point.point.x
      const date: moment.Moment = moment(time).locale('de')
      const formattedDate: string = date.format("DD.MM.YYYY")
      const formattedDay: string = date.format('dddd');
      const actualValue: number = (point.point as any).actualValue;
      let tooltip: string = `${formattedDay} ${formattedDate}<br/>`;
      if(actualValue) tooltip += `Wert:${actualValue}<br/>`;
      tooltip += `<b>${point.series.name}</b>: ${point.y.toLocaleString('de-DE')}`;
      return s + `<br/><br/>${tooltip}`;
    }, '');
  }
}


export const DefaultTimeOptions: Highcharts.TimeOptions = {
  timezoneOffset: new Date().getTimezoneOffset()
}
