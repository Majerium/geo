import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import * as chartState from './chart';

export interface ChartState {
    chartData: chartState.State;
}

export const  chartReducer: ActionReducerMap<ChartState> = {
    chartData: chartState.reducer
}

export const getChartDataState = createFeatureSelector<chartState.State>('chartData');

export const getChartData = createSelector(getChartDataState, chartState.getChatData);
export const getChartTitel = createSelector(getChartDataState, chartState.getChatTitel);
export const getChartSubTitel = createSelector(getChartDataState, chartState.getChartSubTitel);
export const getSeries = createSelector(getChartDataState, chartState.getSeries);
export const getYAxis = createSelector(getChartDataState, chartState.getYAxis);
