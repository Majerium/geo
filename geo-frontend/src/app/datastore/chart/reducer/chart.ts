import {Action} from '@ngrx/store';

import * as chartActions from '../actions/chart'
import {
  DefaultChartConfig,
  DefaultChartTitel,
  DefaultLegend,
  DefaultPlotOptions,
  DefaultPTooltipOptions,
  DefaultSubTitel,
  DefaultTimeOptions,
  DefaultXAxis,
  DefaultYAxis
} from '../model/defaults';

import * as Highcharts from 'highcharts';


export interface State extends Highcharts.Options {
    chart?: Highcharts.ChartOptions;
    title?: Highcharts.TitleOptions;
    subtitle?: Highcharts.SubtitleOptions;
    yAxis?: Highcharts.YAxisOptions;
    xAxis?: Highcharts.XAxisOptions;
    legend?: Highcharts.LegendOptions;
    plotOptions?: Highcharts.PlotOptions;
    series?: Array<Highcharts.SeriesOptionsType>;
    time?: Highcharts.TimeOptions;
}

export const initialState: State = {
  chart: DefaultChartConfig,
  title: DefaultChartTitel,
  subtitle: DefaultSubTitel,
  yAxis: DefaultYAxis,
  xAxis: DefaultXAxis,
  legend: DefaultLegend,
  plotOptions: DefaultPlotOptions,
  tooltip: DefaultPTooltipOptions,
  time: DefaultTimeOptions,
  series: new Array<Highcharts.SeriesOptionsType>()
}


export function reducer(state: State = initialState, action: Action): State {
    switch(action.type) {
      case chartActions.SET_TOOLTIP_OPTIONS: {
        return { ...state,
          tooltip:  (action as chartActions.SetTooltipOptions).payload
        }
      }
      case chartActions.SET_GRAPH_TYPE: {
        return { ...state,
          chart: { type:  (action as chartActions.SetGraphType).payload }
        }
      }
      case chartActions.RESET_CHART: {
          return { ...state,
              ...initialState
          }
      }
      case chartActions.ADD_SERIES_MUL: {
        const isCompareSeries: boolean = (action as chartActions.AddMultipleSeries).isCompareGraph
        return { ...state,
          series: isCompareSeries ? [...state.series, ...(action as chartActions.AddMultipleSeries).series] : [...(action as chartActions.AddMultipleSeries).series, ...state.series]
        }
      }
      case chartActions.CLEAR_SERIES: {
          return { ...state,
              series: new Array<Highcharts.SeriesOptionsType>()
          }
      }
      case chartActions.DEL_SERIES: {
          return { ...state,
              series: state.series.filter((series: Highcharts.SeriesOptionsType) => series !== (action as chartActions.DeleteSeries).payload)
          }
      }
      case chartActions.SET_SUBTITEL: {
          return { ...state,
              subtitle: (action as chartActions.SetSubTitel).payload
          }
      }
      case chartActions.SET_TITEL:
          return { ...state,
              title: (action as chartActions.SetTitel).payload
          }

      case chartActions.SET_YAXIS:
          return { ...state,
              yAxis: (action as chartActions.SetYAxis).payload
          }

      case chartActions.SET_XAXIS:
          return { ...state,
              xAxis: (action as chartActions.SetXAxis).payload
          }

      default:
          return state;
  }
}

export const getChatData = (state: State) => state;
export const getChatTitel = (state: State) => state.title;
export const getChartSubTitel = (state: State) => state.subtitle;
export const getSeries = (state: State) => state.series;
export const getYAxis = (state: State) => state.yAxis;
