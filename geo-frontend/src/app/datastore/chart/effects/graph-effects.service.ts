import {Injectable} from "@angular/core";
import {act, Actions, Effect, ofType} from '@ngrx/effects';
import {Observable} from "rxjs";
import {Action} from "@ngrx/store";
import * as ChartActions from '../actions/chart'
import {switchMap} from "rxjs/operators";
import {ChartService} from "../../../services/chart/chart.service";
import {
  DynamicFilterRequest,
  GeoDataApiControllerService,
  GraphDataRequest, GraphResponse,
  MultiGraphResponse
} from "../../../services/api/generated/data";
import {FilterQueryParams} from "../../router/model/filterQueryParams";
import {DynamicFilter} from "../../router/model/dynamic-filter";
import * as moment from "moment";
import {SetGraphType} from "../actions/chart";

@Injectable()
export class GraphEffectsService {

  public static START_DATE: moment.Moment = moment(	1577919600 * 1000); // First corona its the first that you can measure with
  public static API_LOCAL_DATE_FORMAT: string = 'YYYY-MM-DD'; // First corona its the first that you can measure with

  constructor(
    private readonly actions$: Actions,
    private readonly api: GeoDataApiControllerService,
    private readonly chartService: ChartService
  ) { }

  @Effect()
  getFilterGraph$: Observable<Action> = this.actions$.pipe(
    ofType(ChartActions.GET_FILTER_GRAPH),
    switchMap((action: ChartActions.GetFilterGraph) => {
      return this.api.getKPIsByFilterGraphRequest(this.createChartRequest(false, action.filter)).pipe(
        switchMap((data: MultiGraphResponse) => [
          // new SetGraphType(action.graphConfig.graphType),
          this.chartService.processKpis(
            data.graphResponseList,
            action.selectedKpi,
            false,
            action.isCompareMode,
            action.graphConfig.isInPercentage,
            action.hasDifferentKpi,
            action.graphConfig.cumulative,
            this.getFromFirstPossibleDate(action.filter.from, data.graphResponseList, action.isCompareMode).clone(),
            this.getFromFromLastPointInGraph(action.filter.to, data.graphResponseList).clone()
          )
        ])
      )
    })
  );

  @Effect()
  getCompareGraph$: Observable<Action> = this.actions$.pipe(
    ofType(ChartActions.GET_COMPARE_GRAPH),
    switchMap((action: ChartActions.GetCompareGraph) => {
      return this.api.getKPIsByFilterGraphRequest(this.createChartRequest(true, action.filter)).pipe(
        switchMap((data: MultiGraphResponse) => [
         //  new SetGraphType(action.graphConfig.graphType),
          this.chartService.processKpis(
            data.graphResponseList,
            action.selectedKpi,
            true,
            action.isCompareMode,
            action.graphConfig.isInPercentage,
            action.hasDifferentKpi,
            action.graphConfig.cumulative,
            this.getFromFirstPossibleDate(action.filter.fromCompare, data.graphResponseList, action.isCompareMode).clone(),
            this.getFromFromLastPointInGraph(action.filter.toCompare, data.graphResponseList).clone()
          )
        ])
      )
    })
  );

  private getFromFirstPossibleDate(date: moment.Moment, kpis: Array<GraphResponse>, isCompare: boolean): moment.Moment {
    if (!isCompare) {
      return this.getFromFromFirstPointInGraph(date, kpis);
    } else return date ? date : GraphEffectsService.START_DATE;
  }

  private getFromFromFirstPointInGraph(date: moment.Moment,  kpis: Array<GraphResponse>): moment.Moment {
    return (date ? date : moment(kpis![0].graph![0].xvalue));
  }

  private getFromFromLastPointInGraph(date: moment.Moment,  kpis: Array<GraphResponse>): moment.Moment {
    return (date ? date : moment(kpis![0].graph![kpis![0].graph.length - 1].xvalue)).clone();
  }

  private createChartRequest(isCompare: boolean, filter: FilterQueryParams): GraphDataRequest {
    const request: GraphDataRequest = {
      dataSet: isCompare ? filter.compareDataset : filter.filterDataset,
      filterDatasetRequest: { }
    }
    isCompare ? this.setCompareRequest(request, filter) : this.setFilterRequest(request, filter);
    return request;
  }

  private setFilterRequest(shapeDataRequest: GraphDataRequest, filter: FilterQueryParams): void {
    shapeDataRequest.filterDatasetRequest = { from: filter.from ? filter.from.format(GraphEffectsService.API_LOCAL_DATE_FORMAT) : null, to: filter.to ? filter.to.format(GraphEffectsService.API_LOCAL_DATE_FORMAT) : null, dynamicFilterRequests: []};
    this.setDynamicFilter(shapeDataRequest.filterDatasetRequest.dynamicFilterRequests, filter.dynamicFilter);
  }

  private setCompareRequest(shapeDataRequest: GraphDataRequest, filter: FilterQueryParams): void {
    shapeDataRequest.filterDatasetRequest = { from: filter.fromCompare ? filter.fromCompare.format(GraphEffectsService.API_LOCAL_DATE_FORMAT) : null, to: filter.toCompare ? filter.toCompare.format(GraphEffectsService.API_LOCAL_DATE_FORMAT) : null, dynamicFilterRequests: []};
    this.setDynamicFilter(shapeDataRequest.filterDatasetRequest.dynamicFilterRequests, filter.dynamicCompare);
  }

  private setDynamicFilter(dynamicFilter: Array<DynamicFilterRequest>, filter: { [key: string]: DynamicFilter }) {
    Object.entries(filter).filter(([key, value]) => value.useForGraph).forEach(entry => dynamicFilter.push({type: entry[0], value: entry[1].value}))
  }
}
