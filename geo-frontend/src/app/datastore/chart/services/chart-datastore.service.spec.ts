import { TestBed } from '@angular/core/testing';

import { ChartDatastoreService } from './chart-datastore.service';

describe('ChartDatastoreService', () => {
  let service: ChartDatastoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChartDatastoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
