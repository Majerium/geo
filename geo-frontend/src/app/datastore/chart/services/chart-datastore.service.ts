import {Injectable} from '@angular/core';
import {ChartState, getChartData, getChartSubTitel, getChartTitel, getSeries, getYAxis} from '../reducer';
import {Store} from '@ngrx/store';
import * as chartActions from "../actions/chart";
import {GetCompareGraph, GetFilterGraph, SetGraphType, SetXAxis} from "../actions/chart";
import {FilterQueryParams} from "../../router/model/filterQueryParams";
import {GraphType} from "../../config/model/appConfig";
import {Observable} from "rxjs";
import * as Highcharts from 'highcharts';
import {AxisTypeValue} from 'highcharts';
import {GraphConfig} from "../../router/services/router-datastore.service";

@Injectable({
  providedIn: 'root'
})
export class ChartDatastoreService {

  constructor(private _store: Store<ChartState>) { }

  public fetchFilterChartData(filter: FilterQueryParams, selectedKpi: string, isCompareMode: boolean, graphConfig: GraphConfig, hasDifferentKpi: boolean): void  {
    this._store.dispatch(new GetFilterGraph(filter, selectedKpi, isCompareMode, graphConfig, hasDifferentKpi));
  }

  public fetchCompareChartData(filter: FilterQueryParams, selectedKpi: string, isCompareMode: boolean, graphConfig: GraphConfig, hasDifferentKpi: boolean): void  {
    this._store.dispatch(new GetCompareGraph(filter, selectedKpi, isCompareMode, graphConfig, hasDifferentKpi));
  }

  public getChartData(): Observable<Highcharts.Options> {
    return this._store.select(getChartData);
  }

  public getSeries(): Observable<Array<Highcharts.SeriesOptionsType>> {
    return this._store.select(getSeries);
  }

  public addSeries(series: Highcharts.SeriesOptionsType): void {
    this._store.dispatch(new chartActions.AddSeries(series));
  }

  public deleteSeries(seriesToDelete: Highcharts.SeriesOptionsType): void {
    this._store.dispatch(new chartActions.DeleteSeries(seriesToDelete))
  }

  public clearSeries(): void {
    this._store.dispatch(new chartActions.ClearSeries())
  }

  public getYAxis(): Observable<Highcharts.YAxisOptions> {
    return this._store.select(getYAxis);
  }

  public setXAxis(axisType: AxisTypeValue): void {
    return this._store.dispatch(new SetXAxis({ type: axisType}));
  }

  public setGraphType(graphType: GraphType): void {
    return this._store.dispatch(new SetGraphType(graphType));
  }

  public setYAxis(yAxis: Highcharts.XAxisOptions): void {
    this._store.dispatch(new chartActions.SetYAxis(yAxis));
  }

  public setTooltip(tooltipOptions: Highcharts.TooltipOptions): void {
    this._store.dispatch(new chartActions.SetTooltipOptions(tooltipOptions));
  }

  public getChartTitel(): Observable<Highcharts.TitleOptions> {
    return this._store.select(getChartTitel);
  }

  public setChartTitel(titel: Highcharts.TitleOptions): void {
    this._store.dispatch(new chartActions.SetTitel(titel));
  }

  public getChartSubTitel(): Observable<Highcharts.SubtitleOptions> {
    return this._store.select(getChartSubTitel);
  }

  public setChartSubTitel(subTitel: Highcharts.SubtitleOptions): void {
    this._store.dispatch(new chartActions.SetSubTitel(subTitel));
  }

}
