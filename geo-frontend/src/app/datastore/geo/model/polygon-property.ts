import {GeoJsonProperties} from "geojson";

export interface PolygonProperty extends GeoJsonProperties { }
