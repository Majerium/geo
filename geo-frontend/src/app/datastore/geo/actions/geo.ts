import {Action} from '@ngrx/store'
import {Location} from "../../location/model/location";
import {Feature, FeatureCollection, GeoJsonProperties, MultiPolygon, Polygon} from "geojson";
import {LatLng, Polygon as LeafletPolygon} from "leaflet";
import {PolygonProperty} from "../model/polygon-property";
import {ForBbox} from "../../location/model/for-bbox";
import {ForCenter} from "../../location/model/for-center";

const TYPE = '[GEO]';

export const GET_RASTER  = `${TYPE} GET_RASTER`;

export const GET_COUNTRY_BY_CENTER  = `${TYPE} GET_COUNTRY_BY_CENTER`;

export const NO_ACTION  = `${TYPE} NO_ACTION`;

export class GetCountryByCenter implements Action {
  readonly type = GET_COUNTRY_BY_CENTER;
  constructor(public center: LatLng) { }
}

export class NoAction implements Action {
  readonly type = NO_ACTION;
}

export class GetRaster implements Action {
  readonly type = GET_RASTER;
  constructor(public bbox: ForBbox) { }
}

