import {Injectable} from "@angular/core";
import {Actions, Effect, ofType} from '@ngrx/effects';
import {
  FeatureCollection as ApiFeatureCollection,
  GeoApiControllerService
} from "../../../services/api/generated/shape";
import {Observable} from "rxjs";
import {Action} from "@ngrx/store";
import * as GeoActions from '../../geo/actions/geo'
import {switchMap, tap} from "rxjs/operators";
import {GeoDatastoreService} from "../../geo/services/geo-datastore.service";
import {FeatureCollection, Polygon} from "geojson";
import {PolygonProperty} from "../model/polygon-property";
import {MapService} from "../../../services/map/map.service";

@Injectable()
export class GeometryEffectsService {

  constructor(
    private readonly actions$: Actions,
    private readonly geoDataStore: GeoDatastoreService,
    private readonly api: GeoApiControllerService,
    private readonly mapService: MapService
  ) { }

  @Effect()
  loadRaster$: Observable<Action> = this.actions$.pipe(
    ofType(GeoActions.GET_RASTER),
    switchMap((action: GeoActions.GetRaster) => {
      return this.api.getCountryRaster(action.bbox.zoom, action.bbox.northEast.lat, action.bbox.northEast.lng, action.bbox.southWest.lat, action.bbox.southWest.lng).pipe(
        tap((data: ApiFeatureCollection) => this.mapService.drawFeatureCollection(action.bbox.zoom, data as FeatureCollection<Polygon, PolygonProperty>)),
        switchMap((_) => [new GeoActions.NoAction()])
      )
    })
  );

  @Effect()
  loadCountriesByCenter$: Observable<Action> = this.actions$.pipe(
    ofType(GeoActions.GET_COUNTRY_BY_CENTER),
    switchMap((action: GeoActions.GetCountryByCenter) => {
      return this.api.getCountriesNearCenter(action.center.lat, action.center.lng).pipe(
        tap((data: ApiFeatureCollection) => this.mapService.drawCountries(data as FeatureCollection<Polygon, PolygonProperty>)),
        switchMap((_) => [new GeoActions.NoAction()])
      )
    })
  );

}
