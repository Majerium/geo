import {Injectable} from '@angular/core';
import {Store} from "@ngrx/store";
import {GetCountryByCenter, GetRaster} from "../actions/geo"
import {ForBbox} from "../../location/model/for-bbox";
import {LatLng} from "leaflet";

@Injectable({
  providedIn: 'root'
})
export class GeoDatastoreService {

  constructor(private _store: Store<any>) { }

  public fetchCountryNearCenter(center: LatLng): void {
    this._store.dispatch(new GetCountryByCenter(center));
  }

  public fetchRasterByBbox(bbox: ForBbox): void {
    this._store.dispatch(new GetRaster(bbox))
  }

}
