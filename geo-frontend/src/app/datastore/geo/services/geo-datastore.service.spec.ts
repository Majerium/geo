import { TestBed } from '@angular/core/testing';

import { GeoDatastoreService } from './geo-datastore.service';

describe('GeoDatastoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GeoDatastoreService = TestBed.get(GeoDatastoreService);
    expect(service).toBeTruthy();
  });
});
