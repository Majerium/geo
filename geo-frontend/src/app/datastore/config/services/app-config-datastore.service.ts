import {Injectable} from '@angular/core';
import {Store} from "@ngrx/store";
import {
  ConfigState,
  getCompareDatasetConfig,
  getConfig,
  getDatasetZoomLevel, getFilterAppConfig,
  getFilterDatasetConfig,
  getRasterZoomLevel, showInfo, spinnerState
} from "../reducer";
import {Observable} from "rxjs";
import {AppConfig} from "../model/appConfig";
import {
  GetDatasetCompareConfig,
  GetDatasetCompareConfigSetKpi,
  GetDatasetFilterConfig,
  GetDatasetFilterConfigSetKpi,
  GetDatasets,
  GetZoomLevel, HideInfo, HideSpinner,
  SetDatasetZoomLevel, ShowInfo, ShowSpinner
} from "../actions/appConfig";
import {FilterAppConfig, FilterDatasetConfig} from "../reducer/appConfig";

@Injectable({
  providedIn: 'root'
})
export class AppConfigDatastoreService {

  constructor(private _store: Store<ConfigState>) { }

  public fetchZoomLevel(): void {
    this._store.dispatch(new GetZoomLevel());
  }

  public fetchDatasets(): void {
    this._store.dispatch(new GetDatasets());
  }

  public getConfig(): Observable<AppConfig> {
    return this._store.select(getConfig);
  }

  public getRasterZoomLevel(): Observable<Array<number>> {
    return this._store.select(getRasterZoomLevel);
  }

  public getDatasetZoomLevel(): Observable<Array<number>> {
    return this._store.select(getDatasetZoomLevel);
  }

  public fetchFilterConfigWithKpi(dataset: string, kpi: string): void {
    this._store.dispatch(new GetDatasetFilterConfigSetKpi(dataset, kpi));
  }

  public fetchFilterConfig(dataset: string): void {
    this._store.dispatch(new GetDatasetFilterConfig(dataset));
  }

  public fetchCompareConfig(dataset: string): void {
    this._store.dispatch(new GetDatasetCompareConfig(dataset));
  }

  public fetchCompareConfigWithKpi(dataset: string, kpi: string): void {
    this._store.dispatch(new GetDatasetCompareConfigSetKpi(dataset, kpi));
  }

  public setDatasetZoomLevel(datasetZoomLevel: Array<number>): void {
    this._store.dispatch(new SetDatasetZoomLevel(datasetZoomLevel));
  }

  public getFilterDatasetConfig(): Observable<FilterDatasetConfig> {
    return this._store.select(getFilterDatasetConfig)
  }

  public getCompareDatasetConfig(): Observable<FilterDatasetConfig> {
    return this._store.select(getCompareDatasetConfig)
  }

  public getAppFilterConfig(): Observable<FilterAppConfig> {
    return this._store.select(getFilterAppConfig)
  }

  public getSpinnerState(): Observable<boolean> {
    return this._store.select(spinnerState)
  }

  public getInfoState(): Observable<boolean> {
    return this._store.select(showInfo)
  }

  public showSpinner(): void {
    this._store.dispatch(new ShowSpinner())
  }

  public hideSpinner(): void {
    this._store.dispatch(new HideSpinner())
  }

  public showInfo(): void {
    this._store.dispatch(new ShowInfo())
  }

  public hideInfo(): void {
    this._store.dispatch(new HideInfo())
  }

}
