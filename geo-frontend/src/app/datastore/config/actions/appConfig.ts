import {Action} from "@ngrx/store";
import {ShapeDataConfigResponse} from "../../../services/api/generated/data";

const TYPE = '[APP-CONFIG]';

export const SET_ZOOM_LEVEL  = `${TYPE} Set zoom level`;
export const GET_ZOOM_LEVEL = `${TYPE} Get zoom level`;

export const SET_DATASET_ZOOM_LEVEL = `${TYPE} Set dataset zoom level`;

export const GET_DATASETS = `${TYPE} Get datasets`;
export const SET_DATASETS = `${TYPE} Set datasets`;

export const GET_DATASET_FILTER_CONFIG_SET_KPI = `${TYPE} Get dataset filter config set kpi`;
export const GET_DATASET_FILTER_CONFIG = `${TYPE} Get dataset filter config`;
export const SET_DATASET_FILTER_CONFIG =  `${TYPE} Set dataset filter config`;

export const GET_DATASET_COMPARE_CONFIG_SET_KPI = `${TYPE} Get dataset compare config set kpi`;
export const GET_DATASET_COMPARE_CONFIG = `${TYPE} Get dataset compare config`;
export const SET_DATASET_COMPARE_CONFIG =  `${TYPE} Set dataset compare config`;

export const SHOW_SPINNER = `${TYPE} Show spinner`;
export const HIDE_SPINNER = `${TYPE} Hide spinner`;

export const SHOW_INFO = `${TYPE} Show info`;
export const HIDE_INFO = `${TYPE} Hide info`;

export class HideInfo implements Action {
  readonly type = HIDE_INFO;
}

export class ShowInfo implements Action {
  readonly type = SHOW_INFO;
}

export class ShowSpinner implements Action {
  readonly type = SHOW_SPINNER;
}

export class HideSpinner implements Action {
  readonly type = HIDE_SPINNER;
}

export class SetZoomLevel implements Action {
  readonly type = SET_ZOOM_LEVEL;
  constructor(public payload: Array<number>) { }
}

export class SetDatasetZoomLevel implements Action {
  readonly type = SET_DATASET_ZOOM_LEVEL;
  constructor(public payload: Array<number>) { }
}

export class SetDatasets implements Action {
  readonly type = SET_DATASETS;
  constructor(public payload: Array<string>) { }
}

export class GetZoomLevel implements Action {
  readonly type = GET_ZOOM_LEVEL;
}

export class GetDatasets implements Action {
  readonly type = GET_DATASETS;
}

export class GetDatasetFilterConfigSetKpi implements Action {
  readonly type = GET_DATASET_FILTER_CONFIG_SET_KPI;
  constructor(public dataset: string, public kpi: string) { }
}

export class GetDatasetCompareConfigSetKpi implements Action {
  readonly type = GET_DATASET_COMPARE_CONFIG_SET_KPI;
  constructor(public dataset: string, public kpi: string) { }
}

export class GetDatasetFilterConfig implements Action {
  readonly type = GET_DATASET_FILTER_CONFIG;
  constructor(public dataset: string) { }
}

export class GetDatasetCompareConfig implements Action {
  readonly type = GET_DATASET_COMPARE_CONFIG;
  constructor(public payload: string) { }
}

export class SetDatasetFilterConfig implements Action {
  readonly type = SET_DATASET_FILTER_CONFIG;
  constructor(public payload: ShapeDataConfigResponse) { }
}

export class SetDatasetCompareConfig implements Action {
  readonly type = SET_DATASET_COMPARE_CONFIG;
  constructor(public payload: ShapeDataConfigResponse) { }
}
