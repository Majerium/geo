import {ShapeDataConfigResponse} from "../../../services/api/generated/data";
import {ColorScheme} from "../../../services/color-palette/color-palette.service";

export type GraphType = 'line' | 'column' | 'area';

export interface AppConfig {
  rasterZoomLevel?: Array<number>,
  datasets?: Array<string>,
  datasetRasterZoomLevel?: Array<number>,
  filterConfig?: ShapeDataConfigResponse,
  compareConfig?: ShapeDataConfigResponse,
  showSpinner: boolean,
  showInfo: boolean
}
