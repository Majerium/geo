import {Injectable} from "@angular/core";
import {Actions, Effect, ofType} from "@ngrx/effects";
import {ConfigApiControllerService, ConfigResponse} from "../../../services/api/generated/shape";
import {Observable} from "rxjs";
import {Action} from "@ngrx/store";
import * as ConfigActions from "../actions/appConfig";
import * as RouterAction from "../../router/actions/router"
import {mergeMap, switchMap} from "rxjs/operators";
import {
  DatasetResponse,
  GeoDataConfigControllerService,
  ShapeDataConfigResponse
} from "../../../services/api/generated/data";
import {QUERY_NAMES} from "../../router/reducer";

@Injectable()
export class AppConfigEffectsService {

  constructor(
    private readonly actions$: Actions,
    private readonly shapeConfigApi: ConfigApiControllerService,
    private readonly dataConfigApi: GeoDataConfigControllerService
  ) { }

  @Effect()
  loadZoomLevel$: Observable<Action> = this.actions$.pipe(
    ofType(ConfigActions.GET_ZOOM_LEVEL),
    switchMap((action: ConfigActions.GetZoomLevel) => {
      return this.shapeConfigApi.getConfiguration().pipe(
        mergeMap((data: ConfigResponse) => [
          new ConfigActions.SetZoomLevel(data.rasterZoomLevel as any) // Default Raster zoom level which shapes can be queried for
        ])
      );
    })
  );

  @Effect()
  loadDatasets$: Observable<Action> = this.actions$.pipe(
    ofType(ConfigActions.GET_DATASETS),
    switchMap((action: ConfigActions.GetDatasets) => {
      return this.dataConfigApi.getDatasets().pipe(
        mergeMap((data: DatasetResponse) => [
          new ConfigActions.SetDatasets(data.dataSets)
        ])
      );
    })
  );
  @Effect()
  loadDatasetFilterConfig: Observable<Action> = this.actions$.pipe(
    ofType(ConfigActions.GET_DATASET_FILTER_CONFIG),
    switchMap((action: ConfigActions.GetDatasetFilterConfig) => {
      return this.dataConfigApi.getShapeDataFilterConfig(action.dataset).pipe(
        mergeMap((data: ShapeDataConfigResponse) => [
          new ConfigActions.SetDatasetFilterConfig(data),
          // new RouterAction.SetParam(QUERY_NAMES.CONFIG.FILTER_KPI, data.kpiConfigs[0].kpi),
          new ConfigActions.SetDatasetZoomLevel(data.rasterZoomLevel)
        ])
      );
    })
  );
  @Effect()
  loadDatasetFilterConfigSetKpi: Observable<Action> = this.actions$.pipe(
    ofType(ConfigActions.GET_DATASET_FILTER_CONFIG_SET_KPI),
    switchMap((action: ConfigActions.GetDatasetFilterConfigSetKpi) => {
      return this.dataConfigApi.getShapeDataFilterConfig(action.dataset).pipe(
        mergeMap((data: ShapeDataConfigResponse) => [
          new ConfigActions.SetDatasetFilterConfig(data),
          new RouterAction.SetParam(QUERY_NAMES.CONFIG.FILTER_KPI, this.getKPI(action.kpi, data.kpiConfigs.map(x => x.kpi))),
          new ConfigActions.SetDatasetZoomLevel(data.rasterZoomLevel)
        ])
      );
    })
  );

  private getKPI(kpiToSet: string, possibleKPIs: Array<string>): string {
    return possibleKPIs.includes(kpiToSet) ? kpiToSet : possibleKPIs[0];
  }

  @Effect()
  loadDatasetCompareConfig: Observable<Action> = this.actions$.pipe(
    ofType(ConfigActions.GET_DATASET_COMPARE_CONFIG),
    switchMap((action: ConfigActions.GetDatasetCompareConfig) => {
      return this.dataConfigApi.getShapeDataFilterConfig(action.payload).pipe(
        mergeMap((data: ShapeDataConfigResponse) => [
          new ConfigActions.SetDatasetCompareConfig(data),
        ])
      );
    })
  );
  @Effect()
  loadDatasetCompareConfigSetKpi: Observable<Action> = this.actions$.pipe(
    ofType(ConfigActions.GET_DATASET_COMPARE_CONFIG_SET_KPI),
    switchMap((action: ConfigActions.GetDatasetCompareConfigSetKpi) => {
      return this.dataConfigApi.getShapeDataFilterConfig(action.dataset).pipe(
        mergeMap((data: ShapeDataConfigResponse) => [
          new ConfigActions.SetDatasetCompareConfig(data),
          new RouterAction.SetParam(QUERY_NAMES.CONFIG.COMPARE_KPI, this.getKPI(action.kpi, data.kpiConfigs.map(x => x.kpi))),
        ])
      );
    })
  );
}
