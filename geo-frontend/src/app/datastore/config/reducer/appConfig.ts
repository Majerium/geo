import * as ConfigActions from '../actions/appConfig'
import {Action} from "@ngrx/store";
import {AppConfig} from "../model/appConfig";
import {ShapeDataConfigResponse} from "../../../services/api/generated/data";

export interface State extends AppConfig { }

export const initialState: State = {
  rasterZoomLevel: [],
  datasets: [],
  datasetRasterZoomLevel: [],
  showSpinner: false,
  showInfo: true
};

export function reducer(state: State = initialState, action: Action): State {

  switch(action.type) {
    case ConfigActions.SHOW_INFO: {
      return { ...state,
        showInfo: true
      }
    }
    case ConfigActions.HIDE_INFO: {
      return { ...state,
        showInfo: false
      }
    }
    case ConfigActions.SET_ZOOM_LEVEL: {
      return { ...state,
        rasterZoomLevel: (action as ConfigActions.SetZoomLevel).payload
      }
    }
    case ConfigActions.SET_DATASET_ZOOM_LEVEL: {
      return { ...state,
        datasetRasterZoomLevel: (action as ConfigActions.SetDatasetZoomLevel).payload
      }
    }
    case ConfigActions.SET_DATASETS: {
      return { ...state,
        datasets: (action as ConfigActions.SetDatasets).payload
      }
    }
    case ConfigActions.SET_DATASET_FILTER_CONFIG: {
      const filterConfig = setAllElementForEachFilter((action as ConfigActions.SetDatasetFilterConfig).payload);
      return { ...state,
        filterConfig: filterConfig
      }
    }
    case ConfigActions.SET_DATASET_COMPARE_CONFIG: {
      const filterConfig = setAllElementForEachFilter((action as ConfigActions.SetDatasetFilterConfig).payload);
      return { ...state,
        compareConfig: filterConfig
      }
    }
    case ConfigActions.SHOW_SPINNER: {
      return { ...state,
        showSpinner: true
      };
    }
    case ConfigActions.HIDE_SPINNER: {
      return { ...state,
        showSpinner: false
      };
    }
    default:
      return state;
  }
}

const setAllElementForEachFilter = (config: ShapeDataConfigResponse): ShapeDataConfigResponse => {
  const newFilter: ShapeDataConfigResponse = { dropdownFilter: [], kpiConfigs: config.kpiConfigs, autocompleteFilter: [] };
  for (let i = 0; i < config.dropdownFilter.length; i++) {
    newFilter.dropdownFilter[i] = { type: config.dropdownFilter[i].type, options: ['ALL'], visibleOnGraph: config.dropdownFilter[i].visibleOnGraph, visibleOnMap: config.dropdownFilter[i].visibleOnMap };
    for(let j = 0; j < config.dropdownFilter[i].options.length; j++) {
      newFilter.dropdownFilter[i].options.push(config.dropdownFilter[i].options[j]);
    }
  }
  for (let i = 0; i < config.autocompleteFilter.length; i++) {
    newFilter.autocompleteFilter[i] = { type: config.autocompleteFilter[i].type, options: [], visibleOnGraph: config.autocompleteFilter[i].visibleOnGraph, visibleOnMap: config.autocompleteFilter[i].visibleOnMap };
    for(let j = 0; j < config.autocompleteFilter[i].options.length; j++) {
      newFilter.autocompleteFilter[i].options.push(config.autocompleteFilter[i].options[j]);
    }
  }
  return newFilter;
}

export const getConfig = (state: State) => state;
export const getRasterZoomConfig = (state: State) => state.rasterZoomLevel;
export const getDatasets = (state: State) => state.datasets;


export type FilterDatasetConfig = { datasets: Array<string>, config: ShapeDataConfigResponse };
export type FilterAppConfig = { datasets: Array<string>, filterConfig: ShapeDataConfigResponse, compareConfig: ShapeDataConfigResponse }


export const filterAppConfig = (state: State): FilterAppConfig => {
  return {
    datasets: state.datasets,
    filterConfig: state.filterConfig,
    compareConfig: state.compareConfig
  }
}

export const filterDatasetConfig = (state: State): FilterDatasetConfig => {
  return {
    datasets: state.datasets,
    config: state.filterConfig
  }
}
export const compareDatasetConfig = (state: State): FilterDatasetConfig => {
  return {
    datasets: state.datasets,
    config: state.compareConfig
  }
}

export const getRasterZoomLevel = (state: State) => state.rasterZoomLevel;
export const getDatasetZoomLevel = (state: State) => state.datasetRasterZoomLevel;
export const getSpinner = (state: State) => state.showSpinner;
export const getInfo = (state: State) => state.showInfo;
