import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import {
  getConfig as getC,
  reducer as ConfigReducer,
  State,
  getRasterZoomLevel as forRaster,
  getDatasetZoomLevel as forDataset,
  compareDatasetConfig,
  filterDatasetConfig,
  filterAppConfig, getSpinner, getInfo,
} from './appConfig'

export interface ConfigState {
  appConfig: State;
}

export const configReducer: ActionReducerMap<ConfigState> = {
  appConfig: ConfigReducer
}

export const getConfigState = createFeatureSelector<State>('appConfig');
export const getConfig = createSelector(getConfigState, getC);
export const getRasterZoomLevel = createSelector(getConfigState, forRaster);
export const getDatasetZoomLevel = createSelector(getConfigState, forDataset);
export const getFilterDatasetConfig = createSelector(getConfigState, filterDatasetConfig);
export const getCompareDatasetConfig = createSelector(getConfigState, compareDatasetConfig);
export const getFilterAppConfig = createSelector(getConfigState, filterAppConfig);
export const spinnerState = createSelector(getConfigState, getSpinner);
export const showInfo = createSelector(getConfigState, getInfo)
