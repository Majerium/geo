import {Injectable} from '@angular/core';
import {Store} from "@ngrx/store";
import {getFullLocation, getZoom, LocationState} from "../../location/reducer";
import {SetBounds, SetInitial, SetZoom} from '../actions/location'
import {Location} from '../model/location'
import {Observable} from "rxjs";
import {State} from "../reducer/location";
import {ForBbox} from "../model/for-bbox";
import {filter, map} from "rxjs/operators";
import {control} from "leaflet";
import zoom = control.zoom;

@Injectable({
  providedIn: 'root'
})
export class LocationDatastoreService {

  constructor(private _store: Store<LocationState>) { }

  public getLocationForBbox(): Observable<ForBbox> {
    return this.getLocation().pipe(
      filter( location => location !== undefined),
      map(this.toForBBox)
    )
  }

  private toForBBox(location: Location): ForBbox {
    return { southWest: location.southWest, northEast: location.northEast, zoom: location.zoomLevel }
  }

  public getLocation(): Observable<State> {
    return this._store.select(getFullLocation)
  }

  public getZoom(): Observable<number> {
    return this._store.select(getZoom)
  }

  public setLocation(location: Location): void {
    this._store.dispatch(new SetInitial(location));
  }

  public fetchGeomityByBounds(location: Location): void {
    this._store.dispatch(new SetBounds(location));
  }

  public fetchGeomitryByZoom(zoom: number): void {
    this._store.dispatch(new SetZoom(zoom));
  }
}
