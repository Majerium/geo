import { TestBed } from '@angular/core/testing';

import { LocationDatastoreService } from './location-datastore.service';

describe('LocationDatastoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LocationDatastoreService = TestBed.get(LocationDatastoreService);
    expect(service).toBeTruthy();
  });
});
