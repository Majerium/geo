import { Action } from '@ngrx/store'
import {LatLngBounds} from "leaflet";
import {Location} from '../model/location'

export const TYPE = '[LOCATION]';

export const SET_BOUNDS  =   `${TYPE} SET_BOUNDS`;
export const SET_ZOOM  =   `${TYPE} SET_ZOOM`;
export const SET_INTIAL_STATE = `${TYPE} SET_INITIAL`;

export class SetInitial implements Action {
  readonly type = SET_INTIAL_STATE;
  constructor(public payload: Location) {}
}

export class SetBounds implements Action {
  readonly type = SET_BOUNDS;
  constructor(public payload: Location) {}
}

export class SetZoom implements Action {
  readonly type = SET_ZOOM;
  constructor(public payload: number) {}
}
