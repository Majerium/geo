import {LatLng} from "leaflet";

export interface ForBbox {
  southWest: LatLng;
  northEast: LatLng;
  zoom: number;
}
