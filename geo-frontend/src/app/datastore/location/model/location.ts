import {LatLng} from "leaflet";

export interface Location {
  northWest?: LatLng;
  northEast?: LatLng;
  southEast?: LatLng;
  southWest?: LatLng;
  zoomLevel?: number;
}
