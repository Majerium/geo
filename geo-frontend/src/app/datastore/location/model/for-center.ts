import {LatLng} from "leaflet";

export interface ForCenter {
  southWest: LatLng;
  northEast: LatLng;
}
