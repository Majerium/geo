import * as GeoActions from '../actions/location'
import {Action} from "@ngrx/store";
import {Location} from "../model/location";

export interface State extends Location { }

export function reducer(state: State = {}, action: Action): State {

  switch(action.type) {
    case GeoActions.SET_INTIAL_STATE: {
      return (action as GeoActions.SetInitial).payload;
    }

    case GeoActions.SET_BOUNDS: {
      return {
        zoomLevel: (action as GeoActions.SetBounds).payload.zoomLevel,
        northEast: (action as GeoActions.SetBounds).payload.northEast,
        northWest: (action as GeoActions.SetBounds).payload.northWest,
        southEast: (action as GeoActions.SetBounds).payload.southEast,
        southWest: (action as GeoActions.SetBounds).payload.southWest
      }
    }
    case GeoActions.SET_ZOOM: {
      return { ...state,
        zoomLevel: (action as GeoActions.SetZoom).payload
      }
    }
    default:
      return state;
  }
}

export const getLocation = (state: State) => state;
export const getZoomLevel = (state: State) => state.zoomLevel
