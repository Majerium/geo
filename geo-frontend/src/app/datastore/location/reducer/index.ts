import { ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import {State, reducer as LocationReducer, getLocation, getZoomLevel} from './location'

export interface LocationState {
  locationData: State;
}

export const locationReducer: ActionReducerMap<LocationState> = {
  locationData: LocationReducer
}

export const getLocationDataState = createFeatureSelector<State>('locationData');

export const getFullLocation = createSelector(getLocationDataState, getLocation);
export const getZoom = createSelector(getLocationDataState, getZoomLevel)

