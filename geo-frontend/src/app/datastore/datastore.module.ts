import {InjectionToken, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import * as fromLocationIndex from './location/reducer/index';
import * as fromConfigIndex from './config/reducer/index';
import * as fromChatIndex from './chart/reducer/index';
import * as fromRouterIndex from './router/reducer';
import {ActionReducerMap, MetaReducer, StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import {GeometryEffectsService} from "./geo/effects/geometry-effects.service";
import {AppConfigEffectsService} from "./config/effects/app-config-effects.service";
import {GraphEffectsService} from "./chart/effects/graph-effects.service";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {StoreRouterConnectingModule} from "@ngrx/router-store";
import {CustomSerializer} from "./router/model/router-serializer";
import {RouterEffectService} from "./router/effects/router-effect.service";


export type AppState =
  fromLocationIndex.LocationState |
  fromConfigIndex.ConfigState |
  fromChatIndex.ChartState |
  fromRouterIndex.State |

  undefined // Due to strict mode i think


export const reducers: ActionReducerMap<AppState> = {
  ...fromLocationIndex.locationReducer,
  ...fromConfigIndex.configReducer,
  ...fromChatIndex.chartReducer,
  ...fromRouterIndex.routeReducer
}

export const REDUCERS_TOKEN = new InjectionToken<ActionReducerMap<AppState>>('Registered Token');

Object.assign(REDUCERS_TOKEN, reducers)

const metaReducers: MetaReducer<AppState>[] = [];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forRoot(REDUCERS_TOKEN, { metaReducers: metaReducers }),
    //StoreModule.forRoot({}),
    //StoreModule.forFeature('datastore', FEATURE_REDUCER_TOKEN),
    EffectsModule.forRoot([GeometryEffectsService, AppConfigEffectsService, GraphEffectsService, RouterEffectService]),
    StoreDevtoolsModule.instrument({ maxAge: 25 }),
    StoreRouterConnectingModule.forRoot({ serializer: CustomSerializer })
  ],
  providers: [
    { provide: REDUCERS_TOKEN, useValue: reducers}
  ]
})
export class DatastoreModule { }
