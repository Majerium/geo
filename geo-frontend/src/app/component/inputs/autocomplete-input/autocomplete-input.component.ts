import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-autocomplete-input',
  templateUrl: './autocomplete-input.component.html',
  styleUrls: ['./autocomplete-input.component.scss']
})
export class AutocompleteInputComponent implements OnInit {
  private _label: string;
  private _values: Array<string>;
  private _filteredOptions: Observable<Array<string>>;
  private _hideRemove: boolean = true;

  control = new FormControl();

  @Output() selectedElement = new EventEmitter();
  @Output() removeElement = new EventEmitter();

  @Input() set showRemove(hideRemove: boolean) {
    this._hideRemove = hideRemove
  }

  get showRemove(): boolean {
    return this._hideRemove;
  }

  @Input() set value(value: string) {
    if (value) this.control.setValue(value);
  }

  @Input() set label(label: string) {
    this._label = `KEY_${label}`;
  }

  get label(): string {
    return this._label;
  }

  @Input() set selects(values: Array<string>) {
    this._values = values;
  }

  public getSelects(): Observable<Array<string>> {
    return this._filteredOptions;
  }

  ngOnInit(): void {
    this._filteredOptions = this.control.valueChanges
      .pipe(
        map((actualValue: string) => this._filter(actualValue))
      );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this._values.filter(option => option.toLowerCase().includes(filterValue));
  }

  public emitValue(value: string): void {
    this.selectedElement.emit(value);
  }

  valueSet(): boolean {
    return this.control !== undefined && this.control.value && this.control.value !== 'ALL';
  }

  public emitRemove(): void {
    if (this.valueSet()) {
      this.control.setValue('');
      this.removeElement.emit()
    }
  }
}
