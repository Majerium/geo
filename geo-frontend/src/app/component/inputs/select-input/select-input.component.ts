import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {MatSelect} from "@angular/material/select";

@Component({
  selector: 'app-select-input',
  templateUrl: './select-input.component.html',
  styleUrls: ['./select-input.component.scss']
})
export class SelectInputComponent<T> {

  private _label: string;
  private _values: Array<T>;
  private _hideRemove: boolean = true;

  @ViewChild('field', { static: true }) select: MatSelect;

  @Output() selectedElement = new EventEmitter();
  @Output() removeElement = new EventEmitter();

  @Input() set value(value: T) {
    this.select.value = value;
  };

  get value() {
    return this.select.value;
  }

  @Input() set showRemove(hideRemove: boolean) {
    this._hideRemove = hideRemove
  }

  get showRemove(): boolean {
    return this._hideRemove;
  }

  @Input() set label(label: string) {
    this._label = label;
  }

  get label(): string {
    return this._label;
  }

  @Input() set selects(values: Array<T>) {
    this._values = values;
  }

  get selects(): Array<T> {
    return this._values;
  }

  public emitValue(value: T): void {
    this.selectedElement.emit(value);
  }

  valueSet(): boolean {
    return this.select && this.select.value && this.select.value !== 'ALL';
  }

  public emitRemove(): void {
    if (this.valueSet()) {
      this.select.value = 'ALL';
      this.removeElement.emit()
    }
  }
}
