import { Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';

import * as moment from "moment";
import {FormControl} from "@angular/forms";
import {MatDatepicker} from "@angular/material/datepicker";

@Component({
  selector: 'app-date-input',
  templateUrl: './date-input.component.html',
  styleUrls: ['./date-input.component.scss']
})
export class DateInputComponent {

  private _start: Date;
  private _label: string;
  private _hideRemove: boolean = true;

  date: FormControl;

  @ViewChild('picker') picker: MatDatepicker<any>;

  @Input() set showRemove(hideRemove: boolean) {
    this._hideRemove = hideRemove
  }

  get showRemove(): boolean {
    return this._hideRemove;
  }

  @Input() set label(label: string) {
    this._label = label;
  }

  get label(): string {
    return this._label;
  }

  @Input() set start(start: any /* Its an  moment actually...*/) {
    const previousValue = moment(this._start, 'DD.MM.YYYY').format('DD.MM.YYYY');
    const momentDate: moment.Moment = moment(start, 'DD.MM.YYYY');
    this._start = start ? momentDate.toDate() : null;
    this.date = new FormControl(this._start);
    if (start && previousValue !== start && momentDate.isValid()) this.selectedDate.emit(momentDate);
  }

  get start(): any /* Its an  moment actually...*/ {
    return this._start;
  }

  @Output() selectedDate = new EventEmitter<moment.Moment>();
  @Output() removeElement = new EventEmitter();

  setDate($event) {
    this.selectedDate.emit(moment($event.value))
  }

  valueSet(): boolean {
    return this.date && this.date.value && this.date.value !== '';
  }

  public emitRemove(): void {
    if (this.valueSet()) {
      this.date.setValue('');
      this.removeElement.emit()
    }
  }

}
