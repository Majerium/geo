import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {combineLatest, Observable, Subject} from "rxjs";
import {FilterQueryParams} from "../../datastore/router/model/filterQueryParams";
import * as moment from "moment";
import {AppConfigDatastoreService} from "../../datastore/config/services/app-config-datastore.service";
import {delay, map, takeUntil} from "rxjs/operators";
import {FilterConfig} from "../../services/api/generated/data";
import {FilterLayoutConfig, RouterDatastoreService} from "../../datastore/router/services/router-datastore.service";
import {DynamicFilter} from "../../datastore/router/model/dynamic-filter";
import {QUERY_NAMES} from "../../datastore/router/reducer";
import {LayoutQueryParams} from "../../datastore/router/model/layoutQueryParams";
import {FilterAppConfig} from "../../datastore/config/reducer/appConfig";
import {Params} from "@angular/router";

type FilterLayoutConfigFilterConfig = {
  queryParams: FilterLayoutConfig,
  appFilterConfig: FilterAppConfig
}

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterComponent implements OnInit, OnDestroy {

  filterConfig$: Observable<FilterLayoutConfigFilterConfig>;

  private unsubscribe$: Subject<void> = new Subject();

  constructor(
    private readonly configDataStoreService: AppConfigDatastoreService,
    private readonly routerDatastoreService: RouterDatastoreService,
  ) {
    this.filterConfig$ = combineLatest([
      this.routerDatastoreService.getFilterLayoutConfigQueryParams(),
      this.configDataStoreService.getAppFilterConfig()
    ]).pipe(
        map(([queryParams, filterConfig]) => {
          return {
            queryParams: queryParams,
            appFilterConfig: filterConfig
          }
        }
      )
    )
  }

  ngOnInit(): void {
    this.subscribeToFilterDatastoreChanges();
    this.subscribeToCompareDatastoreChanges();
  }

  ngOnDestroy(): void {
    if(this.unsubscribe$ && !this.unsubscribe$.isStopped) {
      this.unsubscribe$.next();
      this.unsubscribe$.unsubscribe();
    }
  }

  private subscribeToCompareDatastoreChanges(): void {
    combineLatest([
      this.routerDatastoreService.getFilterDataset(),
      this.routerDatastoreService.getCompareDataset(),
      this.routerDatastoreService.isCompare(),
      this.routerDatastoreService.isGraph()
    ]).pipe(
      takeUntil(this.unsubscribe$),
      delay(200)
    ).subscribe(([filterDataset, compareDataset, isCompare, isGraph]) => this.processCompareDatasetConfig(filterDataset, compareDataset, isCompare, isGraph));
  }

  private subscribeToFilterDatastoreChanges(): void {
    this.routerDatastoreService.getFilterDataset().pipe(
      takeUntil(this.unsubscribe$),
      delay(200)
    ).subscribe((dataset) => this.processFilterDatasetConfig(dataset));
  }

  private processFilterDatasetConfig(dataset: string): void {
    if (dataset) {
      this.configDataStoreService.fetchFilterConfig(dataset);
    }
  }

  private processCompareDatasetConfig(filterDataset: string, compareDataset: string, isCompare: boolean, isGraph: boolean): void {
    if (isCompare) {
      this.configDataStoreService.fetchCompareConfig(isGraph ? compareDataset : filterDataset);
    }
  }

  setFrom(from?: moment.Moment): void {
    this.routerDatastoreService.setParam(QUERY_NAMES.FILTER.FILTER_FROM, from ? from.format("DD.MM.YYYY"): null)
  }

  setTo(to?: moment.Moment): void {
    this.routerDatastoreService.setParam(QUERY_NAMES.FILTER.FILTER_TO, to ? to.format("DD.MM.YYYY") : null);
  }

  setFromCompare(from?: moment.Moment): void {
    this.routerDatastoreService.setParam(QUERY_NAMES.FILTER.COMPARE_FROM, from ? from.format("DD.MM.YYYY") : null);
  }

  setToCompare(to?: moment.Moment): void {
    this.routerDatastoreService.setParam(QUERY_NAMES.FILTER.COMPARE_TO, to ? to.format("DD.MM.YYYY"): null);
  }

  toggleCompare(queryConfig: FilterLayoutConfig): void {
    if (!queryConfig.layout.compare) {
      this.routerDatastoreService.setCompareMode(queryConfig);
    } else {
      this.routerDatastoreService.setSingleMode(queryConfig);
    }
  }

  setFilterDataset(dataset: string, filterLayoutConfig: FilterLayoutConfig): void {
    const params: Params = { }
    params[QUERY_NAMES.FILTER.FILTER_DATASET] = dataset;
    params[QUERY_NAMES.FILTER.DYNAMIC.FILTER] = null;
    if (filterLayoutConfig.layout.layoutType === 'map') {
      // For map set the compare dataset to filter dataset
      params[QUERY_NAMES.FILTER.COMPARE_DATASET] = dataset;
      params[QUERY_NAMES.FILTER.DYNAMIC.COMPARE] = null;
    }
    this.routerDatastoreService.setParams(params);
    this.configDataStoreService.fetchFilterConfigWithKpi(dataset, filterLayoutConfig.config.filterKpi);
  }

  setCompareDataset(dataset: string, filter: FilterLayoutConfig): void {
    const params: Params = { }
    params[QUERY_NAMES.FILTER.COMPARE_DATASET] = dataset;
    params[QUERY_NAMES.FILTER.DYNAMIC.COMPARE] = null;
    this.routerDatastoreService.setParams(params);
    this.configDataStoreService.fetchCompareConfigWithKpi(dataset, filter.config.compareKpi);
  }

  addDynamicFilter(filter: FilterConfig, value: string): void {
    if (value === 'ALL') this.removeDynamicFilter(filter.type)
    else this.routerDatastoreService.setComplexArrayParam(
      QUERY_NAMES.FILTER.DYNAMIC.FILTER,
      `${filter.type}`,
      `${value}:${filter.visibleOnGraph}:${filter.visibleOnMap}`
    );
  }

  getLatestFilterValueFor(filter: FilterQueryParams, type: string, alternative: string = 'ALL'): string {
    const latestValue: DynamicFilter = filter.dynamicFilter[type];
    return latestValue ? latestValue.value : alternative;
  }

  getLatestCompareValueFor(filter: FilterQueryParams, type: string, alternative: string = 'ALL'): string {
    const latestValue: DynamicFilter = filter.dynamicCompare[type];
    return latestValue ? latestValue.value : alternative;
  }

  removeDynamicFilter(type: string): void {
    this.routerDatastoreService.removeComplexArrayParam(QUERY_NAMES.FILTER.DYNAMIC.FILTER, type);
  }

  addDynamicCompare(filter: FilterConfig, value: string): void {
    if (value === 'ALL') this.removeDynamicCompare(filter.type)
    else this.routerDatastoreService.setComplexArrayParam(
      QUERY_NAMES.FILTER.DYNAMIC.COMPARE,
      `${filter.type}`,
      `${value}:${filter.visibleOnGraph}:${filter.visibleOnMap}`
    );
  }

  removeDynamicCompare(type: string): void {
    this.routerDatastoreService.removeComplexArrayParam(QUERY_NAMES.FILTER.DYNAMIC.COMPARE, type);
  }

  isFilterVisible(filter: FilterConfig, layout: LayoutQueryParams): boolean {
    return (filter.visibleOnMap && layout.layoutType === 'map') || (filter.visibleOnGraph && layout.layoutType === 'graph');
  }
}
