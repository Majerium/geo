import {Component, OnDestroy, OnInit} from '@angular/core';
import {combineLatest, Observable, Subject} from "rxjs";
import {ShapeService} from "../../services/shape/shape.service";
import {Shape} from "../../model/shape/shape";
import {debounceTime, map, takeUntil} from "rxjs/operators";
import {Kpi} from "../../services/api/generated/data";
import {RouterDatastoreService} from "../../datastore/router/services/router-datastore.service";
import {QUERY_NAMES} from "../../datastore/router/reducer";

interface CompareAndKpiAndSelectedDataset {
  compare: boolean,
  kpi: [Kpi, Kpi],
  selectedDataset
}

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit, OnDestroy{

  static WAIT_FOR_DETAILS_UPDATE = 250;

  compareAndKpisAndDataset$: Observable<CompareAndKpiAndSelectedDataset>;
  unsubscribe$: Subject<void> = new Subject<void>();

  constructor(
    private readonly shapeService: ShapeService,
    private readonly routerDatastoreService: RouterDatastoreService
  ) {
    this.compareAndKpisAndDataset$ = combineLatest([
      this.routerDatastoreService.isCompare(),
      this.routerDatastoreService.getShapesArray(),
      this.shapeService.getShapes$(),
      this.routerDatastoreService.getFilterDataset()
    ]).pipe(
      map(([isCompare, details, shapes, filterDataset]) => this.getDetailsForShapes(isCompare, details, shapes, filterDataset))
    )
  }

  ngOnInit(): void {
    this.subscribeForShapeUpdates();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.unsubscribe();
  }

  private subscribeForShapeUpdates(): void {
    this.shapeService.getShapeUpdates$().pipe(
      takeUntil(this.unsubscribe$),
      debounceTime(DetailsComponent.WAIT_FOR_DETAILS_UPDATE))
      .subscribe(() => this.shapeService.forceUpdate());
  }

  public hideDetails(): void {
    this.routerDatastoreService.setParam(QUERY_NAMES.MAP.SHAPES, null);
  }

  private getDetailsForShapes(isCompare: boolean, geoHashes: Array<string> = [], shapes: { [key: string]: Shape }, filterDataset: string): CompareAndKpiAndSelectedDataset {
    const detailsWithData: Array<string> = geoHashes.filter((hash) => shapes[hash] !== undefined);
    const kpi: Kpi = { neuGenesen: 0, neuerTodesfall: 0, neuerFall: 0, anzahlTodesfall: 0, anzahlFall: 0, anzahlGenesen: 0, isErkrankunsbeginn: 0, vaccinationAmount: 0, deaths: 0 };
    const kpiCompare: Kpi = { neuGenesen: 0, neuerTodesfall: 0, neuerFall: 0, anzahlTodesfall: 0, anzahlFall: 0, anzahlGenesen: 0, isErkrankunsbeginn: 0, vaccinationAmount: 0, deaths: 0 };
    return {
      compare: isCompare,
      kpi: [
        detailsWithData.filter(feature => shapes[feature].kpi).map(feature => shapes[feature].kpi).reduce(this.reduceKpi, kpi),
        detailsWithData.filter(feature => shapes[feature].kpi).map(feature => shapes[feature].compareKpi).reduce(this.reduceKpi, kpiCompare),
      ],
      selectedDataset: filterDataset
    }
  }

  private reduceKpi(kpi1: Kpi, kpi2: Kpi): Kpi {
    return {
      anzahlGenesen: kpi1.anzahlGenesen += kpi2 ? kpi2.anzahlGenesen : 0,
      anzahlFall: kpi1.anzahlFall += kpi2 ? kpi2.anzahlFall : 0 ,
      anzahlTodesfall: kpi1.anzahlTodesfall += kpi2 ? kpi2.anzahlTodesfall : 0,
      neuerFall:  kpi1.neuerFall += kpi2 ? kpi2.neuerFall : 0,
      neuerTodesfall: kpi1.neuerTodesfall += kpi2 ? kpi2.neuerTodesfall : 0,
      neuGenesen: kpi1.neuGenesen += kpi2 ? kpi2.neuGenesen : 0,
      isErkrankunsbeginn: kpi1.isErkrankunsbeginn += kpi2 ? kpi2.isErkrankunsbeginn : 0,
      vaccinationAmount: kpi1.vaccinationAmount += kpi2 ? kpi2.vaccinationAmount : 0,
      deaths: kpi1.deaths += kpi2 ? kpi2.deaths : 0
    }
  }
}
