import {Threshold} from "./threshold";

export abstract class Palette {

  static LOW_VALUE = 2;

  protected abstract getPalette(): Array<Threshold>;

  public getColor(value: number, max: number = 1): string {
    const balance = value / max;
    const thresholds: Array<Threshold> = this.getPalette();
    for (let i = 0; i < thresholds.length; i++) {
      if(thresholds[i].ordinal > balance) {
        return ((i == 0) ? thresholds[0] : thresholds[i-1]).color
      }
    }
  }

  public getColorsCompareColors(): [string, string, string] {
    const thresholds: Array<Threshold> = this.getPalette();
    return [
      thresholds[Palette.LOW_VALUE].color,
      '#ffffff',
      thresholds[thresholds.length -1].color,
    ];
  }

  public getColorForCompare(filter: number, compare: number): string {
    const colors: [string, string, string] = this.getColorsCompareColors();
    if(filter < compare) {
      return colors[colors.length -1];
    } else if (filter > compare) {
      return colors[0];
    } else return colors[1];
  }
}
