export interface Threshold {
  ordinal: number,
  color: string,
}
