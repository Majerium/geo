import {Palette} from "./palette";
import {Threshold} from "./threshold";

export class ColorPaletteYellowOrangeRed extends Palette {
  protected getPalette(): [Threshold, Threshold, Threshold, Threshold, Threshold] {
    return [
        { ordinal: 0.2, color: '#ffffff' },
        { ordinal: 0.4, color: '#ead78d' },
        { ordinal: 0.6, color: '#eaa338' },
        { ordinal: 0.8, color: '#ef6910' },
        { ordinal: 1,   color: '#f33d05' }
    ];
  }
}

export class ColorPaletteBlue extends Palette {
  protected getPalette(): [Threshold, Threshold, Threshold, Threshold, Threshold] {
    return [
      { ordinal: 0.2, color: '#ffffff' },
      { ordinal: 0.4, color: '#bfc7ea' },
      { ordinal: 0.6, color: '#518ff3' },
      { ordinal: 0.8, color: '#2f54ea' },
      { ordinal: 1,   color: '#000b54' }
    ];
  }
}
