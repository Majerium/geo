import {ChangeDetectionStrategy, Component} from '@angular/core';
import {ColorPaletteService, ColorScheme} from "../../services/color-palette/color-palette.service";
import {combineLatest, Observable} from "rxjs";
import {AppConfigDatastoreService} from "../../datastore/config/services/app-config-datastore.service";
import {AppConfig, GraphType} from "../../datastore/config/model/appConfig";
import {ChartDatastoreService} from "../../datastore/chart/services/chart-datastore.service";
import {
  ConfigLayout,
  RouterDatastoreService
} from "../../datastore/router/services/router-datastore.service";
import {QUERY_NAMES} from "../../datastore/router/reducer";
import {FilterAppConfig} from "../../datastore/config/reducer/appConfig";
import {map} from "rxjs/operators";

type LayoutConfigFilterConfig = {
  queryParams: ConfigLayout,
  appFilterConfig: FilterAppConfig
}

@Component({
  selector: 'app-color-scheme',
  templateUrl: './color-scheme.component.html',
  styleUrls: ['./color-scheme.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ColorSchemeComponent {

  filterConfig$: Observable<LayoutConfigFilterConfig>;

  constructor(
    private readonly colorSchemeService: ColorPaletteService,
    private readonly appConfigDatastoreService: AppConfigDatastoreService,
    private readonly chartDatastoreService: ChartDatastoreService,
    private readonly routerDatastoreService: RouterDatastoreService
  ) {
    this.filterConfig$ = combineLatest([
      this.routerDatastoreService.getConfigLayout(),
      this.appConfigDatastoreService.getAppFilterConfig()
    ]).pipe(
      map(([queryParams, appFilterConfig]) => {
        return {
          appFilterConfig: appFilterConfig,
          queryParams: queryParams
        }
      })
    )
  }

  public getColorSchemes(): Array<ColorScheme>{
    return this.colorSchemeService.getColorSchemeNames();
  }

  public getGraphTypes(): Array<GraphType> {
    return [ "area", "column", "line"];
  }

  public setColorScheme(scheme: ColorScheme): void {
    this.routerDatastoreService.setParam(QUERY_NAMES.CONFIG.COLOR_SCHEME, scheme)
  }

  public setGraphType(graphType: GraphType): void {
    this.routerDatastoreService.setParam(QUERY_NAMES.CONFIG.GRAPH_TYPE, graphType);
  }

  public toggleInPercentage(inPercentage: boolean): void {
    this.routerDatastoreService.setParam(QUERY_NAMES.CONFIG.IN_PERCENTAGE, RouterDatastoreService.ofBoolean(!inPercentage))
  }

  public toggleCumulative(cumulative: boolean): void {
    this.routerDatastoreService.setParam(QUERY_NAMES.CONFIG.CUMULATIVE, RouterDatastoreService.ofBoolean(!cumulative))
  }

  public getFilterKpis(config: AppConfig): Array<string> {
    return config.filterConfig ? config.filterConfig.kpiConfigs.map(k => k.kpi) : [];
  }

  public getCompareKpis(config: AppConfig): Array<string> {
    return config.compareConfig ? config.compareConfig.kpiConfigs.map(k => k.kpi) : [];
  }

  public setFilterKpi(kpi: string): void {
    this.routerDatastoreService.setParam(QUERY_NAMES.CONFIG.FILTER_KPI, kpi)
  }

  public setCompareKpi(kpi: string): void {
    this.routerDatastoreService.setParam(QUERY_NAMES.CONFIG.COMPARE_KPI, kpi)
  }

}
