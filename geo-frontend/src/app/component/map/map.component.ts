import {Component, OnDestroy, OnInit} from '@angular/core';
import {MapService} from "../../services/map/map.service";
import {RouterDatastoreService} from "../../datastore/router/services/router-datastore.service";
import {combineLatest, Subject} from "rxjs";
import {debounceTime, filter, map, takeUntil} from "rxjs/operators";
import {Params} from "@angular/router";
import {QUERY_NAMES} from "../../datastore/router/reducer";
import {ShapeService} from "../../services/shape/shape.service";
import {Shape} from "../../model/shape/shape";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnDestroy {

  private _lastX: number;
  private _lastY: number;

  private unsubscribeInitialAction$: Subject<void> = new Subject();
  private unsubscribe$: Subject<void> = new Subject();

  constructor(
    private readonly mapService: MapService,
    private readonly routerDatastoreService: RouterDatastoreService,
    private readonly shapeService: ShapeService
  ) { }

  ngOnInit(): void {
    this.subscribeToInitialAction();
    this.subscribeToSelectedShapes();
    this.subscribeToCloseDetails();
    this.removeGraphRelevantData();
  }

  ngOnDestroy(): void {
    this.mapService.destroy()
    if (this.unsubscribe$ && !this.unsubscribe$.isStopped) {
      this.unsubscribe$.next();
      this.unsubscribe$.unsubscribe();
    }
  }

  private unsubscribe(): void {
    if (this.unsubscribeInitialAction$ && !this.unsubscribeInitialAction$.isStopped) {
      this.unsubscribeInitialAction$.next();
      this.unsubscribeInitialAction$.unsubscribe();
    }
  }

  private subscribeToCloseDetails(): void {
    this.routerDatastoreService.showDetails().pipe(
      takeUntil(this.unsubscribe$),
      filter((showDetails: boolean) => !showDetails)
    ).subscribe(_ => this.shapeService.unselectAllShape());
  }

  private subscribeToSelectedShapes(): void {
    combineLatest([this.shapeService.getShapes$(), this.routerDatastoreService.getShapesArray()]).pipe(
      takeUntil(this.unsubscribe$),
      debounceTime(10),
      map(([shapeData, geoHashes]) => this.shapesWithData(shapeData, geoHashes))
    ).subscribe(shapes => shapes.forEach((shape: Shape) => shape.highlightShape()))
  }

  private removeGraphRelevantData() : void {
    this.routerDatastoreService.removeChartRelevantParams();
  }

  private shapesWithData(shapeData: { [key: string]: Shape }, geoHashes: Array<string> = []): Array<Shape> {
    const shapes: Array<Shape> = [];
    for(let i = 0; i < geoHashes.length; i++) {
      if (shapeData[geoHashes[i]]) {
        shapes.push(shapeData[geoHashes[i]]);
      }
    }
    return shapes;
  }

  private subscribeToInitialAction(): void {
    this.routerDatastoreService.getMapQueryParams().pipe(
      takeUntil(this.unsubscribeInitialAction$),
      filter(params => params.lat && params.lon && params.zoom)
    ).subscribe(params => {
      this.mapService.initiateMap(
        params.lat as number,
        params.lon as number,
        params.zoom as number
      );
      this.unsubscribe();
    })
  }

  public setMouseDown(event: MouseEvent): void {
    this._lastX = event.clientX;
    this._lastY = event.clientY;
  }

  public setMouseUp(event: MouseEvent): void {
    if (event.clientX === this._lastX && event.clientY === this._lastY && !this.wasClickedOnFeature(event)) {
      const params: Params = { };
      params[QUERY_NAMES.LAYOUT.IS_SIDEBAR_VISIBLE] = 'false';
      params[QUERY_NAMES.MAP.SHAPES] = null;
      this.routerDatastoreService.setParams(params);
    }
  }

  private wasClickedOnFeature(event: MouseEvent): boolean {
    const classes: Array<string> = (event.target as any).classList || [];
    return classes.length > 0 && classes[0] === "leaflet-interactive";
  }

}
