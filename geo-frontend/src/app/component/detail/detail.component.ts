import {Component, Input} from '@angular/core';
import {ColorPaletteService} from "../../services/color-palette/color-palette.service";
import {RouterDatastoreService} from "../../datastore/router/services/router-datastore.service";
import {QUERY_NAMES} from "../../datastore/router/reducer";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent  {

  private _isCompare: boolean;
  private _kpi: number;
  private _kpiCompare: number;
  private _label;

  private _selectedKpi: string;

  @Input() set label(label: string) {
    this._label = label;
  }

  @Input() set kpi(number: number) {
    this._kpi = number;
  }

  @Input() set kpiCompare(kpi: number) {
    this._kpiCompare = kpi;
  }

  @Input() set isCompare(isCompare: boolean) {
    this._isCompare = isCompare;
  }

  get label(): string {
    return this._label;
  }

  get kpi(): number {
    return this._kpi;
  }

  get kpiCompare(): number {
    return this._kpiCompare;
  }

  get isCompare(): boolean {
    return this._isCompare;
  }

  constructor(
    private readonly colorPaletteService: ColorPaletteService,
    private readonly routerDatastoreService: RouterDatastoreService)
  {
    this.routerDatastoreService.getFilterKpi().subscribe((selectedKpi: string) => this._selectedKpi = selectedKpi)
  }

  isSelected(){
    return this.label === this._selectedKpi;
  }

  getColorForFilter(): string {
    return this.colorPaletteService.getCompareColor(1, 0);
  }

  getColorForCompare(): string {
    return this.colorPaletteService.getCompareColor(0, 1);
  }

  getFontWeightForHeadline(): string {
    return this.isSelected() ? 'bold' : 'normal';
  }

  setKpi(): void {
    this.routerDatastoreService.setParam(QUERY_NAMES.CONFIG.FILTER_KPI, this._label)
  }

}
