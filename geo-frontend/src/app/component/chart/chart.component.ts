import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {GeoDataApiControllerService, MultiGraphResponse} from "../../services/api/generated/data";
import {debounceTime, filter, takeUntil, tap} from "rxjs/operators";
import {combineLatest, Observable, Subject} from "rxjs";
import * as Highcharts from 'highcharts';
import {ChartDatastoreService} from "../../datastore/chart/services/chart-datastore.service";
import {AppConfigDatastoreService} from "../../datastore/config/services/app-config-datastore.service";
import {TranslateService} from "@ngx-translate/core";
import {FilterQueryParams} from "../../datastore/router/model/filterQueryParams";
import {GraphConfig, RouterDatastoreService} from "../../datastore/router/services/router-datastore.service";
import {LayoutType} from "../../datastore/router/model/layoutQueryParams";
import {GraphType} from "../../datastore/router/model/configQueryParams";

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChartComponent implements OnInit, OnDestroy {

  private unsubscribe$: Subject<void> = new Subject();
  Highcharts: typeof Highcharts = Highcharts;

  kips$: Observable<MultiGraphResponse>;
  chart: Highcharts.Options;
  showChart: boolean = true;

  static WAIT_UNTIL_CHART_REDRAW = 300;

  constructor(
    private readonly api: GeoDataApiControllerService,
    private readonly chartDatastoreService: ChartDatastoreService,
    private readonly configDatastoreService: AppConfigDatastoreService,
    private readonly translateService: TranslateService,
    private readonly routerDatastoreService: RouterDatastoreService,
    private readonly changeDetectionReference: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.subscribeFilter();
    this.subscribeTitle();
    this.subscribeSubtitle();
    this.subscribeChart();
    this.subscribeToChartType();
    this.removeMapRelevantParams();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.unsubscribe();
  }

  private removeMapRelevantParams(): void {
    this.routerDatastoreService.removeMapRelevantParams();
  }

  private subscribeToChartType(): void {
    this.routerDatastoreService.getGraphType().pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe((graphType: GraphType) => this.chartDatastoreService.setGraphType(graphType));
  }

  private subscribeChart(): void {
    this.chartDatastoreService.getChartData()
      .pipe(
        takeUntil(this.unsubscribe$),
        tap(_ => this.prepareChart()),
        debounceTime(ChartComponent.WAIT_UNTIL_CHART_REDRAW),
      ).subscribe((data: Highcharts.Options) => this.updateChart(data));
  }

  private prepareChart(): void {
    this.showChart = false;
    this.changeDetectionReference.detectChanges();
  }

  private updateChart(data: Highcharts.Options): void {
    this.chart = data;
    this.showChart = true
    this.changeDetectionReference.detectChanges();
  }

  private subscribeFilter(): void {
    // For fetching data
    combineLatest([
        this.routerDatastoreService.getFilterQueryParams(),
        this.routerDatastoreService.isCompare(),
        this.routerDatastoreService.getLayoutType(),
        this.routerDatastoreService.getFilterKpi(),
        this.routerDatastoreService.getCompareKpi(),
        this.routerDatastoreService.getGraphConfig()
    ]).pipe(
      takeUntil(this.unsubscribe$),
      filter(([filter, isCompare, layoutType, selectedFilterKpi, selectedCompareKpi, graphConfig]) => this.isGraph(layoutType)),
      tap(() => this.chartDatastoreService.clearSeries()),
      debounceTime(ChartComponent.WAIT_UNTIL_CHART_REDRAW) // Strange errors without
    ).subscribe(([filter, isCompare, layoutType, selectedKpi, selectedCompareKpi, graphConfig]) => this.fetchGraphData(filter, isCompare, layoutType, selectedKpi, selectedCompareKpi, graphConfig));
  }

  private isGraph(graphType: LayoutType): boolean {
    return graphType === 'graph';
  }

  private fetchGraphData(filter: FilterQueryParams, isCompare: boolean, layoutType: LayoutType, selectedFilterKpi: string, selectedCompareKpi: string, graphConfig: GraphConfig): void {
    if (isCompare) this.chartDatastoreService.fetchCompareChartData(filter, selectedCompareKpi, isCompare, graphConfig, selectedFilterKpi !== selectedCompareKpi);
    this.chartDatastoreService.fetchFilterChartData(filter, selectedFilterKpi, isCompare, graphConfig, selectedFilterKpi !== selectedCompareKpi);
    this.setXAxisType(isCompare);
  }

  private setXAxisType(isCompare: boolean): void {
    if (isCompare) this.chartDatastoreService.setXAxis('linear');
    else this.chartDatastoreService.setXAxis('datetime');
  }

  private subscribeTitle(): void {
    combineLatest([this.routerDatastoreService.isCompare(), this.routerDatastoreService.getFilterKpi(), this.routerDatastoreService.getCompareKpi()]).pipe(
      takeUntil(this.unsubscribe$),
      debounceTime(ChartComponent.WAIT_UNTIL_CHART_REDRAW))
      .subscribe(([isCompare, kpi, compareKpi]) => {
        let title: string = this.getTranslated(`${kpi}`);
        if (isCompare) title = `${title} vs ${this.getTranslated(compareKpi)}`;
        this.chartDatastoreService.setChartTitel( { text: title});
        this.chartDatastoreService.setYAxis({ title: { text: title }})
      });
  }

  private subscribeSubtitle(): void {
    combineLatest([this.routerDatastoreService.isCompare(), this.routerDatastoreService.getFilterQueryParams()]).pipe(
      takeUntil(this.unsubscribe$),
      debounceTime(ChartComponent.WAIT_UNTIL_CHART_REDRAW)
    ).subscribe(([isCompare, filter]) => this.chartDatastoreService.setChartSubTitel({ text: this.getSubTitleFilter(isCompare, filter)}));
  }

  private getSubTitleFilter(isCompare: boolean, filter: FilterQueryParams): string {
    const datasource: string = this.getTranslated('DATA_SOURCE');
    const dataset: string = this.getTranslated(filter.filterDataset);
    let datasetAndSource: string = `${ datasource }: ${ dataset } `;
    for (let i = 0; i <= Object.entries(filter.dynamicFilter).length; i++) {
      let key: string = this.getTranslated(Object.keys(filter.dynamicFilter)[i]);
      let hasValue: boolean = Object.values(filter.dynamicFilter)![i] !== undefined;
      let hasTranslateValue = hasValue && Object.values(filter.dynamicFilter)![i].value.includes('_');
      let value = hasTranslateValue ? this.getTranslated(Object.values(filter.dynamicFilter)![i].value) : hasValue ? Object.values(filter.dynamicFilter)![i].value : '';
      if (key !== '' ) datasetAndSource += ` ${ key }: ${value};`;
    }
    if (filter.from) datasetAndSource += ` ${ this.getTranslated("FROM") } ${ filter.from.format('DD.MM.YYYY') }`;
    if (filter.to) datasetAndSource += ` ${ this.getTranslated("TO") } ${ filter.to.format('DD.MM.YYYY') }`;
    if (isCompare) datasetAndSource += this.getSubTitleCompare(filter);
    return datasetAndSource;
  }

  private getSubTitleCompare(filter: FilterQueryParams): string {
    const datasource: string = this.getTranslated('DATA_SOURCE');
    const dataset: string = this.getTranslated(filter.compareDataset);
    let datasetAndSource: string = `${ datasource }: ${ dataset } `;
    for (let i = 0; i <= Object.entries(filter.dynamicCompare).length; i++) {
      let key: string = this.getTranslated(Object.keys(filter.dynamicCompare)[i]);
      let hasValue: boolean = Object.values(filter.dynamicCompare)![i] !== undefined;
      let hasTranslateValue = hasValue && Object.values(filter.dynamicCompare)![i].value.includes('_');
      let value = hasTranslateValue ? this.getTranslated(Object.values(filter.dynamicCompare)![i].value) : hasValue ? Object.values(filter.dynamicCompare)![i].value : '';
      if (key !== '' ) datasetAndSource += ` ${ key }: ${value};`;
    }
    if (filter.fromCompare) datasetAndSource += ` ${ this.getTranslated("FROM") } ${ filter.fromCompare.format('DD.MM.YYYY') }`;
    if (filter.toCompare) datasetAndSource += ` ${ this.getTranslated("TO") } ${ filter.toCompare.format('DD.MM.YYYY') }`;
    return `<br/>${datasetAndSource}`;
  }

  private getTranslated(toBeTranslated: string): string {
    return toBeTranslated ? this.translateService.instant('KEY_' + toBeTranslated) : '';
  }
}
