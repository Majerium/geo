import {Component} from '@angular/core';
import {ColorPaletteService} from "../../services/color-palette/color-palette.service";
import {combineLatest, Observable} from "rxjs";
import {RouterDatastoreService} from "../../datastore/router/services/router-datastore.service";
import {map} from "rxjs/operators";

type ColorAndText = {
  color: string,
  text: string
}

type IsDetailsIsCompare = {
  showDetails: boolean,
  isCompare: boolean
}

@Component({
  selector: 'app-legend',
  templateUrl: './legend.component.html',
  styleUrls: ['./legend.component.scss']
})
export class LegendComponent {

  showDetailsAndCompare$: Observable<IsDetailsIsCompare>;
  selectedKpi$: Observable<string>;

  constructor(
    private readonly routerDatastoreService: RouterDatastoreService,
    private readonly colorSchemeService: ColorPaletteService,
  ) {
    this.selectedKpi$ = this.routerDatastoreService.getFilterKpi();
    this.showDetailsAndCompare$ = combineLatest([this.routerDatastoreService.showDetails(), this.routerDatastoreService.isCompare()]).pipe(
      map(([showDetails, isCompare]) => {
        return {
          isCompare: isCompare,
          showDetails: showDetails
        }
      })
    );

  }

  public getPositionFromRight(isCompare: boolean): string {
    return isCompare ? '270px' : '20px';
  }

  public getColorsAndLabels(): [ColorAndText, ColorAndText, ColorAndText] {
    const colors: [string, string, string] = this.colorSchemeService.getCompareColors();
    return [
      { color: colors[0], text: 'KEY_DS_1' },
      { color: colors[1], text: 'KEY_DS_EQUAL' },
      { color: colors[2], text: 'KEY_DS_2' },
    ];
  }
}
