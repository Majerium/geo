import { Component } from '@angular/core';
import {AppConfigDatastoreService} from "../../datastore/config/services/app-config-datastore.service";

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent {

  constructor(private readonly appConfigDataService: AppConfigDatastoreService) { }

  hideInfo(): void {
    this.appConfigDataService.hideInfo();
  }

}
