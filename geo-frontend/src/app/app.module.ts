import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NgModule} from '@angular/core';
import {MapComponent} from './component/map/map.component';
import {DatastoreModule} from "./datastore/datastore.module";
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from "@angular/common/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule } from '@angular/material/sidenav';
import {MatIconModule} from '@angular/material/icon'
import {MatButtonModule} from '@angular/material/button';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { DateInputComponent } from './component/inputs/date-input/date-input.component';
import {MatNativeDateModule} from "@angular/material/core";
import { FilterComponent } from './component/filter/filter.component';
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {SpinnerInterceptor} from "./spinner.interceptor";
import {MatExpansionModule} from '@angular/material/expansion';
import { ColorSchemeComponent } from './component/color-scheme/color-scheme.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { AutocompleteInputComponent } from './component/inputs/autocomplete-input/autocomplete-input.component';
import { SelectInputComponent } from './component/inputs/select-input/select-input.component';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from "@angular/material/input";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {TranslateLoader, TranslateModule, TranslatePipe} from "@ngx-translate/core";
import { DetailsComponent } from './component/details/details.component';
import {ReactiveFormsModule} from "@angular/forms";
import {MatCardModule} from '@angular/material/card';
import { LegendComponent } from './component/legend/legend.component';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import { DetailComponent } from './component/detail/detail.component';
import { InfoComponent } from './component/info/info.component';
import { ChartComponent } from './component/chart/chart.component';
import { HighchartsChartModule } from 'highcharts-angular';
import {DatePipe, DecimalPipe} from "@angular/common";


@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    DateInputComponent,
    FilterComponent,
    ColorSchemeComponent,
    AutocompleteInputComponent,
    SelectInputComponent,
    DetailsComponent,
    LegendComponent,
    DetailComponent,
    InfoComponent,
    ChartComponent
  ],
  imports: [
    HighchartsChartModule,
    MatInputModule,
    BrowserModule,
    AppRoutingModule,
    DatastoreModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatIconModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatCardModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpTranslateLoader,
        deps: [HttpClient]
      }
    }),
    ReactiveFormsModule
  ],
  providers: [
    DatePipe,
    DecimalPipe,
    { provide: HTTP_INTERCEPTORS, useClass: SpinnerInterceptor, multi: true },
    { provide: MAT_DATE_LOCALE, useValue: 'de-DE'},
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [ MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS ]
    },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function httpTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http);
}


