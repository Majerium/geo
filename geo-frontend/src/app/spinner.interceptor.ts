import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {finalize} from "rxjs/operators";
import {AppConfigDatastoreService} from "./datastore/config/services/app-config-datastore.service";

@Injectable()
export class SpinnerInterceptor implements HttpInterceptor {

  private loading = 0;

  constructor(private readonly appConfigDatastoreService: AppConfigDatastoreService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.showLoading();
    return next.handle(request).pipe(finalize(() => this.hideLoading()));
  }

  private showLoading(): void {
    if(0 === this.loading++){
      this.appConfigDatastoreService.showSpinner();
    }
  }

  private hideLoading() {
    if(--this.loading === 0) {
      this.appConfigDatastoreService.hideSpinner();
    }
  }

}
